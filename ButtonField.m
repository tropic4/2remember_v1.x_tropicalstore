#import "ButtonField.h"
#import "InterfaceDriver.h"
#import "agenda.h"
#import "appuntamento.h"
#import "MainWindowController.h"
#import "DataWindowController.h"
#import "SmartScrollView.h"

@implementation ButtonField
-(id)initWithFrame:(NSRect)rect {
    if (self=[super initWithFrame:rect]) {
        ARRbuttonField=self;
        progressiveNumber=0;
        requireAnimation=NO;
        trackingRects=[[NSMutableArray alloc] init];
        appuntamentoHilight=nil;
        lockSelection=NO;
        numeroPagina=1;
        liveResizeBuffer=[[NSImage alloc] initWithSize:rect.size];
        [self setAutoresizingMask:NSViewMinYMargin];
        bufferIsEmpty=YES;
        modificaTotaleInterfaccia=YES;
        shouldSlide=NO;
        shouldSlideAdd=NO;
        lockRects=NO;
        safeguard=[[NSMutableArray alloc] init];
    }
    return self;
}

-(void)awakeFromNib {
    NSNotificationCenter *nc;
    nc=[NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(numberOfRecordsChanged) name:@"ARRnumberOfRecordsChanged" 		object:nil];
    [nc addObserver:self selector:@selector(selectedRecordChanged) name:@"ARRselectedRecordChanged" object:nil];
    [nc addObserver:self selector:@selector(dataChanged) name:@"ARRdataChanged" object:nil];
}

-(void)dataChanged {
   // NSLog(@" dataChanged");
    [self recalculateButtons];
    [self redrawOffline];
    [self setNeedsDisplay:YES];
}

-(void)setDynamicBuffer:(NSPoint)dove {
    NSRect	theTop,theBottom;
    NSPoint		theOrigin;

    theOrigin.x=0;
    theOrigin.y=0;
    
    theBottom.origin.x=0;
    theBottom.origin.y=0;
    theBottom.size.width=200;
    theBottom.size.height=dove.y+40;

    theTop=[self bounds];
    theTop.origin.x=0;
    theTop.origin.y=theBottom.size.height-1;
    theTop.size.height=theTop.size.height-theBottom.size.height+1;
    
    dynamicTop=[[NSImage alloc] initWithSize:theTop.size];
    [dynamicTop lockFocus];
    [liveResizeBuffer compositeToPoint:theOrigin fromRect:theTop operation:NSCompositeCopy];
    [dynamicTop unlockFocus];
    dynamicBottom=[[NSImage alloc] initWithSize:theBottom.size];
    [dynamicBottom lockFocus];
    [liveResizeBuffer compositeToPoint:theOrigin fromRect:theBottom operation:NSCompositeCopy];
    [dynamicBottom unlockFocus];

    shouldSlide=YES;
}

-(void)resetDynamicBuffer {
    [dynamicTop release];

    [dynamicBottom release];
    shouldSlide=NO;
}

-(void)dynamicAdd:(appuntamento*)appuntamento {
    NSRect		theTop,theBottom;
    NSPoint		theOrigin;

    theOrigin.x=0;
    theOrigin.y=0;

    theBottom.origin=[appuntamento posizione];
    theBottom.size.height=40;
    theBottom.size.width=200;
    
    
    theTop=[self bounds];
    theTop.origin.x=0;
    theTop.origin.y=theBottom.origin.y+theBottom.size.height-1;
    theTop.size.height=theTop.size.height-theBottom.size.height+1;

    dynamicTop=[[NSImage alloc] initWithSize:theTop.size];
    [dynamicTop lockFocus];
    [liveResizeBuffer compositeToPoint:theOrigin fromRect:theTop operation:NSCompositeCopy];
    [dynamicTop unlockFocus];
    
    dynamicBottom=[[NSImage alloc] initWithSize:theBottom.size];
    [dynamicBottom lockFocus];
    [liveResizeBuffer compositeToPoint:theOrigin fromRect:theBottom operation:NSCompositeCopy];
    [dynamicBottom unlockFocus];

    shouldSlideAdd=YES;
}

-(void)endDynamicAdd {
    [dynamicTop release];

    [dynamicBottom release];
    shouldSlideAdd=NO;
}


-(void)selectedRecordChanged {
    NSRect chi;

    [[ARRmainWindowController window] disableFlushWindow];
        
    chi.origin=[[ARRagenda selected] posizione];
    chi.size.width=199;
    chi.size.height=40;
    
    [self redrawOffline];
    //DEVE ESSERE SICURO CHE SIA VISIBILE!!!
    [self scrollRectToVisible:chi];
    
    [ARRinterfaceDriver updateButtons];
    
    [[ARRmainWindowController window] enableFlushWindow];
    [self setNeedsDisplay:YES];
}

-(void)modificaTotaleInterfaccia {
    modificaTotaleInterfaccia=YES;
}

-(void)numberOfRecordsChanged {
    //Solo in partenza e quando il sistema modifica le cose...
    [self recalculateButtons];

    [self redrawOffline];
    
    [ARRinterfaceDriver mainInterfaceUpdate];
    
    [self setScrolling];
    modificaTotaleInterfaccia=YES;
    [self setNeedsDisplay:YES];
}

-(NSImage*)offlineImage
{
    return liveResizeBuffer;
}

-(void)setRequireAnimation:(BOOL)yesorno
{
    requireAnimation=yesorno;
}

- (void)mouseDown:(NSEvent *)theEvent
{
    NSPoint doveClick=[theEvent locationInWindow];
    doveClick=[self convertPoint:doveClick fromView:nil];

    // chiDevi
    // who had to                                                               //leg20230914 - Translation
    appuntamento *chiDeve=[self matchButton:appuntamentoMouseOver withPoint:doveClick];


    if (ARRdoubleClick==YES) {
        if ([theEvent clickCount]==1) {
            //seleziona ma non mostra (toggle)
            [self trackingAggiunto:chiDeve withPoint:doveClick];
            [ARRagenda toggleSelected:chiDeve];
        } else if ([theEvent clickCount]==2) {
            //mostra
            [ARRagenda setSelected:chiDeve];
            [ARRdataWindowController show];
        }
    } else {
        [self trackingAggiunto:chiDeve withPoint:doveClick];

        if ([ARRagenda selected]==chiDeve) {
            [ARRagenda toggleSelected:chiDeve];
        } else {
            [ARRagenda setSelected:chiDeve];
            [ARRdataWindowController show];
        }
    }
        [chiDeve snooze:10];
}

-(void)doubleClickOnSelected {
    [ARRdataWindowController show];
    [[ARRagenda selected] snooze:10];
}

-(void)trackingAggiunto:(appuntamento*)appunt withPoint:(NSPoint)punto {
    if (punto.x>180&&punto.x<195) {
        NSPoint posizione=[appunt posizione];
        if (punto.y>=posizione.y&&punto.y<=posizione.y+20) [appunt matchAlarm];
        if (punto.y>=posizione.y+21&&punto.y<=posizione.y+40) [appunt matchAction];
    }
}

-(appuntamento*)matchButton:(appuntamento*)appunt withPoint:(NSPoint)punto
{
    appuntamento 	*candidate=nil,*appuntamentoAttuale;
    int			quale=0;

    NSPoint	posizione;
    
    posizione.x=-30;
    posizione.y=-30;
    
    if (appunt!=nil) posizione=[appunt posizione];
    
    if (punto.x>=posizione.x&&punto.x<=posizione.x+200&&punto.y>=posizione.y&&punto.y<=posizione.y+39) {
        candidate=appunt;
    } else {
        //Adesso bisogna fare un match tra tutti gli eventi
        NSArray *newArray=[ARRagenda dataArray];
        unsigned howMany=[newArray count];
        for(quale=0; quale<howMany; quale++)
        {
            appuntamentoAttuale=[newArray objectAtIndex:quale];
            NSPoint posizione=[appuntamentoAttuale posizione];
            if (punto.x>=posizione.x&&punto.x<=posizione.x+200&&punto.y>=posizione.y&&punto.y<=posizione.y+39) {
                candidate=appuntamentoAttuale;
            }
        }            
    }
    return candidate;
}

-(BOOL)isOpaque {
    return YES;
}

//intercettiamo cosi' non si puo' trascinare la finestra...
- (void)mouseDragged:(NSEvent *)theEvent {
}

-(void)mouseEntered:(NSEvent *)e {
    appuntamento	*questoAppuntamento;

    appuntamentoMouseOver=[e userData];
    questoAppuntamento=[e userData];
    if (appuntamentoHilight!=questoAppuntamento){
        [self lockFocus];
        if (appuntamentoHilight!=nil) {
            [appuntamentoHilight hilight:NO];
            //ridisegna quello non piu' selezionato.
            [[appuntamentoHilight selfImage] compositeToPoint:[appuntamentoHilight posizione] operation:NSCompositeCopy];
        }
        [questoAppuntamento hilight:YES];
        [[questoAppuntamento selfImage] compositeToPoint:[questoAppuntamento posizione] operation:NSCompositeCopy];

        [self unlockFocus];
        [[ARRmainWindowController window] flushWindow];
        appuntamentoHilight=questoAppuntamento;
    }
}

-(void)mouseExited:(NSEvent *)e {
    NSPoint		mouseLoc;
    BOOL		isInside;

    //Fa qualcosa solo se il mouse � esterno alla view...
    mouseLoc = [self convertPoint:[e locationInWindow] fromView:nil];
    isInside = [self mouse:mouseLoc inRect:[self bounds]];

    if (isInside==NO) {
        if (appuntamentoHilight!=nil) {
            [appuntamentoHilight hilight:NO];
            [self lockFocus];
            [[appuntamentoHilight selfImage] compositeToPoint:[appuntamentoHilight posizione] 			operation:NSCompositeCopy];
            [self unlockFocus];
        }
        appuntamentoHilight=nil;
        [[ARRmainWindowController window] flushWindow];
    }
}

-(void)resetStatus{
    appuntamentoHilight=nil;
    appuntamentoMouseOver=nil;

}

-(void)redrawOffline {
    unsigned		howMany;
    NSMutableArray	*newArray;
    int			yAttuale;
    int			attuale;
    appuntamento	*appuntamentoAttuale;
    NSRect		mySize;
    NSRect		buttonRect;

    newArray=[ARRagenda dataArray];
    howMany=[newArray count];
    mySize.size.height=howMany*39+1;
    mySize.size.width=200;
    [liveResizeBuffer setSize:mySize.size];
    [liveResizeBuffer lockFocus];
    yAttuale=0;
    for(attuale=(howMany-1); attuale>=0; attuale--) {
        appuntamentoAttuale=[newArray objectAtIndex:attuale];
        if ([appuntamentoAttuale isDeleted]==NO) {
            buttonRect.origin=[appuntamentoAttuale posizione];
            [[appuntamentoAttuale selfImage] compositeToPoint:buttonRect.origin operation:NSCompositeCopy];
        }
    }
    [liveResizeBuffer unlockFocus];
}

-(void)recalculateButtons {
    unsigned		howMany;
    NSMutableArray	*newArray;
    int			yAttuale;
    int			attuale;
    appuntamento	*appuntamentoAttuale;
    NSPoint		newPosition;
    NSRect		mySize;

  //  NSLog(@"Recalculate Buttons");
    bufferIsEmpty=NO;
    //Prendiamo il puntatore all'array che contiene i dati
    newArray=[ARRagenda dataArray];

    //Quanti ce ne sono...
    howMany=[newArray count];

    //Iniziamo a disegnare dal basso
    yAttuale=1;
    //nota: partiamo da 1 perch� poi viene tolto 1 ai 40 pixel di altezza per tutti i bottoni eccetto l'ultimo.

    //Per tutti i pulsanti... Iniziando dall'ultimo...
    for(attuale=(howMany-1); attuale>=0; attuale--) {
        //partiamo dal fondo
        appuntamentoAttuale=[newArray objectAtIndex:attuale];

        //deve essere cancellato?
        if ([appuntamentoAttuale isDeleted]==NO) {
            yAttuale--;
            newPosition.x=0;
            newPosition.y=yAttuale;
            yAttuale=yAttuale+40; //Ci spostiamo in su di 40

            [appuntamentoAttuale setPosizione:newPosition];
            //[appuntamentoAttuale setDisplayed:YES];
        } else {
            //lo saltiamo e settiamo display come no
            //[appuntamentoAttuale setDisplayed:NO];
        }
    }
    //Fatto. � ora di aggiornare le dimensioni della NSViwe
    mySize=[self frame];
    mySize.size.height=yAttuale;
    [self setFrame:mySize];
    modificaTotaleInterfaccia=YES;
}

-(void)onlyRecalculateButtons {
    unsigned		howMany;
    NSMutableArray	*newArray;
    int			yAttuale;
    int			attuale;
    appuntamento	*appuntamentoAttuale;
    NSPoint		newPosition;
  //  NSRect		mySize;

    bufferIsEmpty=NO;
    newArray=[ARRagenda dataArray];
    howMany=[newArray count];
    yAttuale=1;
    for(attuale=(howMany-1); attuale>=0; attuale--)
    {
        appuntamentoAttuale=[newArray objectAtIndex:attuale];
        if ([appuntamentoAttuale isDeleted]==NO) {
            yAttuale--;
            newPosition.x=0;
            newPosition.y=yAttuale;
            yAttuale=yAttuale+40; //Ci spostiamo in su di 40
            [appuntamentoAttuale setPosizione:newPosition];
        }
    }
}

-(void)setLockRects:(BOOL)val {
    lockRects=NO;
}

-(void)setScrolling { //arricchire con tutti i check del caso
    NSRect mySize,visibleSize;
    NSPoint scrollPoint;
    
    mySize=[self frame];
    visibleSize=[[self enclosingScrollView] documentVisibleRect];

    [[ARRmainWindowController window] disableFlushWindow];
    scrollPoint.y=mySize.size.height-visibleSize.size.height*numeroPagina;
    scrollPoint.x=0;
    if (scrollPoint.y<0) scrollPoint.y=0;
    [[[self enclosingScrollView] contentView] scrollToPoint:scrollPoint];
    [ARRinterfaceDriver updateButtons];
    [[ARRmainWindowController window] enableFlushWindow];
}

//-(void)drawRect:(NSRect)rect
//{
//    NSRect		dovePasto;
//    NSMutableArray	*newArray;
//    unsigned		howMany;
//    int         attuale;
//    appuntamento	*appuntamentoAttuale;
//    NSRect		buttonRect;
//    NSTrackingRectTag	tempTrackingRectTag;
//    NSNumber		*tempNumber;
//    int			nowRect;
//    NSRect		presentRect;
//
//    dovePasto.origin.x=0;
//    dovePasto.origin.y=0;
//    
//    if ([self inLiveResize]==NO) {
//    //    NSLog(@"Non in live Resize");
//      //  [self setScrolling];
//        //Controlliamo se ci sono state delle variazioni ed � necessario un redraw
//        // English Translation:                                                 //leg20180326 - 1.2.0
//        //  We check if there have been changes and you need a redraw
//        //if (progressiveNumber<[[ARRagenda progressiveNumber] unsignedIntValue])
//        if (modificaTotaleInterfaccia==YES)
//        {
//       //     NSLog(@"Modifica totale interfaccia");
//            modificaTotaleInterfaccia=NO;
//            //[self setScrolling];
//
//            dovePasto=[self bounds];
//            progressiveNumber=[[ARRagenda progressiveNumber] unsignedIntValue];
//            //in questo caso ridisegnamo tutto, perch� � variato
//            // English Translation:                                             //leg20180326 - 1.2.0
//            //  We redesign everything in this case, because it is varied
//            [liveResizeBuffer compositeToPoint:dovePasto.origin operation:NSCompositeCopy];
//        } else {
//        //    NSLog(@"Modifica parziale interfaccia");
//            [liveResizeBuffer compositeToPoint:rect.origin fromRect:rect operation:NSCompositeCopy];
//        }
//
//        if (lockRects==NO) {
//        //Adesso ricostruisce le rect...
//        // English Translation:                                                 //leg20180326 - 1.2.0
//        //  Now reconstructs the rect ...
//        //
//        // Issue #005 solution:                                                 //leg20180327 - 1.2.0
//        //
//        // Variable "tempTrackingRectTag" is used for storage of the
//        //  NSTrackingRectTag. NSTrackingRectTag is typedef'd as an NSInteger.
//        //  In 32-bit Architectures, NSInteger is typedef'd as an int, but
//        //  in 64-bit enviroments, is typedef'd as a long.  Therefore, in
//        //  64-bit enviroments, there is a loss of precision in storing and
//        //  retreiving NSTrackingRectTags using:
//        //          tempTrackingRectTag=[tempNumber intValue];
//        //  and
//        //          tempNumber=[NSNumber numberWithInt:tempTrackingRectTag];
//        //
//        //  Fix was to use "long" variants of these member functions.
//        //
//            howMany=[trackingRects count];
//            for (nowRect=(howMany-1); nowRect>=0; nowRect--) {
//                tempNumber=[trackingRects objectAtIndex:nowRect];
////                tempTrackingRectTag=[tempNumber intValue];
//                tempTrackingRectTag=[tempNumber longValue];                     //leg20180327 - 1.2.0
//                [self removeTrackingRect:tempTrackingRectTag];
//                [trackingRects removeObjectAtIndex:nowRect];
//            }
//            [safeguard removeAllObjects];
//            
//            newArray=[ARRagenda dataArray];
//
//            howMany=[newArray count];
//            for(attuale=(howMany-1); attuale>=0; attuale--) {
//                appuntamentoAttuale=[newArray objectAtIndex:attuale];
//                if ([appuntamentoAttuale isDeleted]==NO) {
//                    buttonRect.origin=[appuntamentoAttuale posizione];
//                    buttonRect.size.width=200;
//                    buttonRect.size.height=39;
//
//                    tempTrackingRectTag=[self addTrackingRect:buttonRect owner:self userData:appuntamentoAttuale assumeInside:NO];
////                    tempNumber=[NSNumber numberWithInt:tempTrackingRectTag];
//                    tempNumber=[NSNumber numberWithLong:tempTrackingRectTag];   //leg20180327 - 1.2.0
//                    [trackingRects addObject:tempNumber];
//                    [safeguard addObject:appuntamentoAttuale]; //Cosi' invia un retain!!!
//                }
//            }
//        }
//
//        
//    } else { //qui siamo in live resize
//        if (shouldSlide==NO&&shouldSlideAdd==NO) {
//   //         NSLog(@"Live Resize");
//            [liveResizeBuffer compositeToPoint:rect.origin fromRect:rect operation:NSCompositeCopy];
//        }
//
//        if (shouldSlide==YES) {
//            [dynamicBottom compositeToPoint:dovePasto.origin operation:NSCompositeCopy];
//            dovePasto.size=[dynamicTop size];
//            presentRect=[ARRsmartScrollView bounds];
//            dovePasto.origin.y=presentRect.size.height-dovePasto.size.height-0;
//            [dynamicTop compositeToPoint:dovePasto.origin operation:NSCompositeCopy];
//        }
//
//        if (shouldSlideAdd==YES) {
//            [dynamicBottom compositeToPoint:dovePasto.origin operation:NSCompositeCopy];
//            dovePasto.size=[dynamicTop size];
//            presentRect=[ARRsmartScrollView bounds];
//            dovePasto.origin.y=presentRect.size.height-dovePasto.size.height-0;
//            [dynamicTop compositeToPoint:dovePasto.origin operation:NSCompositeCopy];
//        }
//    }
//}

//
// Merge in TopXNotes/QuickNotes "�Invalid NSTrackingRect" crash fix by         //leg20240415 - 1.4.0
//  replacing function.
//
-(void)drawRect:(NSRect)rect
{
    NSRect        dovePasto;
    NSMutableArray    *newArray;
    unsigned        howMany;
    int         attuale;
    appuntamento    *appuntamentoAttuale;
    NSRect        buttonRect;
    NSTrackingRectTag    tempTrackingRectTag;
    NSNumber        *tempNumber;
    int            nowRect;
    NSRect        presentRect;

    dovePasto.origin.x=0;
    dovePasto.origin.y=0;
    
    if ([self inLiveResize]==NO) {
    //    NSLog(@"Non in live Resize");
      //  [self setScrolling];
        //Controlliamo se ci sono state delle variazioni ed � necessario un redraw
        // English Translation:                                                 //leg20180326 - 1.2.0
        //  We check if there have been changes and you need a redraw
        //if (progressiveNumber<[[ARRagenda progressiveNumber] unsignedIntValue])
        if (modificaTotaleInterfaccia==YES)
        {
       //     NSLog(@"Modifica totale interfaccia");
            modificaTotaleInterfaccia=NO;
            //[self setScrolling];

            dovePasto=[self bounds];
            progressiveNumber=[[ARRagenda progressiveNumber] unsignedIntValue];
            //in questo caso ridisegnamo tutto, perch� � variato
            // English Translation:                                             //leg20180326 - 1.2.0
            //  We redesign everything in this case, because it is varied
            [liveResizeBuffer compositeToPoint:dovePasto.origin operation:NSCompositeCopy];
        } else {
        //    NSLog(@"Modifica parziale interfaccia");
            [liveResizeBuffer compositeToPoint:rect.origin fromRect:rect operation:NSCompositeCopy];
        }

        if (lockRects==NO) {
        //Adesso ricostruisce le rect...
        // English Translation:                                                 //leg20180326 - 1.2.0
        //  Now reconstructs the rect ...
        //
        // Issue #005 solution:                                                 //leg20180327 - 1.2.0
        //
        // Variable "tempTrackingRectTag" is used for storage of the
        //  NSTrackingRectTag. NSTrackingRectTag is typedef'd as an NSInteger.
        //  In 32-bit Architectures, NSInteger is typedef'd as an int, but
        //  in 64-bit enviroments, is typedef'd as a long.  Therefore, in
        //  64-bit enviroments, there is a loss of precision in storing and
        //  retreiving NSTrackingRectTags using:
        //          tempTrackingRectTag=[tempNumber intValue];
        //  and
        //          tempNumber=[NSNumber numberWithInt:tempTrackingRectTag];
        //
        //  Fix was to use "long" variants of these member functions.
        //
            howMany=[trackingRects count];
            for (nowRect=(howMany-1); nowRect>=0; nowRect--) {
                tempNumber=[trackingRects objectAtIndex:nowRect];
//                tempTrackingRectTag=[tempNumber intValue];
                tempTrackingRectTag=[tempNumber longValue];                     //leg20180327 - 1.2.0
                if (tempTrackingRectTag!=0)                                     //leg20240118 - TopXNotes_Catalyst_v3.0.0
                    [self removeTrackingRect:tempTrackingRectTag];
                
                // Another fix for: An uncaught exception was raised - 0x0 is   //leg20240118 - TopXNotes_Catalyst_v3.0.0
                //  an invalid NSTrackingRectTag. Common possible reasons for
                //  this are: 1. already removed this trackingRectTag,
                //  2. Truncated the NSTrackingRectTag to 32bit at some point.
                //
                // Insure removed NSTrackingRectTag is not removable again.
                tempTrackingRectTag = 0;
                
                [trackingRects removeObjectAtIndex:nowRect];
            }
            [safeguard removeAllObjects];
            
            newArray=[ARRagenda dataArray];

            howMany=[newArray count];
            for(attuale=(howMany-1); attuale>=0; attuale--) {
                appuntamentoAttuale=[newArray objectAtIndex:attuale];
                if ([appuntamentoAttuale isDeleted]==NO) {
                    buttonRect.origin=[appuntamentoAttuale posizione];
                    buttonRect.size.width=200;
                    buttonRect.size.height=39;

                    tempTrackingRectTag=[self addTrackingRect:buttonRect owner:self userData:appuntamentoAttuale assumeInside:NO];
//                    tempNumber=[NSNumber numberWithInt:tempTrackingRectTag];
                    tempNumber=[NSNumber numberWithLong:tempTrackingRectTag];   //leg20180327 - 1.2.0
                    [trackingRects addObject:tempNumber];
                    [safeguard addObject:appuntamentoAttuale]; //Cosi' invia un retain!!!
                }
            }
        }

        
    } else { //qui siamo in live resize
        if (shouldSlide==NO&&shouldSlideAdd==NO) {
   //         NSLog(@"Live Resize");
            [liveResizeBuffer compositeToPoint:rect.origin fromRect:rect operation:NSCompositeCopy];
        }

        if (shouldSlide==YES) {
            [dynamicBottom compositeToPoint:dovePasto.origin operation:NSCompositeCopy];
            dovePasto.size=[dynamicTop size];
            presentRect=[ARRsmartScrollView bounds];
            dovePasto.origin.y=presentRect.size.height-dovePasto.size.height-0;
            [dynamicTop compositeToPoint:dovePasto.origin operation:NSCompositeCopy];
        }

        if (shouldSlideAdd==YES) {
            [dynamicBottom compositeToPoint:dovePasto.origin operation:NSCompositeCopy];
            dovePasto.size=[dynamicTop size];
            presentRect=[ARRsmartScrollView bounds];
            dovePasto.origin.y=presentRect.size.height-dovePasto.size.height-0;
            [dynamicTop compositeToPoint:dovePasto.origin operation:NSCompositeCopy];
        }
    }
}

-(void)displayAll
{
    /*  NSMutableArray	*newArray;
    unsigned		howMany;
    int			yAttuale;
    int			attuale;
    appuntamento	*appuntamentoAttuale;
    int			nowRect;
    NSRect		buttonRect;
    NSTrackingRectTag	tempTrackingRectTag;
    NSNumber		*tempNumber;
    NSRect		dovePasto;

    //Adesso decidiamo se il resize � realtime!!! O � solo un redraw...
    if ([self inLiveResize]==NO) {
        NSLog(@"BF draw no live resize");

        newArray=[ARRagenda dataArray];

        howMany=[newArray count];
        yAttuale=0;

        //rimuoviamo tutti i trackingRects.
        howMany=[trackingRects count];
        for (nowRect=(howMany-1); nowRect>=0; nowRect--)
        {
            tempNumber=[trackingRects objectAtIndex:nowRect];
            tempTrackingRectTag=[tempNumber intValue];
            [self removeTrackingRect:tempTrackingRectTag];
            [trackingRects removeObjectAtIndex:nowRect];
        }

        howMany=[newArray count];
        yAttuale=0;
        [liveResizeBuffer lockFocus];
        for(attuale=(howMany-1); attuale>=0; attuale--)
        {
            //partiamo dal fondo
            appuntamentoAttuale=[newArray objectAtIndex:attuale];
            buttonRect.origin=[appuntamentoAttuale posizione];
            buttonRect.size.width=200;
            buttonRect.size.height=39;
            //lo disegnamo


            [[appuntamentoAttuale selfImage] compositeToPoint:buttonRect.origin operation:NSCompositeCopy];


            tempTrackingRectTag=[self addTrackingRect:buttonRect owner:self userData:appuntamentoAttuale assumeInside:NO];
            tempNumber=[NSNumber numberWithInt:tempTrackingRectTag];

            [trackingRects addObject:tempNumber];

        }
        [liveResizeBuffer unlockFocus];
    } else NSLog(@"LIVE RESIZE");
    dovePasto=[self bounds];
    [liveResizeBuffer compositeToPoint:dovePasto.origin operation:NSCompositeCopy];*/
}

-(void)viewWillStartLiveResize {
    [self setScrolling];
    [super viewWillStartLiveResize];
}

-(void)viewDidEndLiveResize {
    [self setNeedsDisplay:YES];
    [super viewDidEndLiveResize];
}

-(int)numeroPagina {
    return numeroPagina;
}

-(void)setNumeroPagina:(int)pag {
    if (pag>0&&pag<=[self maxPages]) {
        numeroPagina=pag;
        [self setScrolling];
        [self setNeedsDisplay:YES];
    }
}

-(int)maxPages {
    NSRect	visibleRect,totalRect;
    float	delta;

    totalRect=[self bounds];
    visibleRect=[ARRsmartScrollView documentVisibleRect];
    delta=totalRect.size.height/(visibleRect.size.height+1); //il 5 � per sicurezza
    return (int)ceil((double)delta);
}


-(void)dealloc {
    [trackingRects release];
    [liveResizeBuffer release];
    [super dealloc];
}

-(BOOL)canBecomeFirstResponder {
    return YES;
}

-(BOOL)becomeFirstResponder {
    return YES;
}

-(BOOL)resignFirstResponder {
    return YES;
}

-(BOOL)acceptsFirstResponder {
    return YES;
}

-(BOOL)acceptsFirstMouse:(NSEvent *)e {
    return YES;
}
@end
