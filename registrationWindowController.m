//
//  registrationWindowController.m
//
//          2Remember- A simple reminder program

//  2Remember
//
//
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//

#import "registrationWindowController.h"
#import <CoreFoundation/CoreFoundation.h>
#import "globals.h"
#import "AppController.h"

@implementation registrationWindowController

static registrationWindowController *sharedInstance=nil;

+ (registrationWindowController *)sharedInstance {
    return sharedInstance ? sharedInstance : [[self alloc] init];
}

//questa � l'unica istanza...
-(id)init {
    if (sharedInstance) {
        [self dealloc];
    } else {
        sharedInstance =[super init];
    }
    return sharedInstance;
}

-(IBAction)showPanel:(id)sender {
    if ([self codiceValido]==NO) {
        //controlla se � caricata...
        if (!nome) {
            NSWindow *theWindow;

            if (![NSBundle loadNibNamed:@"Registration" owner:self]) {
                NSLog(@"Failed to load Registration.nib");
                //termina!!!
                NSRunAlertPanel(@"There is a problem:", @"Your copy of 2Remember might be damaged. Please restore it from a backup copy, or download a new copy from the website.", @"Ok", nil, nil);
                [NSApp terminate:self];
                return;
            }
            theWindow= [self window];

            [theWindow setExcludedFromWindowsMenu:YES];
            [theWindow setMenu:nil];

        }
        NSRect dove=[[self window] frame];
        dove.size.width=267;
        dove.size.height=138;
        [[self window] setFrame:dove display:NO];
        [[self window] orderFront:nil];
    }
}

-(BOOL) codiceValido {
    NSString *stringaNome=[[NSUserDefaults standardUserDefaults] stringForKey:@"2RememberName"];
    NSString *stringaCodice=[[NSUserDefaults standardUserDefaults] stringForKey:@"2RememberSerial"];

    const char *locNome=[stringaNome lossyCString];
    const char *locCode=[[stringaCodice lowercaseString] lossyCString];

    double nomeSum=0;
    double risultato=0;

    double serie=fmod((double)[[NSCalendarDate calendarDate] monthOfYear],3);

    if ([stringaCodice length]==8 && [stringaNome length]>=8) {
        if (locCode[0]==97||locCode[0]==98) {

            if (serie==0) {
                int ind=0;
                while (locNome[ind]!=nil) {
                    nomeSum=fmod(nomeSum+(double)locNome[ind],99);
                    ind++;
                }
            } else if (serie==1) {
                int ind=0;
                while (locNome[ind]!=nil) {
                    nomeSum=fmod(nomeSum+(double)locNome[ind],79);
                    ind++;
                }
            } else {
                int ind=0;
                while (locNome[ind]!=nil) {
                    nomeSum=fmod(nomeSum+(double)locNome[ind],87);
                    ind++;
                }
            }
            //adesso andiamo a calcolare il codice (per qualsiasi ceck=
            //locNome=[stringaNome lossyCString];
            int ind=0;
            if (nomeSum>0) {
                while (locNome[ind]!=nil) {
                    risultato=fmod(risultato+(double)locNome[ind],nomeSum);
                    ind++;
                }
            } else risultato=0;

            char r1=(char)(fmod(risultato,10)+48);
            char r2=(char)(((risultato-(double)r1+48)/10)+48);
            if (serie==0) {
                if (r2==locCode[1]&&r1==locCode[2]) return YES;
            } else if (serie==1) {
                if (r2==locCode[3]&&r1==locCode[4]) return YES;
            } else if (serie==2) {
                if (r2==locCode[5]&&r1==locCode[6]) return YES;
            }
        }
    }
    return NO;
}

-(BOOL) checkSumValido {
    const char *locNome=[[[nome stringValue] lowercaseString] lossyCString];
    const char *locCode=[[[codice stringValue] lowercaseString] lossyCString];

    double nomeSum=0;
    double codeSum=0;

    if ([[codice stringValue] length]==8 && [[nome stringValue] length]>=8) {
        if (locCode[0]==97) {
            //calcolo checksum nome
            int ind=0;
            while (locNome[ind]!=nil) {
                nomeSum=fmod(nomeSum+(double)locNome[ind],10);
                ind++;
            }
            //Bega: Codice tier 1
            ind=0;
            for (ind=1;ind<7;ind++) {
                codeSum=fmod(nomeSum+(double)locCode[ind],10);
            }
            
            double totale=codeSum+nomeSum;
            char codice2=(char)(97+fmod(totale,10));
            if (codice2==locCode[7]) return YES;
        }
        
        if (locCode[0]==98) {
            //calcolo checksum nome
            int ind=0;
            while (locNome[ind]!=nil) {
                nomeSum=fmod(nomeSum+(double)locNome[ind],10);
                ind++;
            }
 
            for (ind=1;ind<7;ind++) {
                codeSum=fmod(codeSum+(double)locCode[ind],10);
            }

            double totale=codeSum+nomeSum;
            char codice2=(char)(97+fmod(totale,10));
            if (codice2==locCode[7]) return YES;
        }
    }
    return NO;
}

- (IBAction)okButton:(id)sender {
    if ([self checkSumValido]==YES) {
        //salviamo

        CFStringRef myName = CFSTR("2RememberName");
        CFStringRef nameCode = (CFStringRef)[nome stringValue];

        CFPreferencesSetValue(myName, nameCode, kCFPreferencesCurrentApplication, kCFPreferencesAnyUser, kCFPreferencesCurrentHost);

        CFStringRef myCode = CFSTR("2RememberSerial");
        CFStringRef codeCode = (CFStringRef)[codice stringValue];

        CFPreferencesSetValue(myCode, codeCode, kCFPreferencesCurrentApplication, kCFPreferencesAnyUser, kCFPreferencesCurrentHost);

        [[self window] close];
        [ARRappController setRegisterMenu:NO];
        //[[self window] release];
    }
}

- (IBAction)modificaTesto:(id)sender {
    if ([self checkSumValido]==YES) {
        [vista selectTabViewItemAtIndex:3];
    }
}

- (IBAction)website:(id)sender {
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://www.tropic4.com/"]];
}

-(void)dealloc {
    [super dealloc];
}
@end
