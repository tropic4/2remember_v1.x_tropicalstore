//
//  agenda.h
// 2Remember
//
//
//  Created by Andrea Rincon Ray on Wed Feb 26 2003.
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//

#import <Foundation/Foundation.h>
@class appuntamento;

@interface agenda : NSObject {
    NSMutableArray 	*appuntamenti;
    unsigned int	progressiveNumber;
    appuntamento	*selected;
    NSString		*standardDataPath;
    BOOL		errorsWithArchive;
    BOOL		savingEnabled;
    BOOL		autoResort;
    int			lockResorting;
}
-(void)mightNeedResorting;
-(void)load;

-(NSNumber*)progressiveNumber;
-(NSMutableArray*)dataArray;
-(void)toggleSelected:(appuntamento*)appunt;
-(void)setSelected:(appuntamento*)appunt;
-(appuntamento*)selected;
-(void)deleteSelected;
-(appuntamento*)addNew;
-(void)save;

-(void)inconsistentArchiveVersion;
-(void)lockResortingUP;
-(void)lockResortingDOWN;
-(void)shouldSaveAsap;
-(void)selectPreviousObject;
-(void)selectNextObject;
//    int intSort(id num1, id num2, void *context);
// Fixed "Incompatible function pointer types…": return from intSort() was      //leg20240416 - 1.4.0
//  defined as "int" instead of "NSInteger". Changed function prototype.
    NSInteger intSort(id num1, id num2, void *context);
@end
