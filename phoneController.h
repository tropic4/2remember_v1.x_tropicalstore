//
//  phoneController.h
//
//          2Remember- A simple reminder program

//  2Remember
//
//  Created by Andrea Rincon Ray on Tue Apr 08 2003.
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//

#import <AppKit/AppKit.h>


@interface phoneController : NSWindowController {
    NSString	*stringa;
    IBOutlet	id	testo;
}
+ (phoneController *)sharedInstance;
-(void)showWithString:(NSString*)stri ;
-(IBAction)showPanel:(id)sender;
@end
