#import "MainWindowSubclass.h"
#import "globals.h"
#import "InterfaceDriver.h"
#import "agenda.h"
#import "AppController.h"



@implementation MainWindowSubclass
-(void)update {
}

- (id)initWithContentRect:(NSRect)contentRect styleMask:(unsigned int)aStyle backing:(NSBackingStoreType)bufferingType defer:(BOOL)flag {
//- (id)initWithContentRect:(NSRect)contentRect styleMask:(enum NSWindowStyleMask)aStyle backing:(NSBackingStoreType)bufferingType defer:(BOOL)flag { //leg20180405 - 1.2.0

    //OK... adesso siamo in gioco... Partiamo creando una nuova window di tipo borderless...
    //OK... now we're in the game... Let's start by creating a new borderless window... //leg20230914 - Translation
    NSWindow* result = [super initWithContentRect:contentRect styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:NO]; //Valutare defer.
    //Il Background � trasparente...
    [result setBackgroundColor: [NSColor clearColor]];
    //Cosi' non � piu' opaca...
    [result setOpaque:NO];
    //Confermiamo che la finestra ha un'ombra...
    [result setHasShadow: YES];
    //Settiamo il livello della finestra.
    //We set the window level.                                                  //leg20230914 - Translation
    if (ARRfrontmostRT==YES) {
        [self setLevel:NSModalPanelWindowLevel];
    } else {
        [self setLevel:NSNormalWindowLevel];
    }
    //Ritorniamo il risultato.
//    return result;
    return (MainWindowSubclass*)result;                                         //leg20180319 - 1.2.0
}

//Confermato e testato.
//Confirmed and tested.                                                         //leg20230914 - Translation
- (void)mouseDragged:(NSEvent *)theEvent
{
    NSPoint	currentLocation;
    NSPoint	newOrigin;
    NSPoint	thePoint;
    NSRect 	screenFrame = [[self screen] visibleFrame]; //schermo attuale!!!
    NSRect 	windowFrame = [self frame];
    int		whereToDock;

    //grab the current global mouse location; we could just as easily get the mouse location
    //in the same way as we do in -mouseDown:
    currentLocation = [self convertBaseToScreen:[self mouseLocationOutsideOfEventStream]];
//// convertBaseToScreen deprecated, use convertRectToScreen instead.                     //leg20180405 - 1.2.0
//    NSRect mouseLocationRect = NSMakeRect([self mouseLocationOutsideOfEventStream].x,   //leg20180405 - 1.2.0
//                                          [self mouseLocationOutsideOfEventStream].y,   //leg20180405 - 1.2.0
//                                          0.0, 0.0);                                    //leg20180405 - 1.2.0
//    NSRect currentLocationRect = [self convertRectToScreen:mouseLocationRect];          //leg20180405 - 1.2.0
//    currentLocation = NSMakePoint(currentLocationRect.origin.x,                         //leg20180405 - 1.2.0
//                                  currentLocationRect.origin.y);                        //leg20180405 - 1.2.0
    newOrigin.x = currentLocation.x - initialLocation.x;
    newOrigin.y = currentLocation.y - initialLocation.y;

    whereToDock=0;
    // Test sul top
    // Test on the top                                                          //leg20230914 - Translation
    if( (newOrigin.y+windowFrame.size.height) > (screenFrame.origin.y+screenFrame.size.height) ){
        thePoint.x=newOrigin.x;
        thePoint.y=newOrigin.y+windowFrame.size.height;
        if ([self outsideOfScreen:thePoint]) {
            newOrigin.y=screenFrame.origin.y + (screenFrame.size.height-windowFrame.size.height);
            //icona dock top! ->Non c'� (per ora) Ma considerare!
            //top dock icon! ->Not there (for now) But consider!                //leg20230914 - Translation
        }
    }

    //Test su bottom...
    //Test on bottom...                                                         //leg20230914 - Translation
    if (newOrigin.y<(screenFrame.origin.y)){
        thePoint.x=newOrigin.x;
        thePoint.y=newOrigin.y;
        if ([self outsideOfScreen:thePoint]){
            newOrigin.y=screenFrame.origin.y;
            whereToDock=3;//bottom
        }
    }

    //Test Sulla SX...
    //Test on the SX...                                                         //leg20230914 - Translation
    if (newOrigin.x+30<screenFrame.origin.x){
        thePoint.x=newOrigin.x+30;
        thePoint.y=newOrigin.y;

        if ([self outsideOfScreen:thePoint]) {
            newOrigin.x=screenFrame.origin.x-30;
            //Docking a sinistra!!!
            //Docking left!!!                                                   //leg20231024 - Translation
            whereToDock=1;//SX
        }
    }
    //Test sulla DX...
    //Test on the DX...                                                         //leg20230914 - Translation
    if ((newOrigin.x+windowFrame.size.width-30)>(screenFrame.origin.x+screenFrame.size.width)) {
        thePoint.x=newOrigin.x+windowFrame.size.width-30;
        thePoint.y=newOrigin.y;
        if ([self outsideOfScreen:thePoint]){
            newOrigin.x=screenFrame.origin.x+screenFrame.size.width-windowFrame.size.width+30;
            //Docking right!!!                                                   //leg20231024 - Translation
            whereToDock=2;//DX
        }
    }

    [self dockingRealTimeProcessor:whereToDock];

    //go ahead and move the window to the new location
    [self setFrameOrigin:newOrigin];
}

//We start tracking the a drag operation here when the user first clicks the mouse,
//to establish the initial location.
- (void)mouseDown:(NSEvent *)theEvent //Ok Confermato
{
    NSRect  windowFrame = [self frame];

    nc=[NSNotificationCenter defaultCenter];
    configurazioneSchermi=[NSScreen screens];
    //grab the mouse location in global coordinates
    initialLocation = [self convertBaseToScreen:[theEvent locationInWindow]];
//// convertBaseToScreen deprecated, use convertRectToScreen instead.             //leg20180405 - 1.2.0
//    NSRect locationInWindowRect = NSMakeRect([theEvent locationInWindow].x,     //leg20180405 - 1.2.0
//                                             [theEvent locationInWindow].y,     //leg20180405 - 1.2.0
//                                             0.0, 0.0);                         //leg20180405 - 1.2.0
//    NSRect initalLocationRect = [self convertRectToScreen:locationInWindowRect];//leg20180405 - 1.2.0
//    initialLocation = NSMakePoint(initalLocationRect.origin.x,                  //leg20180405 - 1.2.0
//                                  initalLocationRect.origin.y);                 //leg20180405 - 1.2.0
    
    initialLocation.x -= windowFrame.origin.x;
    initialLocation.y -= windowFrame.origin.y;
}

-(void)mouseUp:(NSEvent *)theEvent //Ok Confermato
{
    //Aggiorna il livello della finestra!
    //Update window layer!                                                      //leg20230914 - Translation
    if (ARRfrontmostRT==YES) {
        [self setLevel:NSModalPanelWindowLevel];
    } else {
        [self setLevel:NSNormalWindowLevel];
    }

    //informiamo che la finestra ha ricevuto un click!
    //we inform you that the window has received a click!                       //leg20230914 - Translation
    [ARRappController mainInterfaceDispatch:4];
}

//determina se un punto � interno o esterno agli schermi.
//determines whether a point is inside or outside the screens.                  //leg20230914 - Translation
-(BOOL)outsideOfScreen:(NSPoint)position //OK Confermato
{
//    int    screenNumber;
    NSInteger    screenNumber;                                                  //leg20180405 - 1.2.0
    BOOL	result=YES;
    int	i;
    screenNumber=[configurazioneSchermi count];

    for (i=0; i<screenNumber; i++)
    {
        //� dentro allo schermo.
        frameSchermo=[[configurazioneSchermi objectAtIndex:i] visibleFrame];
        if (position.x>=frameSchermo.origin.x&&position.x<=frameSchermo.origin.x+frameSchermo.size.width&&position.y>=frameSchermo.origin.y&&position.y<=frameSchermo.origin.y+frameSchermo.size.height)
        {
            result=NO;
        }
    }
    return result;
}

-(void)dockingRealTimeProcessor:(int)selection //Ok No proble.
{
    switch (selection)
    {
        case 0: //NO
            ARRdockedRT=NO;
            ARRleftRT=NO;
            ARRbottomRT=NO;
            ARRfrontmostRT=NO;
            [nc postNotificationName:@"ARRhideDockImage" object:self];
            break;
        case 1: //left
            ARRdockedRT=YES;
            ARRleftRT=YES;
            ARRbottomRT=NO;
            ARRfrontmostRT=YES;
            [nc postNotificationName:@"ARRshowDockImage" object:self];
            break;

        case 2: //right
            ARRdockedRT=YES;
            ARRleftRT=NO;
            ARRbottomRT=NO;
            ARRfrontmostRT=YES;
            [nc postNotificationName:@"ARRshowDockImage" object:self];
            break;
        case 3: //bottom
            ARRdockedRT=YES;
            ARRleftRT=NO;
            ARRbottomRT=YES;
            ARRfrontmostRT=YES;
            [nc postNotificationName:@"ARRshowDockImage" object:self];
            break;
        default:
        NSLog(@"MainWindowSubclass:DockingRealTimeProcessor:select-default");
    }
}

-(void) becomeKeyWindow {
    //[self makeFirstResponder:ARRbuttonField];
}

- (BOOL)canBecomeKeyWindow {
    return YES;
}

- (void)sendEvent:(NSEvent *)theEvent
{
    NSEventType	cheTipo;
    unichar theChar;


    cheTipo=[theEvent type];
    if (cheTipo==NSKeyDown) {
        theChar=[[theEvent characters] characterAtIndex:0];

        if (theChar==NSUpArrowFunctionKey) [ARRagenda selectPreviousObject];
        else if (theChar==NSDownArrowFunctionKey) [ARRagenda selectNextObject];
        else if (theChar==13||theChar==3) [ARRinterfaceDriver enterKey];
        else if (theChar==127) [ARRinterfaceDriver deleteKey];
        else [super sendEvent:theEvent];
    }
    else [super sendEvent:theEvent];
    
}
@end
