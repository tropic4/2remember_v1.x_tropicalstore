//
//  DataWindowController.m
// 2Remember
//
//
//  Created by Andrea Rincon Ray on Tue Mar 11 2003.
//  Version 1.0-1.0.5 by Andrea Rincon Ray on or beforer Sun Feb 09 2003.
//  Versions after 1.0.6 by James Lee and Lewis Garrett, Tropical Software, Inc. 

//  2Remember
//  Copyright (c) 2011 Tropical Software, Inc. All rights reserved.
//

#import "DataWindowController.h"
#import "appuntamento.h"
#import "MainWindowController.h"
#import "ButtonField.h"
#import "agenda.h"
#import "DataWindowInterfaceDriver.h"

@implementation DataWindowController
-(id)init{
    NSNotificationCenter *nc;
    if(self=[super initWithWindowNibName:@"DataWindow"]) {
        [self setWindowFrameAutosaveName:@"dataWindow"];
        getNotification=YES;
        appuntamentoAttuale=nil;
        // *** Si registra nel notification center***
        nc=[NSNotificationCenter defaultCenter];
        [nc addObserver:self selector:@selector(dataChanged) name:@"ARRselectedRecordChanged" object:nil];
        [nc addObserver:self selector:@selector(dataChanged) name:@"ARRnumberOfRecordsChanged" object:nil];
    }
    return self;
}

-(void)dataChanged
{
    appuntamento	*appuntamentoInAgenda;
    //se � una nuova: salva la vecchia e apre la nuova
    //se � la stessa non fa niente
    //se � nil si chiude.
    appuntamentoInAgenda=[ARRagenda selected];
    
    if (getNotification==YES) {
        if (appuntamentoInAgenda!=nil&&[appuntamentoInAgenda isDeleted]==NO) {
            if(appuntamentoAttuale!=appuntamentoInAgenda) {
                //aggiorna
                [self saveIfNeeded];
                appuntamentoAttuale=appuntamentoInAgenda;
                [self updateUI];
            } //altrimenti non fa niente...
        } else {
            [[self window] close];
            //setta non visibile
            [ARRdataWindowInterfaceDriver isVisible:NO];
        }
    }
}

-(void)show
{
    [ARRdataWindowInterfaceDriver lockActions:YES];
    [ARRdataWindowInterfaceDriver isVisible:YES];
    [[self window] makeKeyAndOrderFront:self];
    [ARRdataWindowInterfaceDriver lockActions:NO];
}

-(void)updateUI
{
    [ARRdataWindowInterfaceDriver updateUI];
}


-(BOOL)windowShouldClose:(id)sender
{
    //Qui si DEVE deregistrare dal notification center
    if ([ARRagenda selected]!=nil) {
        getNotification=NO;
        [ARRagenda setSelected:nil];
        getNotification=YES;
    }
    [self saveIfNeeded];
    [ARRbuttonField setNeedsDisplay:YES];
    [ARRdataWindowInterfaceDriver isVisible:NO];
    return YES;
}

-(void)saveIfNeeded
{
    if (appuntamentoAttuale!=nil) {
        //salva i dati
        [ARRdataWindowInterfaceDriver saveIfNeeded];
        appuntamentoAttuale=nil;
    }
}

-(void)hide
{
    [[self window] orderOut:self];
    [ARRdataWindowInterfaceDriver saveIfNeeded];
        [ARRdataWindowInterfaceDriver isVisible:NO];
}

-(void)setData:(appuntamento*)appuntamento
{
    appuntamentoAttuale=appuntamento;
    //In piu' fa il redraw?
}

-(void)setPosition:(NSPoint)point;
{
    [[self window] setFrameTopLeftPoint:point];
}

-(void)dealloc
{
    NSNotificationCenter *nc;
    nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self];
}
@end
