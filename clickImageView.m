#import "clickImageView.h"

@implementation clickImageView
-(void)awakeFromNib {
}

- (BOOL)mouseDownCanMoveWindow {
    return YES; //interfaccia
}

-(BOOL)acceptsFirstMouse:(NSEvent *)e {
    return YES;
}


-(void)setImage:(NSImage*)image {
    if (myImage) {
        [myImage release];
    }
    [image retain];
    myImage=image;
}

-(void)fastDraw {
    NSPoint p;

    p.x=0;
    p.y=0;

    [self lockFocus];
    if (myImage) {
        [myImage compositeToPoint:p operation:NSCompositeCopy];
    }
    [self unlockFocus];
}


-(void)drawRect:(NSRect)rect {
    NSPoint p;

    p.x=0;
    p.y=0;

    if (myImage) {
        [myImage compositeToPoint:p operation:NSCompositeCopy];
    }
}

@end
