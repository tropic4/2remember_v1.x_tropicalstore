
1.4.0(7) - Lewis Garrett - Date begun work on this build: 20240415.
/Users/leg/Desktop/Git_Projects/2remember_v1.x_tropicalstore
//
// Tropical Store build.
//
// Develop Release - 1.4.0 - Build with macOS X 13.6.3 and Xcode 15.2.
//
// Set build number (CFBundleVersion) to 7.                                     //leg20240415 - 1.4.0
// Set minimum deployment to macOS 10.11.                                       //leg20240415 - 1.4.0
// Merge in TopXNotes/QuickNotes "…Invalid NSTrackingRect" crash fix by         //leg20240415 - 1.4.0
//  replacing functions:
//      OpenControl.m -updateTrackingRect
//      tabView.m -drawRect
//      ButtonField.m -drawRect
// Add "App Uses Non-Exempt Encryption" key to info.plist.                      //leg20240415 - 1.4.0
// Set Minimum Deployment to 10.11.                                             //leg20240415 - 1.4.0
// Update Copyright dates in:
//                infoPlist.plist, info.plist, ReadMe.rtf, and Help files       //leg20240415 - 1.4.0
// Accept Xcode setting recommendations:                                        //leg20240415 - 1.4.0
//                Update to recommended settings.
//                Migrate “Japanese.lproj” (Deprecated)
//                Migrate “French.lproj” (Deprecated)
//                Migrate “German.lproj” (Deprecated)
// Fixed "Incompatible function pointer types…": return from intSort() was      //leg20240416 - 1.4.0
//  defined as "int" instead of "NSInteger". Changed function prototype in
//  agenda.h to:
//    NSInteger intSort(id num1, id num2, void *context);
// Added "Tropical Store Version" text to About Credits.rtf.                    //leg20240417 - 1.4.0
//
// Update Sparkle Updater from Sparkle 1.x to Sparkle 2.x:                      //leg20240417 - 1.4.0
//   Remove Sparkle.framework v1.27.1.
//   Remove "Run Script" phase.
//   Add Swift Package Manager package:
//        https://github.com/sparkle-project/Sparkle
//          Up to next version 2.0.0 < 3.0.0
//   Add entitlement:
//      com.apple.security.temporary-exception.mach-lookup.global-name
//          <string>$(PRODUCT_BUNDLE_IDENTIFIER)-spks</string>
//          <string>$(PRODUCT_BUNDLE_IDENTIFIER)-spki</string>
//   Set Xcode Build Setting
//      LD_RUNPATH_SEARCH_PATHS = $(inherited) @executable_path/../Frameworks
//   Set Sparkle info.plist settings:
//      SUEnableAutomaticChecks=YES
//      SUFeedURL=https:www.tropic4.com/download/updates/2Remember/appcast_2ReM1.xml
//      SUScheduledCheckInterval=86400
//      SUAutomaticallyUpdate=NO
//      SUEnableInstallerLauncherService=YES
//      SUPublicEDKey=1USZ2JkUkk4/dXCgz2l85DKo9RnecVMi0ej61itL8Fw=
//   Edit 2Remember Run Scheme:
//                   Disable "XPC Servies - Debug XPC services used by app"
//   Prepare APPFEED appcast_2ReM1.xml manually.
//   Sign builds with sign-update tool.
//
// Create test APPFEED folder:
//  https:www.tropic4.com/download/updates/Test_2Remember/appcast_2ReM1.xml
// Build disk installer with DMGCanvas and add zip of installer build           //leg20240419 - 1.4.0
//  folder to this repo. Note: Did build on Catalina because DMGCanvas 2.x
//  fails on Ventura if a license is included in build. Issue is fixed
//  in DMGCanvas 4.x which requires a new license.
//  Pushed installer "2Remember 1.4.0.dmg" to tropic4.com…D2/Builds.
// Updated Help/updatelog.html.                                                 //leg20240419 - 1.4.0
//
// Prepare GM release of 2Remember 1.4.0(7):                                    //leg20240501 - 1.4.0
//   Set build number (CFBundleVersion) to 7.
//   Remake build to change SUFeedURL to production location.
//   Sign build with sign-update tool.
//   Update production APPFEED for build 7.
//   Remake DMG Canvas disk Installer: 2Remember 1.4.0 Installer.dmg.
// GM Release iAddressX 1.4.0(7).                                              //leg20240501 - 1.4.0


1.3.0(7) - Lewis Garrett - Date begun work on this build: 20230914.
/Users/leg/Desktop/Git_Projects/2remember_v1.x_tropicalstore
// Tropical Store build.
// Develop Release - 1.3.0 - Build with MacOS X 10.15.7 and Xcode 12.4.
//
// Set build number (CFBundleVersion) and sparkle:version to 7.                 //leg20230914 - 1.3.0
// Translate some comments in Italian to English.                               //leg20230914 - Translation
// Tropical Store 2Remember 1.3.0 will no longer build on any version of        //leg20230928 - 1.3.0
//  macOS and Xcode because of a "Command PhaseScriptExecution failed with a
//      nonzero exit code":
//  /Users/leg/Library/Developer/Xcode/DerivedData/……
//      /Frameworks/Sparkle.framework/Versions/A: replacing existing signature
//  /Users/leg/Library/Developer/Xcode/DerivedData/……
//      /Frameworks/Sparkle.framework/Versions/A: A timestamp was expected
//      but was not found.
// Project will need to be updated to latest Sparkle.framework and will need    //leg20230928 - 1.3.0
//  to be built with Xcode 14.2 or above.
// Translate some comments in Italian to English.                               //leg20231024 - Translation

1.3.0(6) - Lewis Garrett - Date begun work on this build: 20201222.
/Users/leg/Desktop/Git_Projects/2remember_v1.x_tropicalstore
// Tropical Store build.
// Develop Release - 1.3.0 - Build with MacOS X 10.15.7 and Xcode 12.2.
//
// Set build number (CFBundleVersion) and sparkle:version to 6.                 //leg20201222 - 1.3.0
// Alter MAS Issue #016 and #017 fixes so that windows have DarkAqua appearance.//leg20201222 - 1.3.0
// Add to 1.3.0.html "Update Error!" notice with link to tropic4.com 2Remember  //leg20201223 - 1.3.0
//  webpage for downloading update.
// Update test.appcast… to 2Remember_1.3.0(6).zip.                              //leg20201223 - 1.3.0
// GM Release 1.3.0(6):                                                         //leg20201228 - 1.3.0
//  Update production appcast… to 2Remember_1.3.0(6).zip.
//  Update tropic4.com 2Remember "Download" link with this build:
//  http://www.tropic4.com/download/2Remember.zip

1.3.0(5) - Lewis Garrett - Date begun work on this build: 20201217.
/Users/leg/Desktop/Git_Projects/2remember_v1.x_tropicalstore
// Tropical Store build.
// Develop Release - 1.3.0 - Build with MacOS X 10.15.7 and Xcode 12.2.
//
// Set build number (CFBundleVersion) and sparkle:version to 5.                 //leg20201217 - 1.3.0
// Revise Credits.rtf.                                                          //leg20201217 - 1.3.0
// Fix MAS Issue #016 - "Welcome texts were not visible in Dark mode."          //leg20201218 - 1.3.0
//  Insure "SplashWindow" Appearance is Aqua regardless of Dark Mode.
// Fix MAS Issue #017 - "About box text not visible in Dark mode."              //leg20201219 - 1.3.0
//  Insure "About Window" Appearance is Aqua regardless of Dark Mode.
// Fix Issue #017 - "The “Email” button in the About box doesn’t seem           //leg20201219 - 1.3.0
//  to do anything."
//  Adapted iAddressX's use of CFURLCreateStringByAddingPercentEscapes() to
//  prepare mailto URL.
// Update test.appcast… to 2Remember_1.3.0(5).zip.                              //leg20201219 - 1.3.0

1.3.0(4) - Lewis Garrett - Date begun work on this build: 20201215.
/Users/leg/Desktop/Git_Projects/2remember_v1.x_tropicalstore
// Tropical Store build.
// Develop Release - 1.3.0 - Build with MacOS X 10.15.7 and Xcode 12.2.
//
// Set build number (CFBundleVersion) and sparkle:version to 4.                 //leg20201215 - 1.3.0
// Update to reccommended settings after switch to Xcode 12.                    //leg20201215 - 1.3.0
// Migrate English.lproj.                                                       //leg20201215 - 1.3.0
// Changed minimum macOS Deployment Target to 10.9.                             //leg20201216 - 1.3.0
// First Universal build for Intel and Apple Silicon produced by Xcode 12.2.    //leg20201216 - 1.3.0
// Update test.appcast… to 2Remember_1.3.0(4).zip.                              //leg20201216 - 1.3.0

1.3.0(3) - Lewis Garrett - Date begun work on this build: 20201215.
/Users/leg/Desktop/Git_Projects/2remember_v1.x_tropicalstore
// Develop Release - 1.3.0 - Build with MacOS X 10.15.7 and Xcode 11.7.
// Set build number (CFBundleVersion) and sparkle:version to 3.                 //leg20201215 - 1.3.0
// Update Help index.html and updatelog.html.                                   //leg20201215 - 1.3.0
// Changed minimum macOS Deployment Target to 10.7.                             //leg20201215 - 1.3.0
// Update test.appcast… to 2Remember_1.3.0(3).zip.                              //leg20201215 - 1.3.0

1.3.0(2) - Lewis Garrett - Date begun work on this build: 20201214.
/Users/leg/Desktop/Git_Projects/2remember_v1.x_tropicalstore
// Develop Release - 1.3.0 - Build with MacOS X 10.15.7 and Xcode 11.7.
// Set build number (CFBundleVersion) and sparkle:version to 2.                 //leg20201214 - 1.3.0
// Fix Issue #016 - 2Remember multiple launches                                 //leg20201214 - 1.3.0
// Update test.appcast… to 2Remember_1.3.0(2).zip.                              //leg20201214 - 1.3.0

1.3.0(1) - Lewis Garrett - Date begun work on this build: 20201204.
/Users/leg/Desktop/Git_Projects/2remember_v1.x_tropicalstore
// Develop Release - 1.3.0 - Build with MacOS X 10.15.7 and Xcode 11.7.
// Set build number (CFBundleVersion) and sparkle:version to 1.                 //leg20201204 - 1.3.0
// Implemented Issue #5 "Add ReadMe Menu Item".                                 //leg20201204 - 1.3.0
// Fix Issue #013 - AutoFill Not Working                                        //leg20201209 - 1.3.0
//  Added missing NSContactsUsageDescription (“Privacy - Contacts Usage
//  Description“) key in info.plist
// Update Sparkle to version 1.24.                                              //leg20201209 - 1.3.0
// Fix Issue #014: Sparkle update stalls during "Install and Relaunch"          //leg20201209 - 1.3.0
//  Removed Sandboxing and changed code signing to Manual Developer ID.
// Fix Issue #016 - 2Remember multiple launches                                 //leg20201214 - 1.3.0

1.3.0(0) - Lewis Garrett - Date begun work on this build: 20201201.
/Users/leg/Desktop/Git_Projects/2remember_v1.x_tropicalstore
// Develop Release - 1.3.0 - Build with MacOS X 10.15.7 and Xcode 11.7.
// Set build number (CFBundleVersion) and sparkle:version to 0.                 //leg20201201 - 1.3.0
// Set Hardened Runtime=NO for Project until we need to Notarize a build.       //leg20201201 - 1.3.0
// Sandboxed 2Remember TS.                                                      //leg20201201 - 1.3.0
// Updated .nib files with High Sierra/Xcode 9.2 so they open with Xcode 11.7   //leg20201202 - 1.3.0
//  Fixes Issue #10: About Box cannot be displayed
// Replace deprecated loadNibNamed:owner method with                            //leg20201202 - 1.3.0
//  loadNibNamed:owner:topLevelObjects.
// Create test appcast "test_appcast_2ReM1.xml".                                //leg20201202 - 1.3.0
// Update Sparkle to version 1.23.                                              //leg20201203 - 1.3.0
// Set Hardened Runtime=YES to Notarize a build.                                //leg20201203 - 1.3.0
// Update infoPlist.strings with current release and copyright info.            //leg20201203 - 1.3.0
// Build bootstrap build 0 with test appcast URL.                               //leg20201203 - 1.3.0

/Current/Git_Projects/2remember_v1.x_tropicalstore, LEG Mar 16, 2018 - Xcode 9.2/MacOSX 10.13.3
- TS Build 1.2.1 (1.2.1)  AS Build 1.2.0(3)
-
-  TS Version 1.2.1
-
- Merge AS 2Remember 1.2.0 changes to TS 2Remember 1.2.1:
-       Build for 64-bit Architecture Machines to satisfy Apple's mandate.
-       Retrofitted -setWillAppLaunchAtLogin from iAddressX to replace autoStartup.h/c to support adding 2Remember to Login items at macOS start-up.  Tagged //leg20180307 - 1.2.0
- 	Add Auto Fill-in feature to Trip Reminder's Goto Where text field.  Tagged //leg20180309 - 1.2.0
-	Change display of Version in About Box to include Build number.  Tagged //leg20180313 - 1.2.0
-	Update Copyright and Version info in info.plist .  Tagged //leg20180314 - 1.2.0
- 	Update Copyright dates in Help files.  

2Remember1.2.0b0_TS_Proj-20141016
- Begin Tropical Store version.  tagged //leg20141016 - 1.2.0
- Disable original 2Remember registration code so that Tropical's registration/update system can be implemented.
- Add product key handling code.
- Add Sparkle updating framework.  Had to rebuild Sparkle updating framework from Sparkle-master 1.5 Beta (Git) because version previously used did not support code signing of the update archive.  See:
https://github.com/sparkle-project/Sparkle/wiki/Publishing-An-Update
http://stackoverflow.com/questions/24981019/sparkle-appcast-signing-the-update-is-improperly-signed
http://stackoverflow.com/questions/21254574/how-to-sign-your-update-in-sparkle
-
- Sign with Developer ID
-
- Developed with MacOS X 10.10 (Yosemite) and Xcode 6.1.  Base SDK: 10.6, OS X Deployment Target: 10.5.


CFBundleShortVersionString = "1.2.1";
CFBundleGetInfoString = "2Remember version 1.2, Copyright 2011-2018 Tropical Software, Inc.";
NSHumanReadableCopyright = "Copyright 2011-2018 Tropical Software, Inc.";

