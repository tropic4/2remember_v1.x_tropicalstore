//
//  backgroundView.m
//
//          2Remember- A simple reminder program

//  2Remember
//
//  Created by Andrea Rincon Ray on Tue Apr 08 2003.
//  Version 1.0-1.0.5 by Andrea Rincon Ray on or beforer Sun Feb 09 2003.
//  Versions after 1.0.6 by James Lee and Lewis Garrett, Tropical Software, Inc. 

//  2Remember
//  Copyright (c) 2011 Tropical Software, Inc. All rights reserved.
//

#import "backgroundView.h"


@implementation backgroundView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (void)drawRect:(NSRect)rect {
    NSRect bounds=[self bounds];
    [[[NSColor blackColor] colorWithAlphaComponent:0.7] set];
    [NSBezierPath fillRect:bounds];
}
@end
