//
//  registrationWindowController.h
//
//
//          2Remember- A simple reminder program
//
//
//  Version 1.0-1.0.5 by Andrea Rincon Ray on or beforer Sun Feb 09 2003.
//  Versions after 1.0.6 by James Lee and Lewis Garrett, Tropical Software, Inc. 

//  2Remember
//  Versions after 1.0.6 by James Lee and Lewis Garrett, Tropical Software, Inc. 
//  Copyright (c) 2011 Tropical Software, Inc. All rights reserved.
//

#import <AppKit/AppKit.h>


@interface registrationWindowController : NSWindowController {
    IBOutlet id nome;
    IBOutlet id codice;
    IBOutlet id vista;
}

+ (registrationWindowController *)sharedInstance;

- (IBAction)showPanel:(id)sender;
- (IBAction)okButton:(id)sender;
- (IBAction)website:(id)sender;

- (IBAction)modificaTesto:(id)sender;

- (BOOL) codiceValido;
- (BOOL) checkSumValido;
@end
