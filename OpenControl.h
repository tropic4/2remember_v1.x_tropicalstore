/* OpenControl */

#import <Cocoa/Cocoa.h>


@interface OpenControl : NSView
{
    NSTrackingRectTag	trackingRectTag;
    int			currentModeOfOperation;
    NSWindow		*theWindow;
    NSRect		oldRect;
}
-(void)updateTrackingRect;
@end
