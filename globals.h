// These are then imported into a file. Serve to your preferences.
//Queste vanno poi in un file importato. Servono per le preferenze.


@class MainWindowController;
@class InterfaceDriver;
@class appuntamento;
@class agenda;
@class DataWindowController;
@class SmartScrollView;
@class ButtonField;
@class DataWindowInterfaceDriver;
@class AppController;

extern NSString *ARRdocked; // if it is docked //ok se � docked
extern NSString *ARRfrontmost; // must remain always in front? //deve rimanere di fronte sempre?
extern NSString *ARRleft; // is docked to the left? //� docked a sinistra?
extern NSString *ARRbottom;
extern NSString *ARRwinX;
extern NSString *ARRwinY;
extern SmartScrollView *ARRsmartScrollView;
extern NSString *ARRdateTimeOrdering;
extern NSString *ARRtimeFormat;

extern BOOL ARRaboutToTerminate;

//extern dispatchClass 		*ARRsmartDispatch;
extern MainWindowController	*ARRmainWindowController;
extern InterfaceDriver		*ARRinterfaceDriver;
extern agenda			*ARRagenda;
extern NSWindow			*ARRdataWindow;
extern DataWindowController	*ARRdataWindowController;
extern ButtonField		*ARRbuttonField;
extern DataWindowInterfaceDriver *ARRdataWindowInterfaceDriver;
extern AppController		*ARRappController;
//extern tabView			*ARRtabView;

extern int ARRbrowseMode;
extern int ARRbackgroundMode;
extern int ARRnormalMode;


extern int ARRshow;
extern int ARRhide;
extern int ARRlock;
extern int ARRsetup;
extern int ARRinitialize;
extern int ARRunlock;

extern BOOL ARRdockedRT;
extern BOOL ARRleftRT;
extern BOOL ARRbottomRT;
extern BOOL ARRfrontmostRT;

// Other custom preferences.
//Altre preferenze custom...
extern BOOL ARRdoubleClick;
extern BOOL ARRalarmTab;
extern BOOL ARRalarmSound;
extern BOOL ARRalarmIcon;
extern BOOL ARRalarmAlert;

extern int ARRdataSorting;

extern BOOL ARRautoStartup;
extern BOOL ARRanimation;
extern BOOL ARRautoOpen;