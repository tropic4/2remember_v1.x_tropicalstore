// Retrofitted -setWillAppLaunchAtLogin from iAddressX to replace               //leg20180307 - 1.2.0
//  autoStartup.h/c to support adding 2Remember to Login items at
//  macOS start-up.
//
//#import <Carbon/Carbon.h>
//#import <CoreFoundation/CFPreferences.h>
//#import <stdio.h>
//#import <string.h>
//#import "autoStartup.h"
//
//#define kAutoLaunchedApplicationDictionary    CFSTR("AutoLaunchedApplicationDictionary")
//#define kHideLabel                CFSTR("Hide")
//#define kLoginAppCFStringRef            CFSTR("loginwindow")
//#define kPathLabel                CFSTR("Path")
//#define kAliasDataLabel                CFSTR("AliasData")
//
//// Note: If not NULL the results have to be DisposePtr'ed.
//static char* Copy_CFStringRefToCString(const CFStringRef pCFStringRef) {
//    char* results = NULL;
//    if (NULL != pCFStringRef) {
//        CFIndex length = sizeof(UniChar) * CFStringGetLength(pCFStringRef) + 1;
//        results = (char*) NewPtrClear(length);
//        if (!CFStringGetCString(pCFStringRef,results,length,kCFStringEncodingASCII)) {
//            if (!CFStringGetCString(pCFStringRef,results,length,kCFStringEncodingUTF8)) {
//                DisposePtr(results);
//                results = NULL;
//            }
//        }
//    }
//    return results;
//}
//
////se option =0 toglie, altrimenti mette...
//int setStartup(int option) {
//    ProcessSerialNumber myPSN;
//    ProcessInfoRec     myPIR;
//    Str255            appName;
//    FSSpec            appFSSpec;
//    CFStringRef     appCFStringRef;
//    AliasHandle     myAliasH;
//    OSStatus         anErr = noErr;
//    CFURLRef        nostraUrl;
//    CFBundleRef     mainBundle;
//
//    // Get the main bundle for the app
//    mainBundle = CFBundleGetMainBundle();
//    nostraUrl=CFBundleCopyBundleURL(mainBundle);
//    myPIR.processInfoLength = sizeof(ProcessInfoRec);
//    myPIR.processName = appName;
//    myPIR.processAppSpec = &appFSSpec;
//    //troviamo che processo siamo
//    if (noErr != (anErr = GetCurrentProcess(&myPSN))) return 0;
//    //carichiamo le informazioni per il processo
//    if (noErr != (anErr = GetProcessInformation(&myPSN,&myPIR))) return 0;
//
//    appCFStringRef = CFURLCopyLastPathComponent(nostraUrl);
//    if (NULL == appCFStringRef) return 0; //se c'� un errore ritorna.
//    CFStringRef        stringaPath; //� l'unica che ci serve!!!! WOW
//                              //adesso mi becco l'array che mi permette di saprer le preferenze
//    CFArrayRef     tCFArrayRef = CFPreferencesCopyValue(kAutoLaunchedApplicationDictionary, kLoginAppCFStringRef, kCFPreferencesCurrentUser, kCFPreferencesAnyHost);
//    if (NULL == tCFArrayRef) printf("\n Auto Launched Application pref is NULL."); //Non troviamo le preferenze
//    else if (CFArrayGetTypeID() != CFGetTypeID(tCFArrayRef)) printf("\n Auto Launched Application pref is not an array.");
//    else
//    { //tutto ok... possiamo iniziare
//        CFIndex found = -1; //Non ci siamo ancora trovati
//        CFIndex index, count = CFArrayGetCount(tCFArrayRef);
//        for (index = 0;index < count;index++)//iteriamo
//        {
//            //troviamo l'elemento index...
//            CFDictionaryRef elementCFDictionaryRef = CFArrayGetValueAtIndex(tCFArrayRef, index);
//            //controlliamo l'integrit�. E' NULL?
//            if (NULL == elementCFDictionaryRef) printf("\n element #%ld of %ld of tCFArrayRef is NULL.",index + 1,count);
//            //esiste aliasData?
//            else if (!CFDictionaryGetValueIfPresent(elementCFDictionaryRef,kPathLabel,(void*) &stringaPath)) printf("\n 'Path' key missing from elementCFDictionaryRef for element %ld.",index + 1);
//            else
//            {//entriamo nel vivo... abbiamo acquisito tutte le informazioni di cui avevamo bisogno
//                CFStringRef test= CFStringCreateWithSubstring (nil,stringaPath, CFRangeMake(CFStringGetLength(stringaPath)-CFStringGetLength(appCFStringRef),CFStringGetLength(stringaPath)));
//                char *prima=Copy_CFStringRefToCString(test);
//                char *seconda=Copy_CFStringRefToCString(appCFStringRef);
//                //qui dobbiamo confrontare le stringhe!!!!
//                if (strcmp(prima, seconda)==0)
//                {
//                    found = index;    // YES!
//                }
//                if (prima!=nil) DisposePtr(prima);
//                if (seconda!=nil) DisposePtr(seconda);
//            }
//        }
//        CFMutableArrayRef     tCFMutableArrayRef;
//        tCFMutableArrayRef=CFArrayCreateMutableCopy(kCFAllocatorDefault,count,tCFArrayRef);
//        if (NULL == tCFMutableArrayRef) printf("\n CFArrayCreateMutableCopy failed (tCFMutableArrayRef == NULL).");
//        // else if (found > -1)    // We found ourselfs! so remove ourselfs.
//        else if (option==0)
//        {
//            //if (option==0) {
//            if (found > -1) {
//                printf("\n �MATCHED!� element %ld.",found + 1);
//                CFArrayRemoveValueAtIndex(tCFMutableArrayRef,found);
//                CFPreferencesSetValue(kAutoLaunchedApplicationDictionary, tCFMutableArrayRef, kLoginAppCFStringRef, kCFPreferencesCurrentUser, kCFPreferencesAnyHost);
//                if (CFPreferencesAppSynchronize(kLoginAppCFStringRef)) printf("\nCFPreferencesAppSynchronize success!");
//                else printf("\nCFPreferencesAppSynchronize failure.!");
//                //CFRelease(tCFMutableArrayRef);//LEAK?
//            }
//            }
//        //    else    // we didn't find ourselfs! so we insert ourself into the middle of the array
//        else if (option==1) //togliamo e rimettiamo!
//        {
//            //      if (option==1) {
//            if (found > -1) {
//                printf("\n �MATCHED!� element %ld.",found + 1);
//                CFArrayRemoveValueAtIndex(tCFMutableArrayRef,found);
//                CFPreferencesSetValue(kAutoLaunchedApplicationDictionary, tCFMutableArrayRef, kLoginAppCFStringRef, kCFPreferencesCurrentUser, kCFPreferencesAnyHost);
//                if (CFPreferencesAppSynchronize(kLoginAppCFStringRef))
//                    printf("\nCFPreferencesAppSynchronize success!");
//                else printf("\nCFPreferencesAppSynchronize failure.!");
//                //CFRelease(tCFMutableArrayRef);//LEAK?
//            }
//
//            FSRef appFSRef;
//            //SInt32 value = 0;
//            CFBooleanRef tCFBoolNo=kCFBooleanFalse;
//            CFDataRef tCFDataRef;
//            //CFURLRef appCFURLRef;
//            CFStringRef appPathCFStringRef;
//            if (noErr != (anErr = NewAlias(NULL,&appFSSpec,&myAliasH)))
//            {
//                printf("\n NewAlias error: %ld.", anErr);
//                return 0;
//            }
//            tCFDataRef = CFDataCreate(kCFAllocatorDefault,(UInt8*) *myAliasH,GetHandleSize((Handle) myAliasH));
//            if (NULL == tCFDataRef)
//            {
//                printf("\n CFDataCreate failed (tCFDataRef == NULL).");
//                return 0;
//            }
//            if (noErr != (anErr = FSpMakeFSRef(&appFSSpec, &appFSRef)))
//            {
//                printf("\n FSpMakeFSRef error: %ld.", anErr);
//                return 0;
//            }
//
//            appPathCFStringRef = CFURLCopyFileSystemPath(nostraUrl, kCFURLPOSIXPathStyle);
//            if (NULL == appPathCFStringRef)
//            {
//                printf("\n CFURLCopyFileSystemPath failed (appPathCFStringRef == NULL).");
//                return 0;
//            }
//
//            CFDictionaryRef     elementCFDictionaryRef;
//            //CFDictionaryRef    filedataCFDictionaryRef;
//            void*        keys[3];
//            void*        values[3];
//
//            keys[0] = (void*) kAliasDataLabel;
//            keys[1] = (void*) kHideLabel;
//            keys[2] = (void*) kPathLabel;
//
//            values[0] = (void*) tCFDataRef;
//            values[1] = (void*) tCFBoolNo;
//            values[2] = (void*) appPathCFStringRef;
//
//            elementCFDictionaryRef = CFDictionaryCreate(kCFAllocatorDefault,(const void**) &keys,(const void**) &values,3, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
//            if (NULL == elementCFDictionaryRef) {
//                printf("\n CFDictionaryCreate failed (elementCFDictionaryRef == NULL).");
//                return 0;
//            }
//            // insert ourselfs into the preference array
//            CFArrayInsertValueAtIndex(tCFMutableArrayRef,count >> 1,(const void *) elementCFDictionaryRef);
//            // set the new preference
//            CFPreferencesSetValue(kAutoLaunchedApplicationDictionary, tCFMutableArrayRef, kLoginAppCFStringRef, kCFPreferencesCurrentUser, kCFPreferencesAnyHost);
//            if (CFPreferencesAppSynchronize(kLoginAppCFStringRef)) printf("\nCFPreferencesAppSynchronize success!");
//            else
//                printf("\nCFPreferencesAppSynchronize failure.!");
//#if 0
//            // we don't need these anymore
//            CFRelease(appCFURLRef);
//            CFRelease(tCFDataRef);
//            CFRelease(appPathCFStringRef);
//            CFRelease(tCFURLStringTypeCFNumberRef);
//            CFRelease(appCFStringRef);
//            CFRelease(filedataCFDictionaryRef);
//            CFRelease(tiledataCFDictionaryRef);
//            CFRelease(elementCFDictionaryRef);
//#endif
//            // release our mutable array
//            //CFRelease(tCFMutableArrayRef);
//
//            }
//        //CFRelease(tCFArrayRef);//LEAK
//        }
//    return 0;
//        }

