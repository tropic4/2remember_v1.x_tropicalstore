//
//  splashWindowController.h
//
//          2Remember- A simple reminder program

//  2Remember
//
//
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//

#import <AppKit/AppKit.h>


@interface splashWindowController : NSWindowController {
    IBOutlet id welcomeField;
}
+ (splashWindowController *)sharedInstance;
- (IBAction)showPanel:(id)sender;
@end
