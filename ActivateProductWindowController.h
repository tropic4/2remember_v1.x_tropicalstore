//
//  ActivateProductWindowController.h
//  KitchenTimer
//
//  Created by Lewis Garrett on 5/22/14.
//
//

#import <Cocoa/Cocoa.h>

@interface ActivateProductWindowController : NSWindowController {
    NSTextField *nameTextField;
    NSTextField *companyTextField;
    NSTextField *productKeyTextField;
    IBOutlet NSButtonCell *cancelButtonCell;
    IBOutlet NSButtonCell *activateButtonCell;

	NSMutableDictionary *savedSettingsDictionary;
    
	NSString* productCodeOwnerNameText;
	NSString* completeProductCodeText;

    NSMutableString * name;
    NSMutableString * company;
    NSMutableString * productKey;
}

@property (assign) IBOutlet NSTextField *nameTextField;
@property (assign) IBOutlet NSTextField *companyTextField;
@property (assign) IBOutlet NSTextField *productKeyTextField;

-(IBAction)activateButtonAction:(id)sender;
-(IBAction)cancelButtonAction:(id)sender;

@end
