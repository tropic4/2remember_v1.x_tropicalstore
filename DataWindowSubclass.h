/* DataWindowSubclass */

#import <Cocoa/Cocoa.h>

@interface DataWindowSubclass : NSWindow
{
    BOOL visibleChild;
}
-(void)setVisibleChild:(BOOL)val;
@end
