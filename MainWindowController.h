//
//  MainWindowController.h
// 2Remember
//
//
//  Created by Andrea Rincon Ray on Sun Feb 09 2003.
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//

#import <AppKit/AppKit.h>

@interface MainWindowController : NSWindowController {
    int 	dockedStat;
    BOOL 	setNeedUpdate;
    NSRect	openRect;
}
-(void)setOpenRect:(NSRect)e;
-(BOOL)outsideOfScreen:(NSPoint)position;
-(NSRect)getOpenRect;
-(void)gestoreMouseInside:(NSNotification *)note;
-(void)showFromDock:(int)selection;
-(void)dock;
-(void)undock;
-(void)setWindowInit;
-(void)saveWindow;
-(void)requireUpdate;
-(void)requireUpdateRealTime;
@end
