//
//  AppController.m
// 2Remember
//
//
//  Created by Andrea Rincon Ray on Fri Feb 07 2003.
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//

#import <Carbon/Carbon.h>                                                       //leg20180307 - 1.2.0
#import <ServiceManagement/ServiceManagement.h>                                 //leg20180307 - 1.2.0

#import <Sparkle/Sparkle.h>                                                     //leg20141016 - 1.2.0
#import "AppController.h"
#import "globals.h"

#import "MainWindowController.h"
#import "agenda.h"
#import "DataWindowController.h"
#import "appuntamento.h"
#import "ButtonField.h"
#import "splashWindowController.h"
#import "aboutWindowController.h"
#import "prefsWindowController.h"
#import "registrationWindowController.h"                                        //leg20141016 - 1.2.0
#import "InterfaceDriver.h"
#import "dataDisplayWindowController.h"
#import "autoStartup.h"
#import "ViewReadMe.h"                                                          //leg20201204 - 1.3.0


@implementation AppController

@synthesize registeredName, registeredCompany, registeredProductCode;           //leg20141016 - 1.2.0

-(id)init
{
    if (self=[super init]){
        ARRappController=self;
        numeroAllarmi=0;;

        [self inizializzaPreferenze];
        [self leggePreferenzeUtente];

// Disable old registration pop-up panel.                                       //leg20141016 - 1.2.0
//         Timer recording!
//		timer registrazione!
//        [NSTimer scheduledTimerWithTimeInterval:600
//                                         target:self
//                                       selector:@selector(registrazione:)
//                                       userInfo:nil
//                                        repeats:YES];
        
        // allocates the agenda item.
		//alloca l'oggetto agenda...
        ARRagenda=[[agenda alloc]init];

        // timer auto-save every 60 seconds
		//timer salvataggio automatico ogni 60 secondi
        [NSTimer scheduledTimerWithTimeInterval:60
                                         target:self
                                       selector:@selector(salvataggioPeriodico)
                                       userInfo:nil
                                        repeats:YES];
    }
    return self;
}

-(void)awakeFromNib {
    // Shows the main window
	//Mostra la finestra principale
    if (!ARRmainWindowController) {
        ARRmainWindowController=[[MainWindowController alloc] init];
        //setta le dimensioni iniziali ed alloca...
        [ARRmainWindowController setWindowInit];
    }
    // charge on the window (not shown)
	//carica la data window (non la mostra)
    if (!ARRdataWindowController) {
        ARRdataWindowController=[[DataWindowController alloc] init];
    }
    // loads and initializes the data ...
	//carica ed inizializza i dati...
    [ARRagenda load];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"splashMostrato"]==NO) {
        [[splashWindowController sharedInstance] showPanel:self];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"splashMostrato"];
    }

    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"2RememberName"] length]>0) registerMenu=NO; else registerMenu=YES;
}


// ***  System Events Response *******************************************************//

//- (void)applicationWillFinishLaunching:(NSNotification *)aNotification {        //leg20141016 - 1.2.0
//	// Insert code here to initialize your application
//	2RememberDocumentController = [[2RememberDocumentController alloc] init];
//	[2RememberDocumentController release];
//}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {         //leg20141016 - 1.2.0
	// Insert code here to initialize your application
    
    // Fix Issue #016 - 2Remember multiple launches                             //leg20201214 - 1.3.0
    //
    // Hsoi 2014-10-19 - Determine if app is running (again) and terminate this instance if so.
    // This code is taken directly from DTS Follow-up: 612125399. See https://bitbucket.org/iaddressx/iaddressx/issue/27/iaddressx-is-running-twice
    // for discussion of greater issues that still need addressing.
    //
    NSArray*    apps = [NSRunningApplication runningApplicationsWithBundleIdentifier:[[NSBundle mainBundle] bundleIdentifier]];
    if ([apps count] > 1) {
        NSRunningApplication* curApp = [NSRunningApplication currentApplication];
        for (NSRunningApplication* app in apps) {
            if ([[app bundleIdentifier] isEqualToString:[curApp bundleIdentifier]]) {
                NSAlert *testAlert = [NSAlert alertWithMessageText:NSLocalizedString(@"2Remember is Already Running.", nil)
                                                     defaultButton:NSLocalizedString(@"Quit", nil)
                                                   alternateButton:nil
                                                       otherButton:nil
                                         informativeTextWithFormat:NSLocalizedString(@"Only one version of 2Remember can be run by a user. This instance must quit.", nil)];
                [testAlert runModal];
                
                [app activateWithOptions:NSApplicationActivateAllWindows | NSApplicationActivateIgnoringOtherApps];
                
                [NSApp terminate:nil];
                return;
            }
        }
    }

//    // Experimental customization of Sparkle Updating
//    NSTimeInterval ck4UpdateTimeInterval = [[SUUpdater sharedUpdater] updateCheckInterval];
//    [[SUUpdater sharedUpdater] setAutomaticallyChecksForUpdates:YES];
//    [SUUpdater sharedUpdater].delegate = self;
//
//    [[SUUpdater sharedUpdater] checkForUpdates:nil];
    
    // *** DEBUGGING CODE ***
    if (NO) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RegisteredProductCode"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RegisteredName"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RegisteredCompany"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"IsDisabled"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"FirstRunDate"];
        [NSUserDefaults resetStandardUserDefaults];
    }
    
    registeredProductCode = (NSMutableString *)[[NSUserDefaults standardUserDefaults] stringForKey:@"RegisteredProductCode"];    //leg20140521 - 1.1.0
    registeredName = (NSMutableString *)[[NSUserDefaults standardUserDefaults] stringForKey:@"RegisteredName"];                  //leg20140521 - 1.1.0
    registeredCompany = (NSMutableString *)[[NSUserDefaults standardUserDefaults] stringForKey:@"RegisteredCompany"];            //leg20140521 - 1.1.0
    
    NSDate *firstRunDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"FirstRunDate"];
    NSTimeInterval timeSinceFirstRun = 0.0;
    timeSinceFirstRun = [[NSDate date] timeIntervalSinceDate:firstRunDate];
    if (!registeredProductCode) {
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"IsDisabled"]) {
            if (!firstRunDate) {
                [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"FirstRunDate"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSAlert *alert = [NSAlert alertWithMessageText:@"2Remember not registered!" defaultButton:nil alternateButton:nil otherButton:nil informativeTextWithFormat:@"2Remember must be registered within 30 days of first use!"];
                [alert runModal];
            } else {
                
                // Disable "Check For Updates" menu.
                NSMenu *menu = [[NSApplication sharedApplication] mainMenu];
                NSMenuItem *supportMenuItem = [menu itemWithTitle:@"Support"];
                NSMenu *supportSubMenu = [supportMenuItem submenu];
                [supportSubMenu setAutoenablesItems:NO];
                NSMenuItem *ck4UpdatesMenuItem = [supportSubMenu itemWithTitle:@"Check For Updates"];
                [ck4UpdatesMenuItem setEnabled:NO];
                
                if (timeSinceFirstRun > 60*60*24*30 && !registeredProductCode) {
//                if (timeSinceFirstRun > 60*3 && !registeredProductCode) {     // For testing makes the trial period just 3 minutes.
                    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:@"IsDisabled"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    NSAlert *alert = [NSAlert alertWithMessageText:@"30 days of free use has expired. No new reminders can be added until product is registered!" defaultButton:nil alternateButton:@"Activate 2Remember" otherButton:@"Buy 2Remember" informativeTextWithFormat:@"2Remember must be registered for continued use!"];
                    NSInteger buttonIndex = [alert runModal];
                    
                    if (buttonIndex == -1)
                        [self jumpToTropic4BuyPage:nil];
                    else if (buttonIndex == 0)
                        [self showActivateProductKeyWindow:nil];
                } else {
                    NSInteger daysSinceFirstRun = timeSinceFirstRun/86400.0;
                    NSInteger daysLeftB4Expire = 30 - daysSinceFirstRun;
//                    NSInteger daysSinceFirstRun = timeSinceFirstRun/60;       // For testing makes the trial period just 3 minutes.
//                    NSInteger daysLeftB4Expire = 3 - daysSinceFirstRun;       // For testing makes the trial period just 3 minutes.
                    NSAlert *alert = [NSAlert alertWithMessageText:@"2Remember not registered!" defaultButton:nil alternateButton:nil otherButton:nil informativeTextWithFormat:@"2Remember must be registered within %d days!", daysLeftB4Expire];
                    [alert runModal];
                }
            }
        } else {
            // 30 days of free use has expired, Disable "Check For Updates" menu.
            NSMenu *menu = [[NSApplication sharedApplication] mainMenu];
            NSMenuItem *supportMenuItem = [menu itemWithTitle:@"Support"];
            NSMenu *supportSubMenu = [supportMenuItem submenu];
            [supportSubMenu setAutoenablesItems:NO];
            NSMenuItem *ck4UpdatesMenuItem = [supportSubMenu itemWithTitle:@"Check For Updates"];
            [ck4UpdatesMenuItem setEnabled:NO];
            
            NSAlert *alert = [NSAlert alertWithMessageText:@"30 days of free use has expired. No new reminders can be added until product is registered!" defaultButton:nil alternateButton:@"Activate 2Remember" otherButton:@"Buy 2Remember" informativeTextWithFormat:@"Go to Activate menu to register 2Remember!"];
            NSInteger buttonIndex = [alert runModal];
            
            if (buttonIndex == -1)
                [self jumpToTropic4BuyPage:nil];
            else if (buttonIndex == 0)
                [self showActivateProductKeyWindow:nil];
        }
    }
}

//***Risposta Eventi Sistema*******************************************************//

-(void)applicationWillBecomeActive:(NSNotification *)notification {
    [self mainInterfaceDispatch:2];
}

-(void)applicationWillResignActive:(NSNotification *)notification {
    [self mainInterfaceDispatch:3];
}

-(void)applicationDidBecomeActive:(NSNotification *)notification {

    // Disable File/New menu item and plusButton if 30 day trial has ended.     //leg20141016 - 1.2.0
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"IsDisabled"]) {

        // Disable the plusButton (new reminder).                               //leg20141016 - 1.2.0
        [ARRinterfaceDriver->plusButton setEnabled:NO];

        // Disable File/New menu item.                                          //leg20141016 - 1.2.0
        NSMenu *menu = [[NSApplication sharedApplication] mainMenu];
        NSMenuItem *fileMenuItem = [menu itemWithTitle:@"File"];
        NSMenu *fileSubMenu = [fileMenuItem submenu];
        [fileSubMenu setAutoenablesItems:NO];
        NSMenuItem *newMenuItem = [fileSubMenu itemWithTitle:@"New"];
        [newMenuItem setEnabled:NO];
    }
}

-(void)applicationWillTerminate:(NSNotification *)notification {

	

	//setStartup(ARRautoStartup);
	

	
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender {
	ARRaboutToTerminate=YES;
	if ([[NSUserDefaults standardUserDefaults] boolForKey:@"domandaStartup"]==NO) {
		
//        int button = NSRunAlertPanel(@"Do You want 2Remember to be launched automatically the next time you start your computer?", @"You can always set this in 2Remember's preferences.", @"No", @"Yes", nil);
        NSInteger button = NSRunAlertPanel(@"Do You want 2Remember to be launched automatically the next time you start your computer?", @"You can always set this in 2Remember's preferences.", @"No", @"Yes", nil); //leg20180405 - 1.2.0

        if (NSOKButton==button) ARRautoStartup=NO; else ARRautoStartup=YES;
//        if (NSModalResponseOK==button) ARRautoStartup=NO; else ARRautoStartup=YES;  //leg20180405 - 1.2.0
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"domandaStartup"];
    }
//    setStartup(ARRautoStartup);
    [self setWillAppLaunchAtLogin:ARRautoStartup];                              //leg20180307 - 1.2.0
	
    [self salvaPreferenzeUtente];
	
    [ARRdataWindowController hide];
    [ARRmainWindowController showFromDock:ARRunlock];
    [ARRmainWindowController saveWindow];
    [ARRagenda save];

    return NSTerminateNow;
}

// Retrofitted from iAddressX to support adding 2Remember to Login items        //leg20180307 - 1.2.0
//  at macOS start-up.
- (void)setWillAppLaunchAtLogin:(BOOL)launch {
    
    LSSharedFileListRef     loginItems = NULL;
    LSSharedFileListItemRef currentItem = NULL;
    [[self class] copyLoginItems:&loginItems andCurrentLoginItem:&currentItem];
    
    if (loginItems) {
        if (launch && !currentItem) {
            NSURL*  thePath = [[NSBundle mainBundle] bundleURL];
            NSLog(@"Adding to startup items.");
            LSSharedFileListItemRef item = LSSharedFileListInsertItemURL(loginItems, kLSSharedFileListItemBeforeFirst, NULL, NULL, (__bridge CFURLRef)thePath, NULL, NULL);
            if (item) {
                CFRelease(item);
            }
        }
        else if (!launch && currentItem) {
            NSLog(@"Removing from startup items.");
            LSSharedFileListItemRemove(loginItems, currentItem);
        }
        
        CFRelease(loginItems);
    }
    
    if (currentItem) {
        CFRelease(currentItem);
    }
}

// Retrofitted from iAddressX to support adding 2Remember to Login items        //leg20180307 - 1.2.0
//  at macOS start-up.
// Hsoi 2014-09-22 - Based upon: https://github.com/codykrieger/gfxCardStatus/blob/master/Classes/GSStartup.m

+ (void)copyLoginItems:(LSSharedFileListRef *)loginItems andCurrentLoginItem:(LSSharedFileListItemRef *)currentItem {
    NSParameterAssert(loginItems != NULL);
    NSParameterAssert(currentItem != NULL);
    
    *loginItems = LSSharedFileListCreate(kCFAllocatorDefault, kLSSharedFileListSessionLoginItems, NULL);
    
    if (*loginItems) {
        UInt32      seedValue;
        NSArray*    loginItemsArray = (__bridge NSArray *)LSSharedFileListCopySnapshot(*loginItems, &seedValue);
        
        if (loginItemsArray) {
            NSString* bundleName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
            NSString* appName = [NSString stringWithFormat:@"%@.app", bundleName];
            
            for (id item in loginItemsArray) {
                LSSharedFileListItemRef itemRef = (__bridge LSSharedFileListItemRef)item;
                CFURLRef                URL = NULL;
                
                if (LSSharedFileListItemResolve(itemRef, 0, &URL, NULL) == noErr) {
                    if ([[(__bridge NSURL *)URL path] hasSuffix:appName]) {
                        NSLog(@"App exists in startup items.");
                        
                        *currentItem = itemRef;
                        CFRetain(*currentItem);
                        CFRelease(URL);
                        break;
                    }
                    
                    CFRelease(URL);
                }
            }
            
            CFRelease((__bridge CFArrayRef)loginItemsArray);
        }
    }
}

// *** Events Application Response *************************************************//
//*** Risposta Eventi Applicazione *************************************************//

-(void)notifyOfAlarm:(appuntamento*)who {
//    if (ARRalarmAlert==YES) {
    if (ARRalarmAlert) {                                                        //leg20180326 - 1.2.0
        
// Backed-out following 20180326 leak fix because it was preventing alarm       //leg20180406 - 1.2.0
//  alert window from being shown when alarm fired.
//
        [[dataDisplayWindowController newInstance] showWithTodo:who];
//        dataDisplayWindowController *ddwc = [dataDisplayWindowController newInstance];    //leg20180326 - 1.2.0
//        [ddwc showWithTodo:who];                                                          //leg20180326 - 1.2.0
//        [ddwc release];                                                                   //leg20180326 - 1.2.0
    }

//    if (ARRalarmIcon==YES) {
    if (ARRalarmIcon) {                                                         //leg20180326 - 1.2.0
//        int bubu=[NSApp requestUserAttention:NSCriticalRequest]; bubu=bubu;
        long bubu=[NSApp requestUserAttention:NSCriticalRequest];               //leg20180405 - 1.2.0
#pragma unused(bubu)
    }

//    if (ARRalarmTab==YES) {
    if (ARRalarmTab) {                                                          //leg20180326 - 1.2.0
        if (numeroAllarmi==0) { // means that it is the first
								// adesso setta
								//vuol dire che � il primo
                                //adesso setta
            // English Translation:                                             //leg20180405 - 1.2.0
            // now sect means that it is the first now sect
            timerGenerale=[NSTimer scheduledTimerWithTimeInterval:0.7
                                                           target:self
                                                         selector:@selector(blinkON)
                                                         userInfo:nil
                                                          repeats:YES];
        }
        numeroAllarmi++;
    }
//    if (ARRalarmSound==YES) NSBeep();
    if (ARRalarmSound) NSBeep();                                                //leg20180326 - 1.2.0
}

-(void)endNotificationOfAlarm:(appuntamento*)who{
    if (numeroAllarmi>0) numeroAllarmi--;
    if (numeroAllarmi==0&&timerGenerale!=nil) [timerGenerale invalidate];
}

-(void)blinkON {
    // first ... disable the timer
	//primo... disattiviamo il timer
    timerSpegni=[NSTimer scheduledTimerWithTimeInterval:0.1
                                                 target:self
                                               selector:@selector(blinkOFF)
                                               userInfo:nil
                                                repeats:NO];
    // block the redraw
	//blocchiamo il redraw
    [[ARRmainWindowController window] disableFlushWindow];
    // then we'll post the event (if necessary)
	//poi postiamo l'evento (se necessario)
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"ARRblinkON" object:self];
    // always necessary ridisegnamo all ..
	//sempre se necessario ridisegnamo tutto...
    [ARRbuttonField redrawOffline];
    [ARRbuttonField setLockRects:YES];
    [ARRbuttonField display];
    [ARRbuttonField setLockRects:NO];
    // rehabilitate the redraw
	//riabilitiamo il redraw
    [[ARRmainWindowController window] enableFlushWindow];
    [[ARRmainWindowController window] flushWindow];
}

-(void)blinkOFF {
    [[ARRmainWindowController window] disableFlushWindow];
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc postNotificationName:@"ARRblinkOFF" object:self];
    [ARRbuttonField redrawOffline];
    [ARRbuttonField setLockRects:YES];
    [ARRbuttonField display];
    [ARRbuttonField setLockRects:NO];
    [[ARRmainWindowController window] enableFlushWindow];
    [[ARRmainWindowController window] flushWindow];
}

//0- mainWindowController: mouseEntered
//1- mainWindowController:mouseExited
-(void)mainInterfaceDispatch:(int)event {

    // I look at the result in the future!
	//in futuro guardo anche il risultato!!!
    if (event==0) {
        //NSLog(@"showFromDock:ARRshow");
        if (ARRautoOpen==YES) {
            if ([ARRinterfaceDriver isMouseOutside]==NO) {//allora � dentro
                [ARRmainWindowController showFromDock:ARRshow];
                if ([ARRinterfaceDriver isMouseOutside]==YES) {
                    [ARRmainWindowController showFromDock:ARRhide];
                }
            }
        }
    }

    if (event==1) {
        //NSLog(@"showFromDock:ARRhide");
        if (ARRautoOpen==YES) {
            if ([ARRinterfaceDriver isMouseOutside]==YES) {
                [ARRmainWindowController showFromDock:ARRhide];
            }
        }
    }

    if (event==2) {//application will become active
        //NSLog(@"showFromDock:ARRlock");
        if (ARRaboutToTerminate==NO) [ARRmainWindowController showFromDock:ARRlock];
    }

    if (event==3) {//application will resign active
        //NSLog(@"showFromDock:ARRunlock");
        [ARRmainWindowController showFromDock:ARRunlock];
    }

    if (event==4) {//mouseClick
        //NSLog(@"showFromDock:ARRshow");
        [ARRmainWindowController showFromDock:ARRshow];
    }

}

- (void) salvataggioPeriodico {
    static int seriale=0;
    if ([[ARRagenda progressiveNumber] intValue]!=seriale) {
        seriale = [[ARRagenda progressiveNumber] intValue];
        [ARRagenda save];
    }
}

// Menu Manager
//*** Gestione Menu ****************************************************************//
- (IBAction)showAboutBox:(id)sender {
    [[aboutWindowController sharedInstance] showPanel:sender];
}

- (IBAction)showPreferences:(id)sender {
    [[prefsWindowController sharedInstance] showPanel:sender];
}

-(IBAction)registrazione:(id)who {                                              //leg20141016 - 1.2.0
    [[registrationWindowController sharedInstance] showPanel:self];
}

- (IBAction)checkForNewVersion:(id)sender {
    NSString *currVersionNumber =[[[NSBundle bundleForClass:[self class]] infoDictionary] objectForKey:@"CFBundleVersion"];

    NSDictionary *productVersionDict=[NSDictionary dictionaryWithContentsOfURL: [NSURL URLWithString:@"http://www.andreaegreta.com/softwareupdates/versionlist.xml"]];

    NSString *latestVersionNumber = [productVersionDict valueForKey:@"2Remember"];

    if (latestVersionNumber==nil) {
        NSRunAlertPanel(@"I can't connect to the update server.", @"Make sure you are connected to the internet before checking for an updated version.", @"OK", nil, nil);
    } else {
        if ([latestVersionNumber isEqualTo: currVersionNumber]) {
            NSRunAlertPanel(@"Your Software is up-to-date.", @"You have the most recent version of 2Remember.", @"OK", nil, nil);
        } else {
            int button = NSRunAlertPanel(@"A new version is available.", [NSString stringWithFormat:@"A new version of 2Remember is available ( version %@ ). Would you like to download the new version now?", latestVersionNumber], @"OK", @"Cancel", nil);

            if (NSOKButton==button) {
//            if (NSModalResponseOK==button) {                                    //leg20180405 - 1.2.0
                [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://www.andreaegreta.com/softwareupdates/"]];
            }
        }
    }
}

- (IBAction)newMenu:(id)sender { //OK
    [ARRinterfaceDriver plusButton:self];
}

- (IBAction)openMenu:(id)sender {
    if ([ARRagenda selected]!=nil) [ARRbuttonField doubleClickOnSelected];
}

// Problem Location
// Problema di Localizzazione
- (BOOL)validateMenuItem:(NSMenuItem *)anItem {
    if ([[anItem title] isEqualToString:@"Open Selected"] &&
        ([ARRagenda selected]==nil)) {
        return NO;
    }
    if ([[anItem title] isEqualToString:@"Registration..."] &&
        (registerMenu==NO)) {
        return NO;
    }

    return YES;
}

//  Accessory Variables
//*** Variabili Accessorie **********************************************************//

-(void)setRegisterMenu:(BOOL)val {
    registerMenu=val;
}

// Preferences
//*** Preferenze ********************************************************************//

-(void)inizializzaPreferenze {
    NSRect screenFra=[[NSScreen mainScreen] visibleFrame];
    int y=screenFra.origin.y+screenFra.size.height-50;

    // Create a free dictionary
	//Crea il dizionario
    NSMutableDictionary *defaultValues=[NSMutableDictionary dictionary];

    // Record defaults
	//Registra i default
    [defaultValues setObject:[NSNumber numberWithBool:YES] forKey:ARRdocked];
    [defaultValues setObject:[NSNumber numberWithBool:YES] forKey:ARRfrontmost];
    [defaultValues setObject:[NSNumber numberWithBool:YES] forKey:ARRleft];
    [defaultValues setObject:[NSNumber numberWithBool:NO] forKey:ARRbottom];
    [defaultValues setObject:[NSNumber numberWithInt:(screenFra.origin.x-30)] forKey:ARRwinX];
    [defaultValues setObject:[NSNumber numberWithInt:y] forKey:ARRwinY];
    [defaultValues setObject:[NSNumber numberWithBool:YES] forKey:@"useDoubleClick"];
    [defaultValues setObject:[NSNumber numberWithBool:YES] forKey:@"alarmTab"];
    [defaultValues setObject:[NSNumber numberWithBool:YES] forKey:@"alarmIcon"];
    [defaultValues setObject:[NSNumber numberWithBool:YES] forKey:@"alarmAlert"];
    [defaultValues setObject:[NSNumber numberWithBool:YES] forKey:@"alarmSound"];
    [defaultValues setObject:[NSNumber numberWithBool:NO] forKey:@"splashMostrato"];
    [defaultValues setObject:[NSNumber numberWithInt:0] forKey:@"dataSorting"];
    [defaultValues setObject:[NSNumber numberWithBool:NO] forKey:@"autoStartup"];
    [defaultValues setObject:[NSNumber numberWithBool:YES] forKey:@"animation"];
    [defaultValues setObject:[NSNumber numberWithBool:NO] forKey:@"domandaStartup"];
    [defaultValues setObject:[NSNumber numberWithBool:YES] forKey:@"ARRautoOpen"];
    [defaultValues setObject:@"" forKey:@"2RememberName"];
    [defaultValues setObject:@"" forKey:@"2RememberSerial"];


    [[NSUserDefaults standardUserDefaults] registerDefaults: defaultValues];
}

// Reads user preferences                                                       //leg20230914 - Translation
-(void)leggePreferenzeUtente {
    // Now compile the RealTime preferences:
	//adesso compila le preferenze RealTime:
    ARRdockedRT=[[NSUserDefaults standardUserDefaults] boolForKey:ARRdocked];
    ARRleftRT=[[NSUserDefaults standardUserDefaults] boolForKey:ARRleft];
    ARRbottomRT=[[NSUserDefaults standardUserDefaults] boolForKey:ARRbottom];
    ARRfrontmostRT=[[NSUserDefaults standardUserDefaults] boolForKey:ARRfrontmost];

    ARRdateTimeOrdering=[[NSUserDefaults standardUserDefaults] stringForKey:NSShortDateFormatString];
    [ARRdateTimeOrdering retain];
    ARRtimeFormat=[[NSUserDefaults standardUserDefaults] stringForKey:NSTimeFormatString];
    [ARRtimeFormat retain];
    ARRdoubleClick=[[NSUserDefaults standardUserDefaults] boolForKey:@"useDoubleClick"];

    ARRalarmTab=[[NSUserDefaults standardUserDefaults] boolForKey:@"alarmTab"];
    ARRalarmIcon=[[NSUserDefaults standardUserDefaults] boolForKey:@"alarmIcon"];
    ARRalarmAlert=[[NSUserDefaults standardUserDefaults] boolForKey:@"alarmAlert"];
    ARRalarmSound=[[NSUserDefaults standardUserDefaults] boolForKey:@"alarmSound"];
    ARRautoStartup=[[NSUserDefaults standardUserDefaults] boolForKey:@"autoStartup"];
    ARRanimation=[[NSUserDefaults standardUserDefaults] boolForKey:@"animation"];
    ARRdataSorting=[[NSUserDefaults standardUserDefaults] integerForKey:@"dataSorting"];
    ARRautoOpen=[[NSUserDefaults standardUserDefaults] boolForKey:@"ARRautoOpen"];
}

// Save user preferences                                                        //leg20230914 - Translation
-(void)salvaPreferenzeUtente {
    [[NSUserDefaults standardUserDefaults] setBool:ARRdockedRT forKey:ARRdocked];
    [[NSUserDefaults standardUserDefaults] setBool:ARRleftRT forKey:ARRleft];
    [[NSUserDefaults standardUserDefaults] setBool:ARRbottomRT forKey:ARRbottom];
    [[NSUserDefaults standardUserDefaults] setBool:ARRfrontmostRT forKey:ARRfrontmost];
    [[NSUserDefaults standardUserDefaults] setBool:ARRdoubleClick forKey:@"useDoubleClick"];
    [[NSUserDefaults standardUserDefaults] setBool:ARRalarmTab forKey:@"alarmTab"];
    [[NSUserDefaults standardUserDefaults] setBool:ARRalarmIcon forKey:@"alarmIcon"];
    [[NSUserDefaults standardUserDefaults] setBool:ARRalarmAlert forKey:@"alarmAlert"];
    [[NSUserDefaults standardUserDefaults] setBool:ARRalarmSound forKey:@"alarmSound"];
    [[NSUserDefaults standardUserDefaults] setInteger:ARRdataSorting forKey:@"dataSorting"];
    [[NSUserDefaults standardUserDefaults] setBool:ARRautoStartup forKey:@"autoStartup"];
    [[NSUserDefaults standardUserDefaults] setBool:ARRanimation forKey:@"animation"];
    [[NSUserDefaults standardUserDefaults] setBool:ARRautoOpen forKey:@"ARRautoOpen"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

// *** No definitions found ***********************************************************//
//***Deinizializzazione************************************************************//
-(void)dealloc {
    [ARRmainWindowController release];
    [ARRagenda release];
    [super dealloc];
}


-(IBAction)showActivateProductKeyWindow:(id)sender                              //leg20141016 - 1.2.0
{
	// Lazy instantiation of controller
    //	if (!activateProductWindowController) {
    activateProductWindowController = [[ActivateProductWindowController alloc] initWithWindowNibName:@"ActivateProductWindowController"];
    //	}
    
	[activateProductWindowController showWindow:sender];
	[[activateProductWindowController window] center];
	[[activateProductWindowController window] setReleasedWhenClosed:YES];
}

-(IBAction)jumpToTropic4BuyPage:(id)sender                                      //leg20141016 - 1.2.0
{
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://www.tropic4.com/products.html"]];
}

// Display a ReadMe RTF document in its own window.                             //leg20201204 - 1.3.0
- (IBAction) viewReadMe: (id) sender {
    // Lazy instantiation of controller
    if (!_viewReadMeWindowController) {
        _viewReadMeWindowController = [[ViewReadMe alloc] initWithWindowNibName:@"ViewReadMe"];
    }

    [_viewReadMeWindowController showWindow:sender];
    [[_viewReadMeWindowController window] center];
    
    // Force window to front.
    [[_viewReadMeWindowController window] orderFrontRegardless];
}

@end
