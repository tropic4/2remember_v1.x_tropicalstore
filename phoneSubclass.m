//
//  phoneSubclass.m
//
//          2Remember- A simple reminder program

//  2Remember
//
//  Created by Andrea Rincon Ray on Tue Apr 08 2003.
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//

#import "phoneSubclass.h"


@implementation phoneSubclass
- (id)initWithContentRect:(NSRect)contentRect styleMask:(unsigned int)aStyle backing:(NSBackingStoreType)bufferingType defer:(BOOL)flag {

    //OK... adesso siamo in gioco... Partiamo creando una nuova window di tipo borderless...
    NSWindow* result = [super initWithContentRect:contentRect
                                        styleMask:NSBorderlessWindowMask
                                          backing:NSBackingStoreBuffered
                                            defer:NO];

    [result setBackgroundColor: [NSColor clearColor]];
    [result setOpaque:NO];
    [result setHasShadow: NO];
//    return result;
    return (phoneSubclass*)result;                                              //leg20180319 - 1.2.0
}

- (void)mouseDown:(NSEvent *)theEvent {
    [self close];
}

- (BOOL)canBecomeKeyWindow {
    return YES;
}

- (void)sendEvent:(NSEvent *)theEvent {
    NSEventType	cheTipo=[theEvent type];
    if (cheTipo==NSLeftMouseDown||cheTipo==NSKeyDown) [self close];
    [super sendEvent:theEvent];
}
@end
