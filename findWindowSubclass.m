//
//  findWindowSubclass.m
// 2Remember
//
//
//  Created by Andrea Rincon Ray on Sun Mar 23 2003.
//  Version 1.0-1.0.5 by Andrea Rincon Ray on or beforer Sun Feb 09 2003.
//  Versions after 1.0.6 by James Lee and Lewis Garrett, Tropical Software, Inc. 

//  2Remember
//  Copyright (c) 2011 Tropical Software, Inc. All rights reserved.
//

#import "findWindowSubclass.h"


@implementation findWindowSubclass
- (id)initWithContentRect:(NSRect)contentRect styleMask:(unsigned int)aStyle backing:(NSBackingStoreType)bufferingType defer:(BOOL)flag {
    
    NSWindow* result = [super initWithContentRect:contentRect
                                        styleMask:NSBorderlessWindowMask
                                          backing:NSBackingStoreBuffered
                                            defer:YES]; //Valutare defer.
    [result setOpaque: YES];
    [result setHasShadow: YES];
    [result setAlphaValue:0.8];
    return result;
}


@end
