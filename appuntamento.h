//
//  appuntamento.h	(appointments)
// 2Remember
//
//
//  Created by Andrea Rincon Ray on Wed Feb 26 2003.
//  Version 1.0-1.0.5 by Andrea Rincon Ray on or beforer Sun Feb 09 2003.
//  Versions after 1.0.6 by James Lee and Lewis Garrett, Tropical Software, Inc. 

//  2Remember
//  Copyright (c) 2011 Tropical Software, Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>


@interface appuntamento : NSObject <NSCoding> {
    NSString 		*person;
    NSString		*note;
    int			kind;
    NSString		*syncId;
    NSDate	*dataCreazione;
    NSString		*phone;
    NSString		*place;
    BOOL		alarm;
    NSString		*email;
    NSString		*what;
    NSString		*todo;
    NSCalendarDate	*date;
    int			hour;
    int			minute;
    int			categoria;

    NSImage		*selectedImage;
    NSImage		*selfImage;
    NSImage		*hilightImage;

    NSPoint 		posizione;

    BOOL		markDelete;
    BOOL		markNew;
    BOOL		displayed;

    BOOL		hilight;
    BOOL		selected;

    BOOL		blink;
    int			snoozeDelta;
    BOOL		suona;
    
    NSImage		*myIcon,*myIconH;

    NSMutableDictionary *attributiDescrizione,*attributiData,*attributiTag,*attributiDati;

    NSTimer		*ilTimer;
}
-(void)initDizionari;
-(void)setDeleted:(BOOL)val;
-(BOOL)isDeleted;
-(void)setNew:(BOOL)val;
-(BOOL)isNew;
-(void)setPosizione:(NSPoint)val;
-(NSPoint)posizione;
-(void)setDisplayed:(BOOL)val;
-(BOOL)displayed;
-(NSImage*)selfImage;
-(void)selected:(BOOL)val;
-(void)hilight:(BOOL)val;
-(void)setPerson:(NSString*)val;
-(NSString*)person;
-(void)setNote:(NSString*)val;
-(NSString*)note;
-(void)setKind:(int)val;
-(int)kind;
-(void)setSyncId:(NSString*)val;
-(NSString*)syncId;
-(void)setDate:(NSCalendarDate*)val;
-(NSCalendarDate*)date;
//-(void)setSDate:(NSString*)val;
//-(NSString*)sDate;
-(void)setSTime:(NSString*)val;
-(NSString*)sTime;
-(void)setPhone:(NSString*)val;
-(NSString*)phone;
-(void)setPlace:(NSString*)val;
-(NSString*)place;
-(void)setAlarm:(BOOL)val;
-(BOOL)alarm;
-(void)setEmail:(NSString*)val;
-(NSString*)email;
-(void)setWhat:(NSString*)val;
-(NSString*)what;
-(void)setTodo:(NSString*)val;
-(NSString*)todo;

-(void)redrawImage;

-(void)setupAlarms;
-(void)blinkON;
-(void)blinkOFF;
-(void)fireAlarm;
-(void)snooze:(int)howMuch;
-(void)evaluateStartUp;
-(NSTimeInterval)secondsFromNow;
-(void)setTimer;
-(BOOL)canSetAlarm;
-(void)mightStopAlarm;

-(NSString*)dataRelativa;
-(NSString*)descrizioneTitolo;
//-(NSFormattedText*)descrizioneFormattata;
-(NSString*)descrizioneBottone;

-(void)matchAction;
-(void)matchAlarm;

-(void)setDataCreazione:(NSDate*)dataNuova;
-(NSTimeInterval)absoluteCreation;
@end
