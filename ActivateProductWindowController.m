//
//  ActivateProductWindowController.m
//  2Remember
//
//  Created by Lewis Garrett on 5/22/14.
//
//

#import "AppController.h"                                                       //leg20141016 - 1.2.0
#import "ActivateProductWindowController.h"
#import "globals.h"                                                             //leg20141016 - 1.2.0
#import "InterfaceDriver.h"                                                     //leg20141016 - 1.2.0

void MakeProductKeySN( UInt16 inSeed, Str31 outProductKey,
                      UInt8 inProductKeyLength, UInt8 inProductKeyMinLen, UInt16 inMapLen);
unsigned short MakeNameHash( CFStringRef inUserName );
unsigned short MakeNameHashIpod( CFStringRef inUserName );

union APP_SIG {
	OSType	fourChar;
	UInt8	byte[4];
} mAppSignature;

#define kProductCodePrefix	@"2REM1" // 2Remember product code prefix.

//@interface ActivateProductWindowController () {
//    NSMutableString * name;
//    NSMutableString * company;
//    NSMutableString * productKey;
//}
//
//@end

@implementation ActivateProductWindowController
@synthesize nameTextField;
@synthesize companyTextField;
@synthesize productKeyTextField;

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    return self;
}

//- (void)windowWillLoad {
//    [super windowDidLoad];
//
//    KitchenTimerAppDelegate * kitchenTimerAppDelegate = [[NSApplication sharedApplication] delegate];
//    [nameTextField setStringValue:kitchenTimerAppDelegate.registeredName];
//    [companyTextField setStringValue:kitchenTimerAppDelegate.registeredCompany];
//    [productKeyTextField setStringValue:kitchenTimerAppDelegate.registeredProductCode];
//}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window
    //  controller's window has been loaded from its nib file.

    AppController * appControllerAppDelegate = [[NSApplication sharedApplication] delegate];
    if (!appControllerAppDelegate.registeredName)
        [nameTextField setStringValue:@""];
    else
        [nameTextField setStringValue:appControllerAppDelegate.registeredName];
    
    if (!appControllerAppDelegate.registeredCompany)
        [companyTextField setStringValue:@""];
    else
        [companyTextField setStringValue:appControllerAppDelegate.registeredCompany];

    if (!appControllerAppDelegate.registeredProductCode)
        [productKeyTextField setStringValue:@""];
    else
        [productKeyTextField setStringValue:appControllerAppDelegate.registeredProductCode];
}

//- (BOOL)isWindowLoaded {
//    KitchenTimerAppDelegate * kitchenTimerAppDelegate = [[NSApplication sharedApplication] delegate];
//    if (!kitchenTimerAppDelegate.registeredName) {
//        [nameTextField setStringValue:kitchenTimerAppDelegate.registeredName];
//        [companyTextField setStringValue:kitchenTimerAppDelegate.registeredCompany];
//        [productKeyTextField setStringValue:kitchenTimerAppDelegate.registeredProductCode];
//    }
//    
//    return NO;
//}

- (void)dealloc {

    [super dealloc];
}

-(IBAction)activateButtonAction:(id)sender
{
	name = (NSMutableString *)[nameTextField stringValue];
	company = (NSMutableString *)[companyTextField stringValue];
    productKey = (NSMutableString *)[productKeyTextField stringValue];

    // Remove leading and trailing blanks.
    name = (NSMutableString *)[name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    company = (NSMutableString *)[company stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    productKey = (NSMutableString *)[productKey stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    // Remove dashes (-) from product key.
    productKey = (NSMutableString *)[productKey stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    productCodeOwnerNameText= name;
    completeProductCodeText = productKey;
    
    if ([self validateProductCode]) {
    
        // Code validated - save registration info.
        AppController * appControllerAppDelegate = [[NSApplication sharedApplication] delegate];
        appControllerAppDelegate.registeredName = name;
        appControllerAppDelegate.registeredCompany = company;
        appControllerAppDelegate.registeredProductCode = productKey;

        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:@"IsDisabled"];
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"FirstRunDate"];
        [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"RegisteredName"];
        [[NSUserDefaults standardUserDefaults] setObject:company forKey:@"RegisteredCompany"];
        [[NSUserDefaults standardUserDefaults] setObject:productKey forKey:@"RegisteredProductCode"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"IsDisabled"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        // Enable "Check For Updates" menu.
        NSMenu *menu = [[NSApplication sharedApplication] mainMenu];
        NSMenuItem *supportMenuItem = [menu itemWithTitle:@"Support"];
        NSMenu *supportSubMenu = [supportMenuItem submenu];
        [supportSubMenu setAutoenablesItems:NO];
        NSMenuItem *ck4UpdatesMenuItem = [supportSubMenu itemWithTitle:@"Check For Updates"];
        [ck4UpdatesMenuItem setEnabled:YES];
        
        // Enable File/New menu item.                                           //leg20141016 - 1.2.0
        NSMenuItem *fileMenuItem = [menu itemWithTitle:@"File"];
        NSMenu *fileSubMenu = [fileMenuItem submenu];
        [fileSubMenu setAutoenablesItems:NO];
        NSMenuItem *newMenuItem = [fileSubMenu itemWithTitle:@"New"];
        [newMenuItem setEnabled:YES];

        // Enable the plusButton (new reminder).                                //leg20141016 - 1.2.0
        [ARRinterfaceDriver->plusButton setEnabled:YES];
        
        [[self window] close];
    }
}

-(IBAction)cancelButtonAction:(id)sender
{
 	[[self window] close];
}

-(IBAction)unRegisterButtonAction:(id)sender
{
    AppController * appControllerAppDelegate = [[NSApplication sharedApplication] delegate];
    appControllerAppDelegate.registeredName = nil;
    appControllerAppDelegate.registeredCompany = nil;
    appControllerAppDelegate.registeredProductCode = nil;
    name = nil;
    company = nil;
    productKey = nil;
    
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"IsDisabled"];
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"FirstRunDate"];
    [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"RegisteredName"];
    [[NSUserDefaults standardUserDefaults] setObject:company forKey:@"RegisteredCompany"];
    [[NSUserDefaults standardUserDefaults] setObject:productKey forKey:@"RegisteredProductCode"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    // Disable "Check For Updates" menu.
    NSMenu *menu = [[NSApplication sharedApplication] mainMenu];
    NSMenuItem *supportMenuItem = [menu itemWithTitle:@"Support"];
    NSMenu *supportSubMenu = [supportMenuItem submenu];
    [supportSubMenu setAutoenablesItems:NO];
    NSMenuItem *ck4UpdatesMenuItem = [supportSubMenu itemWithTitle:@"Check For Updates"];
    [ck4UpdatesMenuItem setEnabled:NO];

    [[self window] close];
}

#pragma mark Validate Product Code "C" Routines

- (BOOL)validateProductCode
{
	const int		kHashCharIndex1 =  9;
	const int		kHashCharIndex2 = 13;
	const int		kHashCharIndex3 = 15;
	const int		kHashCharIndex4 = 18;
	const UInt8		kProductKeyLength = 20;
	UInt16			keyHash, keyHashIpod, productKeyLen;
	Boolean			ProdKeyIsPackaged = false;
	Str31			madeProductKey, madeProductKeyIpod;
	UInt16			hashValue = 0;
	Str31			outProductKey;
	int				versionNumber = 1;
	unsigned char	ioProductKey[32];
	unsigned char	ioSNKey[32];
	char			hashNumber[5];
	char *			endChar;
	unsigned char *	prodKeyPtr;
	CFStringRef		serialNumberKeyRef, enteredSerialNumberKeyRef, cfMadeProductCode;
	NSString *		madeProductCode;
	NSString *		enteredProductCode;
	NSString *		userName;
    NSString *      prodID = [completeProductCodeText substringToIndex:5];
    BOOL            IsValidCode = NO;
	
	if ([productCodeOwnerNameText length] < 1)
		productCodeOwnerNameText = @" ";		// default to a blank name
	userName = productCodeOwnerNameText;
    
	// Extract the hash value from the product key entered in the product key UITextFields
	ioProductKey[0] = (UInt8)[completeProductCodeText length];
	prodKeyPtr = (unsigned char *)[completeProductCodeText cStringUsingEncoding:NSASCIIStringEncoding];
	strcpy((char *)&ioProductKey[1], (const char *)prodKeyPtr);
	hashNumber[0] = ioProductKey[kHashCharIndex1];
	hashNumber[1] = ioProductKey[kHashCharIndex2];
	hashNumber[2] = ioProductKey[kHashCharIndex3];
	hashNumber[3] = ioProductKey[kHashCharIndex4];
	hashNumber[4] = 0;
	hashValue = strtoul( hashNumber, &endChar, 16 );
	
	// Determine if it is a Name key or a Packaged key
    if (![[[completeProductCodeText substringToIndex:5] uppercaseString ] isEqualToString:kProductCodePrefix])		// hard-coded 2Remember product prefix
		keyHash = 0;			// name key
	else
		keyHash = hashValue;	// packaged key
    
	// Make a product key
	productKeyLen = kProductKeyLength;
	if (keyHash != 0) {
		productKeyLen -= 9;
		ProdKeyIsPackaged = true;
	}
	else {
		keyHash = MakeNameHash((CFStringRef)userName);
		keyHashIpod =  MakeNameHashIpod((CFStringRef)userName);
		MakeProductKeySN( keyHashIpod, madeProductKeyIpod, productKeyLen, productKeyLen, 35 );
	}
	MakeProductKeySN( keyHash, madeProductKey, productKeyLen, productKeyLen, 35 );
    
	// Validate key differently depending on whether or not the key is a packaged key or a name-based key
	if (ProdKeyIsPackaged) {
        // Key is a packaged key which includes embedded hash value
		// Strip the hyphens and hash digits out of entered product key so we can compare against it
		// Example product key layout:
		//								// \pTXNT1ARND5MTB626ABCW	Full product key without hyphens
		//								//	 12345678.012.4.67.90   . = hash digit position, example = 0xDB2B
		//								// \pARN5MT66ACW			Serial number generated from hash value
		//								//   12345678901
		strncpy((char *)&ioSNKey[1], (const char *)&ioProductKey[6], 3);	// ARN
		strncpy((char *)&ioSNKey[4], (const char *)&ioProductKey[10], 3);	// 5MT
		strncpy((char *)&ioSNKey[7], (const char *)&ioProductKey[14], 1);	// 6
		strncpy((char *)&ioSNKey[8], (const char *)&ioProductKey[16], 2);	// 6A
		strncpy((char *)&ioSNKey[10], (const char *)&ioProductKey[19], 2);	// CW
		ioSNKey[0] = (UInt8)11;
		enteredSerialNumberKeyRef = CFStringCreateWithPascalString (kCFAllocatorDefault, ioSNKey, CFStringGetSystemEncoding());
		enteredProductCode = [NSString stringWithFormat:@"%@%@", [prodID uppercaseString], (NSString*)enteredSerialNumberKeyRef];
		
		// Use the hash value to generate a product key
		MakeProductKeySN( hashValue+versionNumber-1, outProductKey, 11, 11, 35);
		serialNumberKeyRef = CFStringCreateWithPascalString (kCFAllocatorDefault, outProductKey, CFStringGetSystemEncoding());
		madeProductCode = [NSString stringWithFormat:@"%@%@", kProductCodePrefix, (NSString*)serialNumberKeyRef];
	} else {
        // Key is a name-based key without a hash value
		cfMadeProductCode = CFStringCreateWithBytes (kCFAllocatorDefault,
                                                     (const UInt8 *)&madeProductKeyIpod[1],
                                                     madeProductKeyIpod[0],
                                                     CFStringGetSystemEncoding(),
                                                     false);
		if (cfMadeProductCode == 0)
			cfMadeProductCode = (CFStringRef)@"UNDEFINEDPRODUCTCODE";

        enteredProductCode = completeProductCodeText;
		madeProductCode = [NSString stringWithString: (NSString *)cfMadeProductCode];
		CFRelease(cfMadeProductCode);
	}
    
	// Compare the generated key with the entered key, put up a warning message if they don't match
	CFStringRef cfProductKey = CFStringCreateWithPascalString (kCFAllocatorDefault, madeProductKey, CFStringGetSystemEncoding());			//•leg - 06/22/10
	CFStringRef cfProductKeyIpod = CFStringCreateWithPascalString (kCFAllocatorDefault, madeProductKeyIpod, CFStringGetSystemEncoding());	//•leg - 06/22/10
	if ((ProdKeyIsPackaged && ![enteredProductCode isEqualToString:(NSString *)madeProductCode]) ||																	//•leg - 06/22/10
		(!ProdKeyIsPackaged && (![enteredProductCode isEqualToString:(NSString *)cfProductKey] &&																	//•leg - 06/22/10
                                ![enteredProductCode isEqualToString:(NSString *)cfProductKeyIpod]))) {																//•leg - 06/22/10
        
		if (ProdKeyIsPackaged) {
            NSAlert *alert = [NSAlert alertWithMessageText:@"Not a valid 2Remember Product Key!" defaultButton:nil alternateButton:nil otherButton:nil informativeTextWithFormat:@""];
            [alert runModal];
		} else {
            NSAlert *alert = [NSAlert alertWithMessageText:@"Not a valid 2Remember Name/Product Key combination!" defaultButton:nil alternateButton:nil otherButton:nil informativeTextWithFormat:@""];
            [alert runModal];
        }
	} else {
        NSAlert *alert = [NSAlert alertWithMessageText:@"Product Key Validated!" defaultButton:nil alternateButton:nil otherButton:nil informativeTextWithFormat:@""];
        [alert runModal];
        IsValidCode = YES;
	}
    
	CFRelease(cfProductKey);																												//•leg - 06/22/10
	CFRelease(cfProductKeyIpod);																											//•leg - 06/22/10
    
	if (ProdKeyIsPackaged) {
		CFRelease(enteredSerialNumberKeyRef);
		CFRelease(serialNumberKeyRef);
	}
    
    return IsValidCode;
}

void MakeProductKeySN( UInt16 inSeed, Str31 outProductKey,
                      UInt8 inProductKeyLength, UInt8 inProductKeyMinLen, UInt16 inMapLen)
{
	//•leg - 08/14/06 - Fix bug where CW version of MakeProductKey didn't
	// 	generate the same key as the Xcode version.
	//	"UInt8	digits[6]" was being overrrun if "v * ch" was
	//	a sufficiently large value, thus overwriting usedSeeds[24]
	//UInt8	ch, map[36], digits[6], usedSeeds[24];
	UInt8	ch, map[36], digits[7], usedSeeds[24];
	int		i, numUsedSeeds;
	UInt32	seed, v;
	
	outProductKey[0] = 0;			// Initialize length
	if (inSeed == 0 || inMapLen > sizeof map || inProductKeyLength >=
        sizeof (Str31) || inProductKeyMinLen > inProductKeyLength )
		return;
	
	seed = inSeed % 800000L;		// Large numbers get us in trouble
	
	// Generate the lookup table
	ch = (seed % (inMapLen + 1));
	for (i = 0; i < inMapLen; i++) {
		ch += 9;
		if ( ch >= inMapLen ) {
			ch -= inMapLen;
		}
		map[i] = (ch < 26) ? ch + 'A' : ch - 26 + '1';
	}
	
	if (inProductKeyMinLen < inProductKeyLength) {
		inProductKeyLength -= seed % (inProductKeyLength - inProductKeyMinLen + 1);
	}
	ch = map[0];
	numUsedSeeds = 0;
	while (outProductKey[0] < inProductKeyLength) {
		for (i = 0, v = seed * ch; v != 0; i++) {
			digits[i] = v % 10;
			v /= 10;
		}
        
		for ( i-= 2; i >= 1 && outProductKey[0] < inProductKeyLength; i -= 2) {
			outProductKey[++outProductKey[0]] =
            map[(digits[i + 1] * 10 + digits[i]) % inMapLen];
		}
		
		usedSeeds[numUsedSeeds++] = ch;
		
		// Generate a new seed
		ch = outProductKey[outProductKey[0]];
		i = 0;
		while (i < numUsedSeeds) {
			for (i = 0; i < numUsedSeeds; i++) {
				if (usedSeeds[i] == ch) {
					ch++;
					break;
				}
			}
		}
	}
}


// -----------------------------------------------------------------------------
//	• MakeNameHash
// -----------------------------------------------------------------------------

unsigned short MakeNameHash( CFStringRef inUserName )
{
	BytePtr	c;
	UInt16	hashValue = 0, len;
	char	userName[31];
	Str31	applicationName = {"\2Remember"};   // hard-coded application name
	union APP_SIG appSignature_iPod;
    
	// Return a zero hash if user name not gettable
	if (!CFStringGetCString (inUserName, userName, sizeof(userName), CFStringGetSystemEncoding()))
		return hashValue;
    
    // hard-coded application signature
	appSignature_iPod.byte[0] = '2';
	appSignature_iPod.byte[1] = 'R';
	appSignature_iPod.byte[2] = 'e';
	appSignature_iPod.byte[3] = 'M';
    
	// Include user name
	c = (BytePtr)&userName;
	len = strlen(userName);
	while (len--) {
		hashValue += *c++;
	}
    
	// Include application name and version
	c = applicationName;
	len = *c++;
	while (len--) {
		hashValue += *c++;
	}
	hashValue += 0x31;					// hard-coded version '1'
    
	c = &appSignature_iPod.byte[0];
	len = sizeof appSignature_iPod;
	while (len--) {
		hashValue += *c++;
	}
	
	return hashValue;
}


// -----------------------------------------------------------------------------
//	• MakeNameHashIpod											[private]
// -----------------------------------------------------------------------------
//

unsigned short MakeNameHashIpod( CFStringRef inUserName )
{
	BytePtr	c;
	UInt16	hashValue = 0, len;
	char	userName[31];
	Str31	applicationName = {"\p2Remember"};   // hard-coded application name
	union APP_SIG appSignature_iPod;
    
	// Return a zero hash if user name not gettable
	if (!CFStringGetCString (inUserName, userName, sizeof(userName), CFStringGetSystemEncoding()))
		return hashValue;
    
	appSignature_iPod.byte[0] = '2';
	appSignature_iPod.byte[1] = 'R';
	appSignature_iPod.byte[2] = 'e';
	appSignature_iPod.byte[3] = 'M';
    
	// Include user name
	c = (BytePtr)&userName;
	len = strlen(userName);
	while (len--) {
		hashValue += *c++;
	}
    
	// Include application name and version
	c = applicationName;
	len = *c++;
	while (len--) {
		hashValue += *c++;
	}
	hashValue += 0x31;					// hard-coded version '1'
    
	c = &appSignature_iPod.byte[0];
	len = sizeof appSignature_iPod;
	while (len--) {
		hashValue += *c++;
	}
	
	return hashValue;
}

@end
