#import "InterfaceDriver.h"

#import "MainWindowController.h"
#import "globals.h"
#import "clickImageView.h"
#import "OpenControl.h"
#import "tabView.h"
#import "agenda.h"
#import "appuntamento.h"
#import "ButtonField.h"
#import "DataWindowController.h"
#import "ButtonField.h"

@implementation InterfaceDriver
-(id)init {
    //  NSNotificationCenter *nc;

    //Salviamo il nostro indirizzo serva a qualcuno
    //Let's save our address for someone                                        //leg20230914 - Translation
    ARRinterfaceDriver=self;

    //Non ci sono situazioni particolari
    //There are no particular situations                                        //leg20231024 - Translation
    displayStatus=0;

    //carichiamo le immagini
    topLeftStraight=[NSImage imageNamed:@"topLeftStraight"];
    topLeftRound=[NSImage imageNamed:@"topLeftRound"];
    topRightStraight=[NSImage imageNamed:@"topRightStraight"];
    topRightRound=[NSImage imageNamed:@"topRightRound"];
    bottomLeftStraight=[NSImage imageNamed:@"bottomLeftStraight"];
    bottomLeftRound=[NSImage imageNamed:@"bottomLeftRound"];
    bottomRightStraight=[NSImage imageNamed:@"bottomRightStraight"];
    bottomRightRound=[NSImage imageNamed:@"bottomRightRound"];
    primoAvvio=YES;
    return self;
}

-(void)awakeFromNib {
    NSNotificationCenter *nc;
    nc=[NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(showDockImage) name:@"ARRshowDockImage" object:nil];
    [nc addObserver:self selector:@selector(hideDockImage) name:@"ARRhideDockImage" object:nil];
    [nc addObserver:self selector:@selector(updateSize) name:@"ARRupdateSize" object:nil];
    [nc addObserver:self selector:@selector(updateButtons) name:@"ARRselectedRecordChanged" object:nil];
    [nc addObserver:self selector:@selector(mainInterfaceUpdate) name:@"ARRmainInterfaceUpdate" object:nil];

    [topLeft setImage:topLeftRound];
    [topRight setImage:topRightRound];
    [bottomLeft setImage:bottomLeftRound];
    [bottomRight setImage:bottomRightRound];
    [titleBar setImage:[NSImage imageNamed:@"titleBar"]];
    [bottomBackground setImage:[NSImage imageNamed:@"bottomCenter"]];

    [leftTabView setImage:[NSImage imageNamed:@"whiteTag"] also:[NSImage imageNamed:@"redTag"]];
    [rightTabView setImage:[NSImage imageNamed:@"whiteTagR"] also:[NSImage imageNamed:@"redTagR"]];

    if (ARRdockedRT==YES) {
        [self showDockImage];
    }
}

-(void)dealloc {
    [topLeftStraight release];
    [topLeftRound release];
    [topRightStraight release];
    [topRightRound release];
    [bottomLeftStraight release];
    [bottomLeftRound release];
    [bottomRightStraight release];
    [bottomRightRound release];
    [super dealloc];
}

-(void)mainInterfaceUpdate {
    NSRect		buttonFieldRect/*,newRect,oldRect*/;
    int			attuale;
    NSMutableArray	*newArray;
    unsigned		howMany;
    appuntamento	*appuntamentoAttuale;
    //  NSImage		*offlineImage;
    // BOOL		needAlsoResize,exitFromLoop;
    // float		heightDelta,dynSize,oldHeight;

    //NSLog(@"Main Interface Update");
    //avvio... altezza � 0!
    buttonFieldRect=[scrollView frame];
    [self updateSize];
    if (buttonFieldRect.size.height==0) {
        //NSLog(@"FirstUpdate");
        //Qui setta tutti quelli che verranno mostrati come visible!!!
        newArray=[ARRagenda dataArray];
        howMany=[newArray count];
        for(attuale=0; attuale<howMany; attuale++) {
            appuntamentoAttuale=[newArray objectAtIndex:attuale];
            if ([appuntamentoAttuale isDeleted]==NO) {
                [appuntamentoAttuale setDisplayed:YES];
            }
        }
        //Ok, adesso ricalcola le dimensioni della finestra e forza un aggiornamento.
        //  [self updateSize];
        [ARRmainWindowController requireUpdate];
    } else [ARRmainWindowController requireUpdate];
}

-(void)showDockImage { //AGGIUNGERE DOCK IN BASSO+{ //Add dock bottom           //leg20230914 - Translation
    //Dobbiamo mostrare quella a sinistra.
    //We need to show the one on the left.                                      //leg20230914 - Translation
    if (displayStatus!=1 && ARRdockedRT==YES && ARRleftRT==YES && ARRbottomRT==NO) {
        [rightTabView setVisible:YES];
        [rightTabView setHilight:NO];
        [leftTabView setVisible:NO];
        [topLeft setImage:topLeftStraight];
        [bottomLeft setImage:bottomLeftStraight];
        [topRight setImage:topRightRound];
        [bottomRight setImage:bottomRightRound];
        displayStatus=1;
        [self redisplay];
    }

    if (displayStatus!=1 && ARRdockedRT==YES && ARRleftRT==NO && ARRbottomRT==NO) {
        [leftTabView setVisible:YES];
        [leftTabView setHilight:NO];
        [rightTabView setVisible:NO];
        [topRight setImage:topRightStraight];
        [bottomRight setImage:bottomRightStraight];
        [topLeft setImage:topLeftRound];
        [bottomLeft setImage:bottomLeftRound];
        displayStatus=1;
        [self redisplay];
    }

    if (displayStatus!=2 && ARRdockedRT==YES && ARRleftRT==NO && ARRbottomRT==YES) {
        [leftTabView setVisible:NO];
        [leftTabView setHilight:NO];
        [rightTabView setVisible:NO];
        [rightTabView setHilight:NO];
        [bottomLeft setImage:bottomLeftStraight];
        [bottomRight setImage:bottomRightStraight];
        [topLeft setImage:topLeftRound];
        [topRight setImage:topRightRound];
        displayStatus=2;
        [self redisplay];
    }
} 

-(void)hideDockImage {
    if (displayStatus!=0) {
        [topLeft setImage:topLeftRound];
        [bottomLeft setImage:bottomLeftRound];
        [topRight setImage:topRightRound];
        [bottomRight setImage:bottomRightRound];
        [rightTabView setVisible:NO];
        [leftTabView setVisible:NO];
        displayStatus=0;
        [self redisplay];

        //Fatto tutto!
        //Everything done!                                                      //leg20231024 - Translation
    }
}

-(void)redisplay {
    //Per prima cosa ridisegnamo la finestra...
    //First let's redraw the window...                                          //leg20230914 - Translation
    [mainWindow display];
    //E adesso invalidiamo l'ombra, cosi' verr� ricalcolata...
    //And now let's invalidate the shadow, so it will be recalculated...        //leg20230914 - Translation
    if (floor(NSAppKitVersionNumber) <= NSAppKitVersionNumber10_1)
    {
        [mainWindow setHasShadow:NO];
        [mainWindow setHasShadow:YES];
    }
    else
        [mainWindow invalidateShadow];
}

- (IBAction)downButton:(id)sender {
    [ARRbuttonField setNumeroPagina:[ARRbuttonField numeroPagina]+1];
}

- (IBAction)minusButton:(id)sender {
    if ([ARRagenda selected]!=nil) {
        //Controlliamo se dobbiamo ridimensionare o no...
        // English Translation:                                                 //leg20180326 - 1.2.0
        // We check if we have to resize it or not ...
        if ([ARRbuttonField maxPages]>1) {
            //Qui non ridimensioniamo niente...
            //cancelliamo
            [mainWindow disableFlushWindow];
            [ARRagenda deleteSelected];
            [ARRdataWindowController dataChanged];
            //ricalcoliamo le nuove posizioni
            [ARRbuttonField recalculateButtons];
            //ridiseegnamo la parte offline
            [ARRbuttonField redrawOffline];
            //Ci poniamo con lo scrolling corretto
            [ARRbuttonField setScrolling];
            //Avvertiamo che c'� stata una modifica totale dell'interfaccia
            [ARRbuttonField modificaTotaleInterfaccia];
            //disegnamoci
            [mainWindow enableFlushWindow];
            [buttonField setNeedsDisplay:YES];
        } else {
            //in questo caso deve fare la transizione
            // English Translation:                                             //leg20180326 - 1.2.0
            //  in this case must make the transition

            //per prima cosa allochiamo le tre immagini che servono
            // English Translation:                                             //leg20180326 - 1.2.0
            //  first we allocate the three images that serve
            [ARRagenda lockResortingUP];
            [ARRbuttonField setDynamicBuffer:[[ARRagenda selected] posizione]];
            [ARRagenda deleteSelected];
            [ARRdataWindowController dataChanged];

            [self updateSize];

            [ARRbuttonField onlyRecalculateButtons];
            [ARRbuttonField redrawOffline];
            [ARRbuttonField modificaTotaleInterfaccia];
            [ARRmainWindowController requireUpdate];
            [ARRagenda lockResortingDOWN];
            [ARRbuttonField recalculateButtons];
            [ARRbuttonField redrawOffline];
            [ARRbuttonField resetDynamicBuffer];


        }
        [ARRbuttonField resetStatus];
    }
}

- (IBAction)plusButton:(id)sender {
    appuntamento	*nuovoAppuntamento;
    NSRect		doveSiamo,doveSaremo;

    // scroll in basso

    [ARRagenda lockResortingUP];
    // Aggiunge uno nuovo in agenda...
    // Adds a new one to the agenda...                                          //leg20230914 - Translation
    nuovoAppuntamento=[ARRagenda addNew];

    // memorizza la rect attuale
    // stores the current rect                                                  //leg20230914 - Translation
    doveSiamo=[mainWindow frame];

    // ricalcola le dimensioni
    // recalculate the dimensions                                               //leg20230914 - Translation
    [self updateSize];
    // mostra datawindow
    // show datawindow                                                          //leg20230914 - Translation
    doveSaremo=[ARRmainWindowController getOpenRect];
    [ARRagenda setSelected:nuovoAppuntamento];
    [ARRbuttonField recalculateButtons];
    [ARRbuttonField redrawOffline];
    [ARRbuttonField setNumeroPagina:[ARRbuttonField maxPages]];
    // se le dimensioni sono variate
    // if the dimensions have changed
    if (doveSiamo.size.height<doveSaremo.size.height) {
        [ARRbuttonField dynamicAdd:nuovoAppuntamento];
        [ARRmainWindowController requireUpdate];
        [ARRbuttonField endDynamicAdd];
    }

    //scroll in basso
    //scroll down                                                               //leg20230914 - Translation
    [ARRbuttonField setNumeroPagina:[ARRbuttonField maxPages]];
    [ARRdataWindowController show];
    [ARRdataWindowController dataChanged];
    [ARRagenda lockResortingDOWN];
    [ARRbuttonField resetStatus];
}

- (IBAction)upButton:(id)sender {
    [ARRbuttonField setNumeroPagina:[ARRbuttonField numeroPagina]-1];
}

-(void)updateSize {
    NSRect screenSize;
    NSRect plusSize;
    NSRect titleSize;
    NSRect targetSize;
    NSRect windowSize,realWindowSize;
    NSRect scrollSize;
    float	maxHeight;

    unsigned		howMany;
    NSMutableArray	*newArray;
    int			yAttuale;
    int			attuale;
    appuntamento	*appuntamentoAttuale;

    //NSLog(@"UpdateSize");
    screenSize=[[mainWindow screen] visibleFrame];
    maxHeight=screenSize.size.height-50;

    //Prendiamo il puntatore all'array che contiene i dati
    newArray=[ARRagenda dataArray];

    //Quanti ce ne sono...
    howMany=[newArray count];

    //Iniziamo a disegnare dal basso
    yAttuale=1;
    //nota: partiamo da 1 perch� poi viene tolto 1 ai 40 pixel di altezza per tutti i bottoni eccetto l'ultimo.

    //Per tutti i pulsanti... Iniziando dall'ultimo...
    for(attuale=(howMany-1); attuale>=0; attuale--)
    {
        //partiamo dal fondo
        appuntamentoAttuale=[newArray objectAtIndex:attuale];
        //deve essere cancellato?
        if ([appuntamentoAttuale isDeleted]==NO) {
            //calcoliamo tutti i dati per il display
            //   newPosition=[appuntamentoAttuale posizione];
            yAttuale--;
            yAttuale=yAttuale+40; //Ci spostiamo in su di 40
        }
    }

    if (yAttuale==1) yAttuale--;

    plusSize=[bottomBackground frame];
    titleSize.size.height=22;
    windowSize=[ARRmainWindowController getOpenRect];
    realWindowSize=[[ARRmainWindowController window] frame];
    scrollSize=[scrollView frame];
    scrollSize.size.height=yAttuale;
    if (yAttuale+plusSize.size.height+titleSize.size.height>maxHeight) {
        scrollSize.size.height=yAttuale;
        targetSize.size.height=scrollSize.size.height+plusSize.size.height+titleSize.size.height;
        while (targetSize.size.height>maxHeight) {
            scrollSize.size.height=scrollSize.size.height-39;
            targetSize.size.height=scrollSize.size.height+plusSize.size.height+titleSize.size.height-2;
        }

    } else targetSize.size.height=yAttuale+plusSize.size.height+titleSize.size.height-2;

    targetSize.origin.y=realWindowSize.origin.y+realWindowSize.size.height-targetSize.size.height;
    if (targetSize.origin.y<screenSize.origin.y) targetSize.origin.y=screenSize.origin.y;
    if (targetSize.origin.y+targetSize.size.height>screenSize.origin.y+screenSize.size.height) targetSize.origin.y=screenSize.origin.y+screenSize.size.height-targetSize.size.height;

    targetSize.origin.x=realWindowSize.origin.x;
    targetSize.size.width=260;

    [ARRmainWindowController setOpenRect:targetSize];
}

- (BOOL)isMouseOutside {
    NSPoint	mouseLoc;
    NSPoint	localMouseLoc;
    NSRect	windowLoc;

    windowLoc=[mainWindow frame];
    //primo, � interno alla backView?
    //first, is it internal to the backView?                                    //leg20230914 - Translation
    mouseLoc=[NSEvent mouseLocation];
    // localMouseLoc=[backView convertPoint:mouseLoc fromView:nil];
    localMouseLoc.x=mouseLoc.x-windowLoc.origin.x;
    localMouseLoc.y=mouseLoc.y-windowLoc.origin.y;

    if (localMouseLoc.x>=30 && localMouseLoc.x<windowLoc.size.width-30 && localMouseLoc.y>0 && localMouseLoc.y<=windowLoc.size.height){

        return NO;
    } else if ([backView mouse:localMouseLoc inRect:[leftTabView frame]]==YES || [backView mouse:localMouseLoc inRect:[rightTabView frame]]==YES) {

        if (ARRbottomRT==NO) return NO; else return YES;
    } //controlla se � interno alla finestra...
      //check if it's inside the window...                                      //leg20230914 - Translation
    return YES;
}

-(void)drawRect:(NSRect)rect {
    [self updateButtons];
}

-(void)updateButtons {
    NSRect buttonFieldVisible;
    NSRect buttonFieldRect;

    if ([ARRagenda selected]!=nil) [minusButton setEnabled:YES]; else [minusButton setEnabled:NO];
    buttonFieldVisible=[[ARRbuttonField enclosingScrollView] documentVisibleRect];
    buttonFieldRect=[buttonField frame];

    if ([ARRbuttonField numeroPagina]>1) [upButton setEnabled:YES]; else [upButton setEnabled:NO];
    if ([ARRbuttonField numeroPagina]<[ARRbuttonField maxPages]) [downButton setEnabled:YES]; else [downButton setEnabled:NO];
}

-(void)deleteKey {
    [minusButton performClick:self];
}

-(void)enterKey {
    if ([ARRagenda selected]!=nil) [ARRbuttonField doubleClickOnSelected]; else [plusButton performClick:self];
}


@end
