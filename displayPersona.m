//
//  displayPersona.m
// 2Remember
//
//
//  Created by Andrea Rincon Ray on Tue Mar 25 2003.
//  Version 1.0-1.0.5 by Andrea Rincon Ray on or beforer Sun Feb 09 2003.
//  Versions after 1.0.6 by James Lee and Lewis Garrett, Tropical Software, Inc. 

//  2Remember
//  Copyright (c) 2011 Tropical Software, Inc. All rights reserved.
//

#import "displayPersona.h"


@implementation displayPersona
-(void)setNome:(NSString*)stri {
    [stri retain];
    [name release];
    name=stri;
}

-(void)setEmail:(NSString*)stri {
    [stri retain];
    [email release];
    email=stri;
}

-(void)setPhone:(NSString*)stri {
    [stri retain];
    [phone release];
    phone=stri;
}

-(NSString*)name {
    NSString *tempo=[[NSString alloc] initWithString:@""];
    if (name!=nil) tempo=[tempo stringByAppendingFormat:@" %@",name];
    if (cognome!=nil) tempo=[tempo stringByAppendingFormat:@" %@",cognome];
    if (azienda!=nil) tempo=[tempo stringByAppendingFormat:@" %@",azienda];
    if ([tempo length]>1) tempo=[tempo substringFromIndex:1];
    return tempo;
}

-(NSString*)phone {
    return phone;
}

-(NSString*)email {
    return email;
}

-(void)setKind:(NSString*)stri {
    //kind=ABCopyLocalizedPropertyOrLabel(stri);
    [stri retain];
    [kind release];
    kind=stri;
}

-(void)setCognome:(NSString*)stri {
    [stri retain];
    [cognome release];
    cognome=stri;
}
-(void)setAzienda:(NSString*)stri {
    [stri retain];
    [azienda release];
    azienda=stri;
}

-(NSString*)descrizione {
    NSString *tempo=[[NSString alloc] initWithString:@""];
    if (name!=nil) tempo=[tempo stringByAppendingFormat:@" %@",name];
    if (cognome!=nil) tempo=[tempo stringByAppendingFormat:@" %@",cognome];
    if (azienda!=nil) tempo=[tempo stringByAppendingFormat:@" %@",azienda];
    if (kind!=nil) tempo=[tempo stringByAppendingFormat:@" (%@)",kind];
    if (phone!=nil) tempo=[tempo stringByAppendingFormat:@" %@",phone];
    if (email!=nil) tempo=[tempo stringByAppendingFormat:@" %@",email];
    if ([tempo length]>1) tempo=[tempo substringFromIndex:1];
    return tempo;
}

@end
