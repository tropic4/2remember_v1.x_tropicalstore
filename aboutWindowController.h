//
//  aboutWindowController.h
//
//          2Remember- A simple reminder program

//  2Remember
//
//
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//

#import <AppKit/AppKit.h>


@interface aboutWindowController : NSWindowController {
    IBOutlet id appNameField;
    IBOutlet id copyrightField;
    IBOutlet id creditsField;
    IBOutlet id versionField;
    NSTimer *scrollTimer;
    float currentPosition;
    float maxScrollHeight;
    NSTimeInterval startTime;
    BOOL restartAtTop;
}
+ (aboutWindowController *)sharedInstance;
- (IBAction)showPanel:(id)sender;
- (IBAction)website:(id)sender;
- (IBAction)email:(id)sender;
@end
