//
//  aboutWindowController.m
//
//          2Remember- A simple reminder program

//  2Remember
//
//
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//

#import "aboutWindowController.h"


@implementation aboutWindowController

static aboutWindowController *sharedInstance=nil;

+ (aboutWindowController *)sharedInstance {
    return sharedInstance ? sharedInstance : [[self alloc] init];
}

-(id)init {
    if (sharedInstance) {
        [self dealloc];
    } else {
        sharedInstance =[super init];
    }
    return sharedInstance;
}

-(IBAction)showPanel:(id)sender {
    if (!appNameField) {
        NSWindow *theWindow;
        NSString *creditsPath;
        NSAttributedString *creditsString;
        NSString *appName;
        NSString *versionString;
        NSString *buildString;                                                  //leg20180313 - 1.2.0
        NSString *copyrightString;
        NSDictionary *infoDictionary;
        CFBundleRef localInfoBundle;
        NSDictionary *localInfoDict;

        if (![NSBundle loadNibNamed:@"AboutBox" owner:self])
        {
            NSLog(@"Failed to load AboutBox.nib");
            return;
        }

        theWindow=[appNameField window];

        infoDictionary=[[NSBundle mainBundle] infoDictionary];

        localInfoBundle = CFBundleGetMainBundle();
        localInfoDict=(NSDictionary*) CFBundleGetLocalInfoDictionary(localInfoBundle);

        appName= [localInfoDict objectForKey:@"CFBundleName"];
        [appNameField setStringValue:appName];

        [theWindow setTitle:[NSString stringWithFormat:@"About %@", appName]];

//        versionString=[infoDictionary objectForKey:@"CFBundleVersion"];
//        [versionField setStringValue:[NSString stringWithFormat:@"Version %@", versionString]];
        versionString=[infoDictionary objectForKey:@"CFBundleShortVersionString"];                                  //leg20180313 - 1.2.0
        buildString=[infoDictionary objectForKey:@"CFBundleVersion"];                                               //leg20180313 - 1.2.0
        [versionField setStringValue:[NSString stringWithFormat:@"Version %@(%@)", versionString, buildString]];    //leg20180313 - 1.2.0

        creditsPath=[[NSBundle mainBundle] pathForResource:@"Credits" ofType:@"rtf"];

        creditsString = [[NSAttributedString alloc] initWithPath:creditsPath documentAttributes:nil];

        [creditsField replaceCharactersInRange:NSMakeRange(0,0)
                                       withRTF:[creditsString RTFFromRange:
                                           NSMakeRange(0, [creditsString length] )
                                                        documentAttributes:nil]];

        copyrightString=[localInfoDict objectForKey:@"NSHumanReadableCopyright"];
        
        [copyrightField setStringValue:copyrightString];

        maxScrollHeight=[[creditsField string] length];
        
        // Fix MAS Issue #017 - "About box text not visible in Dark mode."      //leg20201219 - 1.3.0
        //  Insure "About Window" Appearance is Aqua regardless of Dark Mode.
        if (@available(macOS 10.9, *)) {
//            theWindow.appearance = [NSAppearance appearanceNamed: NSAppearanceNameAqua];
            // Set appearance on just rolling credits instead of the window.    //Leg20201222 - 1.3.0
            NSTextView * _creditsField = creditsField;
            _creditsField.appearance = [NSAppearance appearanceNamed: NSAppearanceNameAqua];
        } else {
            // Not an Issue prior to Dark Mode option in MacOS.
        }
        
        [theWindow setExcludedFromWindowsMenu:YES];
        [theWindow setMenu:nil];
        [theWindow center];
    }

    if (![[appNameField window] isVisible]) {
        currentPosition=0;
        restartAtTop=NO;
        startTime=[NSDate timeIntervalSinceReferenceDate] + 5.0;
        [creditsField scrollPoint:NSMakePoint(0,0)];
    }

    [[appNameField window] makeKeyAndOrderFront:nil];
}

-(void)windowDidBecomeKey:(NSNotification*)notification {
    scrollTimer=[NSTimer scheduledTimerWithTimeInterval:1/2
                                                 target:self
                                               selector:@selector(scrollCredits:)
                                               userInfo:nil
                                                 repeats:YES];
}

-(void)windowDidResignKey:(NSNotification*)notification {
    [scrollTimer invalidate];
}

-(void)scrollCredits:(NSTimer*)timer {
    if ([NSDate timeIntervalSinceReferenceDate]>=startTime) {
        if (restartAtTop) {
            startTime=[NSDate timeIntervalSinceReferenceDate]+3.0;
            restartAtTop=NO;
            [creditsField scrollPoint:NSMakePoint(0,0)];
            return;
        }

        if (currentPosition>=maxScrollHeight) {
            startTime=[NSDate timeIntervalSinceReferenceDate]+3.0;
            currentPosition=0;
            restartAtTop=YES;
        } else {
            [creditsField scrollPoint:NSMakePoint(0,currentPosition)];

            currentPosition+=0.01;
        }
    }
}

- (IBAction)website:(id)sender {
[[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://www.tropic4.com/"]];
}

- (IBAction)email:(id)sender {
    // Fix Issue #017 - "The “Email” button in the About box doesn’t seem       //leg20201219 - 1.3.0
    //  to do anything."
    //  Adapted iAddressX's use of CFURLCreateStringByAddingPercentEscapes() to
    //  prepare mailto URL.
    //[[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"mailto:support@tropic4.com "]];
    NSString *aString = @"mailto:support@tropic4.com?subject=2Remember Support";
    [[NSWorkspace sharedWorkspace] openURL:
     [NSURL URLWithString:
      (NSString *)CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef) aString, NULL, NULL, kCFStringEncodingUTF8)]];
}

@end
