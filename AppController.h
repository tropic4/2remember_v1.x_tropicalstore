//
//  AppController.h
// 2Remember
//
//
//  Created by Andrea Rincon Ray on Fri Feb 07 2003.
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import "ActivateProductWindowController.h"                                     //leg20141016 - 1.2.0

@class appuntamento;
@class ViewReadMe;                                                              //leg20201204 - 1.3.0

@interface AppController : NSObject {
    int		numeroAllarmi;
    NSTimer	*timerGenerale;
    NSTimer	*timerSpegni;
    BOOL	registerMenu;

    NSMutableString *registeredProductCode;                                     //leg20141016 - 1.2.0
    NSMutableString *registeredName;                                            //leg20141016 - 1.2.0
    NSMutableString *registeredCompany;                                         //leg20141016 - 1.2.0

    ActivateProductWindowController *activateProductWindowController;           //leg20141016 - 1.2.0
    
    ViewReadMe*                 _viewReadMeWindowController;                    //leg20201204 - 1.3.0
}

@property (nonatomic, strong) NSMutableString *registeredProductCode;           //leg20141016 - 1.2.0
@property (nonatomic, strong) IBOutlet NSMutableString *registeredName;         //leg20141016 - 1.2.0
@property (nonatomic, strong) IBOutlet NSMutableString *registeredCompany;      //leg20141016 - 1.2.0

-(IBAction)jumpToTropic4BuyPage:(id)sender;
-(IBAction)showActivateProductKeyWindow:(id)sender;

- (void)notifyOfAlarm:(appuntamento*)who;
- (void)endNotificationOfAlarm:(appuntamento*)who;
- (void)blinkON;
- (void)blinkOFF;
- (IBAction)showAboutBox:(id)sender;
- (IBAction)checkForNewVersion:(id)sender;
- (IBAction)showPreferences:(id)sender;

- (IBAction)newMenu:(id)sender;
- (IBAction)openMenu:(id)sender;

- (IBAction)registrazione:(id)sender;

- (void)setRegisterMenu:(BOOL)val;
- (void)mainInterfaceDispatch:(int)event;
- (void)inizializzaPreferenze;
- (void)leggePreferenzeUtente;
- (void)salvaPreferenzeUtente;
- (void) salvataggioPeriodico;

@end

