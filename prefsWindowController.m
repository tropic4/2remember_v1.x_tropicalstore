//
//  prefsWindowController.m
//
//          2Remember- A simple reminder program

//  2Remember
//
//
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//

#import "prefsWindowController.h"
#import "globals.h"
#import "agenda.h"


@implementation prefsWindowController

static prefsWindowController *sharedInstance=nil;

+ (prefsWindowController *)sharedInstance {
    return sharedInstance ? sharedInstance : [[self alloc] init];
}

-(id)init {
    if (sharedInstance) {
        [self dealloc];
    } else {
        sharedInstance =[super init];
    }
    return sharedInstance;
}

-(IBAction)showPanel:(id)sender {
    if (!blinkButton) {
        NSWindow *theWindow;
        
        if (![NSBundle loadNibNamed:@"Preferences" owner:self]) {
            NSLog(@"Failed to load Preferences.nib");
            return;
        }

        theWindow= [blinkButton window];

        [theWindow setExcludedFromWindowsMenu:YES];
        [theWindow setMenu:nil];
        [theWindow center];
    }

    if (![[blinkButton window] isVisible]) {
        [self updateUI];
    }

    [[blinkButton window] makeKeyAndOrderFront:nil];
}

- (IBAction)setBlink:(id)sender {
    if ([blinkButton state]==NSOffState) ARRalarmTab=NO; else ARRalarmTab=YES;
}

- (IBAction)setBounce:(id)sender {
    if ([bounceButton state]==NSOffState) ARRalarmIcon=NO; else ARRalarmIcon=YES;
}

- (IBAction)setBeep:(id)sender {
    if ([beepButton state]==NSOffState) ARRalarmSound=NO; else ARRalarmSound=YES;
}

- (IBAction)setAlert:(id)sender {
    if ([alertButton state]==NSOffState) ARRalarmAlert=NO; else ARRalarmAlert=YES;
}

- (IBAction)setStartup:(id)sender {
    if ([startUpButton state]==NSOffState) ARRautoStartup=NO; else ARRautoStartup=YES;
}

- (IBAction)setAnimation:(id)sender {
    if ([animationButton state]==NSOffState) ARRanimation=NO; else ARRanimation=YES;
}

- (IBAction)setAutoShow:(id)sender {
    if ([locAutoShow state]==NSOffState) ARRautoOpen=NO; else ARRautoOpen=YES;
}

- (IBAction)setSorting:(id)sender {
    ARRdataSorting=[sortingMenu indexOfSelectedItem];
    [ARRagenda mightNeedResorting];
}

-(void) updateUI {
    if (ARRalarmTab==NO) [blinkButton setState:NSOffState]; else [blinkButton setState:NSOnState];
    if (ARRalarmIcon==NO) [bounceButton setState:NSOffState]; else [bounceButton setState:NSOnState];
    if (ARRalarmSound==NO) [beepButton setState:NSOffState]; else [beepButton setState:NSOnState];
    if (ARRalarmAlert==NO) [alertButton setState:NSOffState]; else [alertButton setState:NSOnState];
    if (ARRautoStartup==NO) [startUpButton setState:NSOffState]; else [startUpButton setState:NSOnState];
    if (ARRanimation==NO) [animationButton setState:NSOffState]; else [animationButton setState:NSOnState];
    if (ARRautoOpen==NO) [locAutoShow setState:NSOffState]; else [locAutoShow setState:NSOnState];
    [sortingMenu selectItemAtIndex:ARRdataSorting];
}
@end

