//
//  agenda.m
// 2Remember
//
//
//  Created by Andrea Rincon Ray on Wed Feb 26 2003.
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//

#import "agenda.h"
#import "appuntamento.h"
#import "globals.h"


@implementation agenda
//*** Initialization ************************************************************//
//***Inizializzazione************************************************************//
+(void)initialize {
    if (self==[agenda class]) {
        [self setVersion:1];
    }
}

-(id)init {
    if (self=[super init]) {
        appuntamenti=[[NSMutableArray alloc] init];
        progressiveNumber=1;
        selected=nil;
        errorsWithArchive=NO;
        savingEnabled=YES;
        autoResort=YES;
        lockResorting=0;
    }
    return self;
}

//*** De-Initialization ************************************************************//
//*** Deinizializzazione ************************************************************//

-(void)dealloc {
    [appuntamenti release];
    [super dealloc];
}

//*** Accessories ************************************************************//
//***Accessori************************************************************//

-(void)toggleSelected:(appuntamento*)appunt {
    NSNotificationCenter *nc;
    nc = [NSNotificationCenter defaultCenter];

    if (selected==nil){
        selected=appunt;
        [selected selected:YES];
        [selected hilight:NO];
        [nc postNotificationName:@"ARRselectedRecordChanged" object:self];
    } else if (selected!=appunt) {
        [selected selected:NO];
        [selected hilight:NO];
        selected=appunt;
        [selected selected:YES];
        [selected hilight:NO];
        [nc postNotificationName:@"ARRselectedRecordChanged" object:self];
    } else {
        [selected selected:NO];
        [selected hilight:NO];
        selected=nil;
        [nc postNotificationName:@"ARRselectedRecordChanged" object:self];
    }
}

-(void)setSelected:(appuntamento*)appunt {
    NSNotificationCenter *nc;
    nc = [NSNotificationCenter defaultCenter];
    if (selected!=appunt&&selected!=nil){
        [selected selected:NO];
    }
    selected=appunt;
    if (selected!=nil) {
        [selected selected:YES];
    }
    [nc postNotificationName:@"ARRselectedRecordChanged" object:self];
}

-(appuntamento*)selected { return selected; }

-(NSMutableArray*)dataArray { return appuntamenti; }

-(NSNumber*)progressiveNumber { return [NSNumber numberWithUnsignedInt:progressiveNumber]; }

//***Utility************************************************************//
// Fixed "Incompatible function pointer types…": return from intSort() was      //leg20240416 - 1.4.0
//  defined as "int" instead of "NSInteger". Changed function prototype to:
//    NSInteger intSort(id num1, id num2, void *context);

-(void)mightNeedResorting {
    if (autoResort==YES&&lockResorting==0) {
        if (ARRdataSorting!=0) [appuntamenti sortUsingFunction:intSort context:nil];
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        [nc postNotificationName:@"ARRdataChanged" object:self];
    }
}

-(void)lockResortingUP {
    lockResorting++;
}

-(void)lockResortingDOWN {
    if (lockResorting>0) lockResorting--;
    [self mightNeedResorting];
}

//OK!!! GOOD!
//int intSort(id num1, id num2, void *context) {
// Fixed "Incompatible function pointer types…": return from intSort() was      //leg20240416 - 1.4.0
//  defined as "int" instead of "NSInteger". Changed function prototype.
NSInteger intSort(id num1, id num2, void *context) {

    appuntamento *a1 = (appuntamento*)num1;
    appuntamento *a2 = (appuntamento*)num2;

    if ([a1 date]==nil&&[a2 date]==nil) {
        if ([a1 absoluteCreation] > [a2 absoluteCreation]) return NSOrderedDescending;
        else return NSOrderedAscending;
    }
    if ([a1 date]==nil) return NSOrderedDescending;
    if ([a2 date]==nil) return NSOrderedAscending;


    if ([a1 secondsFromNow] < [a2 secondsFromNow]) {
        if (ARRdataSorting==1) return NSOrderedDescending;
        if (ARRdataSorting==2) return NSOrderedAscending;
    }
    else if ([a1 secondsFromNow] > [a2 secondsFromNow]) {
        if (ARRdataSorting==1) return NSOrderedAscending;
        if (ARRdataSorting==2) return NSOrderedDescending;
    }
    else
        return NSOrderedSame;
    return NSOrderedSame;
}

//*** Salvation ************************************************************//
//***Salvataggio************************************************************//

-(void)load {
    //NSString	*datapath=@"~/Library/Preferences/com.andreaegreta.2remember.data";
    //NSString	*datapath=@"~/Library/Preferences/com.tropic4.2remember.data";			//leg20110706 - 1.1
    NSString	*datapath=@"~/Library/Application Support/Tropical Software, Inc./2Remember/2Remember_Data.data";			//leg20110719 - 1.1
    NSData	*myBuffer;
    NSNotificationCenter	*nc;

	// Create the folder hierarchy required by Apple Store//leg20110719 - 1.1
	NSError *error = nil;
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSString	*folderpath=@"~/Library/Application Support/Tropical Software, Inc./2Remember/";
	standardDataPath=[[folderpath stringByStandardizingPath] retain];
	[fileManager createDirectoryAtPath:standardDataPath withIntermediateDirectories:YES attributes:nil error:&error];
	if ([error code] != 0) {
		NSLog(@"Error creating directory for data file! Error code=%d", [error code]);
		return;
	}

    autoResort=NO;
    standardDataPath=[[datapath stringByStandardizingPath] retain];
    //standardDataPath=[[datapath stringByExpandingTildeInPath] retain];
    myBuffer = [NSData dataWithContentsOfFile:standardDataPath];
    if (myBuffer!=nil) {
        [appuntamenti release];
        NS_DURING
            appuntamenti=[[NSUnarchiver unarchiveObjectWithData:myBuffer] retain];
        NS_HANDLER
            NSLog(@"Exception loading data in agenda.m");
            appuntamenti=[[NSMutableArray alloc] init];
        NS_ENDHANDLER
            if (errorsWithArchive==YES) {
				// Requires translation
                //Richiede traduzione
                int button = NSRunAlertPanel(@"There is a problem:", @"The database document (where I store your Todo) I found on your HD has been created by a newer version of this program. It's not safe for me to read and modify it. You can choose either to: Look for an updated version on the web, Quit the Application or overwrite the file (losing your saved Todo's)", @"Website", @"Overwrite", @"Quit");
                if (button==1) {
                    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://www.andreaegreta.com/softwareupdates/"]];
                    savingEnabled=NO;
                    [NSApp terminate:self];

                } else if (button==-1) {
                    savingEnabled=NO;
                    [NSApp terminate:self];
                }
            }
    }

    // Fixed "Incompatible function pointer types…": return from intSort() was      //leg20240416 - 1.4.0
    //  defined as "int" instead of "NSInteger". Changed function prototype to:
    //    NSInteger intSort(id num1, id num2, void *context);
    if (ARRdataSorting!=0) [appuntamenti sortUsingFunction:intSort context:nil];
        nc = [NSNotificationCenter defaultCenter];
        progressiveNumber++;
        [nc postNotificationName:@"ARRnumberOfRecordsChanged" object:self];

        autoResort=YES;
}

-(void)save {
    NSData	*myBuffer;
    BOOL	risultato;

    if (savingEnabled==YES) {
        myBuffer=[NSArchiver archivedDataWithRootObject:appuntamenti];
        risultato=[myBuffer writeToFile:standardDataPath atomically:YES];
    }
}
    
//*** Action ************************************************************//
//***Azioni************************************************************//

-(appuntamento*)addNew {
    appuntamento	*nuovoAppuntamento;
    
    progressiveNumber++;
    nuovoAppuntamento=[[appuntamento alloc] init];
    [appuntamenti addObject:nuovoAppuntamento];
    [nuovoAppuntamento autorelease];
    [nuovoAppuntamento redrawImage];
    return nuovoAppuntamento;
}

-(void)selectNextObject {
    // must select and then just set the scrolling!
	//deve selezionare e poi settare giusto lo scrolling!
    if (selected!=nil) {
        int attuale=[appuntamenti indexOfObject:selected];
        if (attuale<[appuntamenti count]-1) [self setSelected:[appuntamenti objectAtIndex:++attuale]];
    } else {
        [self setSelected:[appuntamenti objectAtIndex:[appuntamenti count]-1]];
    }
}

-(void)selectPreviousObject {
    if (selected!=nil) {
        int attuale=[appuntamenti indexOfObject:selected];
        if (attuale>0) [self setSelected:[appuntamenti objectAtIndex:--attuale]];
    } else {
        [self setSelected:[appuntamenti objectAtIndex:0]];
    }
}

-(void)deleteSelected {
    if (selected!=nil) {
        appuntamento *ultimo=selected;
        [self setSelected:nil];
        progressiveNumber++;
        [ultimo setDeleted:YES];
        [ultimo hilight:NO];
        [ultimo selected:NO];
        [ultimo setAlarm:NO];
        [appuntamenti removeObject:ultimo];
    }
}

-(void)shouldSaveAsap {
    progressiveNumber++;
}

//*** Exceptions ************************************************************//
//***Eccezioni************************************************************//

-(void)inconsistentArchiveVersion {
    errorsWithArchive=YES;
}
@end
