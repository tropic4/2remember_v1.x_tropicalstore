//
//  DataWindowController.h
// 2Remember
//
//
//  Created by Andrea Rincon Ray on Tue Mar 11 2003.
//  Version 1.0-1.0.5 by Andrea Rincon Ray on or beforer Sun Feb 09 2003.
//  Versions after 1.0.6 by James Lee and Lewis Garrett, Tropical Software, Inc. 

//  2Remember
//  Copyright (c) 2011 Tropical Software, Inc. All rights reserved.
//

#import <AppKit/AppKit.h>
#import "globals.h"

@interface DataWindowController : NSWindowController {
    appuntamento	*appuntamentoAttuale;
    BOOL		getNotification;
}
-(void)hide;
-(void)show;
-(void)setData:(appuntamento*)appuntamento;
-(void)setPosition:(NSPoint)point;
-(void)saveIfNeeded;
-(void)updateUI;
-(void)dataChanged;
@end
