#import "DataWindowInterfaceDriver.h"
#import "globals.h"
#import "agenda.h"

#import "findWindowController.h"
#import <AddressBook/AddressBook.h>
#import "appuntamento.h"
#import "displayPersona.h"
#import "DataWindowSubclass.h"
#import <AddressBook/ABAddressBookC.h>


@implementation DataWindowInterfaceDriver
-(id)init
{
    NSNotificationCenter *nc;
    ARRdataWindowInterfaceDriver=self;
    excludeModified=0;
    visibileAdesso=NO;
    pendingUpdate=NO;
    nc=[NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(setupFormatters) name:@"ARRdataFormatChanged" object:nil];
    [self loadChildWindow];
    return self;
}

-(void)awakeFromNib
{
    [self setupFormatters];
}

-(void) saveIfNeeded{
    //NSLog(@"DWID saveIfNeeded");
    [self saveContext];
}

-(void)setupFormatters {
    NSDateFormatter	*dForm;

    //creo il formatter e lo attacco al clip
    dForm=[[NSDateFormatter alloc] initWithDateFormat:ARRdateTimeOrdering allowNaturalLanguage:YES];

    //lo attacco!
    [phoneDate setFormatter:dForm];
    [meetDate setFormatter:dForm];
    [emailDate setFormatter:dForm];
    [gotoDate setFormatter:dForm];
    [websiteDate setFormatter:dForm];
    [buyDate setFormatter:dForm];
    [todoDate setFormatter:dForm];
}

-(void)isVisible:(BOOL)val {
    visibileAdesso=val;
    if (pendingUpdate==YES&&visibileAdesso==YES) {
        pendingUpdate=NO;
        [self updateUI];
    }
}

-(void)lockActions:(BOOL)val{
    if (val==YES) [self excludeModifiedUP];
    if (val==NO) [self excludeModifiedDOWN];
}

-(void)saveContext {
    if (visibileAdesso==YES) {
    [ARRagenda lockResortingUP];
    if (currentTab==0) { //phone
        [appunt setPerson:[phonePerson stringValue]];
        [appunt setPhone:[phoneNumber stringValue]];
        [appunt setDate:[phoneDate objectValue]];
        [appunt setSTime:[phoneTime stringValue]];
        [appunt setNote:[phoneNotes string]];
        [appunt setAlarm:[phoneAlarm state]];
    }

    if (currentTab==1) { //meet
        [appunt setPerson:[meetPerson stringValue]];
        [appunt setPhone:[meetPhone stringValue]];
        [appunt setPlace:[meetWhere stringValue]];
        [appunt setDate:[meetDate objectValue]];
        [appunt setSTime:[meetTime stringValue]];
        [appunt setNote:[meetNotes string]];
        [appunt setAlarm:[meetAlarm state]];
    }

    if (currentTab==2) { //email
        [appunt setPerson:[emailPerson stringValue]];
        [appunt setEmail:[emailEmail stringValue]];
        [appunt setDate:[emailDate objectValue]];
        [appunt setSTime:[emailTime stringValue]];
        [appunt setNote:[emailNotes string]];
        [appunt setAlarm:[emailAlarm state]];
    }

    if (currentTab==3) {//go to
        [appunt setPlace:[gotoWhere stringValue]];
        [appunt setDate:[gotoDate objectValue]];
        [appunt setSTime:[gotoTime stringValue]];
        [appunt setNote:[gotoNotes string]];
        [appunt setAlarm:[gotoAlarm state]];
    }

    if (currentTab==4) {//website
        [appunt setPlace:[websiteUrl stringValue]];
        [appunt setDate:[websiteDate objectValue]];
        [appunt setSTime:[websiteTime stringValue]];
        [appunt setNote:[websiteNotes string]];
        [appunt setAlarm:[websiteAlarm state]];
    }

    if (currentTab==5) { //buy
        [appunt setWhat:[buyWhat stringValue]];
        [appunt setPlace:[buyWhere stringValue]];
        [appunt setDate:[buyDate objectValue]];
        [appunt setSTime:[buyTime stringValue]];
        [appunt setNote:[buyNotes string]];
        [appunt setAlarm:[buyAlarm state]];
    }

    if (currentTab==6) { //to do
        [appunt setTodo:[todoTodo stringValue]];
        [appunt setDate:[todoDate objectValue]];
        [appunt setSTime:[todoTime stringValue]];
        [appunt setNote:[todoNotes string]];
        [appunt setAlarm:[todoAlarm state]];
    }

    [appunt setKind:currentTab];
    [ARRagenda lockResortingDOWN];
    }
}

-(void) updateUI {
   // unsigned len;
    if (visibileAdesso==NO) {
        pendingUpdate=YES;
    } else {
    [self excludeModifiedUP];
    
    if (appunt!=nil) {
        [appunt release];
        appunt=nil;
    }

    appunt=[ARRagenda selected];
    [appunt retain];
    
   // NSLog(@"DWID updateUI");
    [self hideChildWindow];
    currentTab=[appunt kind];
    [dataWindow disableFlushWindow];
    [tabView selectTabViewItemAtIndex:currentTab];
    if (currentTab==0) { //phone
        [phonePerson setStringValue:[appunt person]];
        [phoneNumber setStringValue:[appunt phone]];
        [phoneDate setObjectValue:[appunt date]];
        [phoneTime setStringValue:[appunt sTime]];
        if ([appunt note]!=nil) [phoneNotes setString:[appunt note]]; else [phoneNotes setString:@""];
        if ([appunt canSetAlarm]==YES) [phoneAlarm setState:[appunt alarm]]; else [phoneAlarm setState:NO];
        if ([appunt canSetAlarm]==YES) [phoneAlarm setEnabled:YES]; else [phoneAlarm setEnabled:NO];
    }

    if (currentTab==1) { //meet
        [meetPerson setStringValue:[appunt person]];
        [meetPhone setStringValue:[appunt phone]];
        [meetWhere setStringValue:[appunt place]];
        [meetDate setObjectValue:[appunt date]];
        [meetTime setStringValue:[appunt sTime]];
        [meetNotes setString:[appunt note]];
        if ([appunt canSetAlarm]==YES) [meetAlarm setState:[appunt alarm]]; else [meetAlarm setState:NO];
        if ([appunt canSetAlarm]==YES) [meetAlarm setEnabled:YES]; else [meetAlarm setEnabled:NO];
    }

    if (currentTab==2) { //email
        [emailPerson setStringValue:[appunt person]];
        [emailEmail setStringValue:[appunt email]];
        [emailDate setObjectValue:[appunt date]];
        [emailTime setStringValue:[appunt sTime]];
        [emailNotes setString:[appunt note]];
        if ([appunt canSetAlarm]==YES) [emailAlarm setState:[appunt alarm]]; else [emailAlarm setState:NO];
        if ([appunt canSetAlarm]==YES) [emailAlarm setEnabled:YES]; else [emailAlarm setEnabled:NO];
    }

    if (currentTab==3) {//go to
        [gotoWhere setStringValue:[appunt place]];
        [gotoDate setObjectValue:[appunt date]];
        [gotoTime setStringValue:[appunt sTime]];
        [gotoNotes setString:[appunt note]];
        if ([appunt canSetAlarm]==YES) [gotoAlarm setState:[appunt alarm]]; else [gotoAlarm setState:NO];
        if ([appunt canSetAlarm]==YES) [gotoAlarm setEnabled:YES]; else [gotoAlarm setEnabled:NO];
    }

    if (currentTab==4) {//website
        [websiteUrl setStringValue:[appunt place]];
        [websiteDate setObjectValue:[appunt date]];
        [websiteTime setStringValue:[appunt sTime]];
        [websiteNotes setString:[appunt note]];
        if ([appunt canSetAlarm]==YES) [websiteAlarm setState:[appunt alarm]]; else [websiteAlarm setState:NO];
        if ([appunt canSetAlarm]==YES) [websiteAlarm setEnabled:YES]; else [websiteAlarm setEnabled:NO];
    }

    if (currentTab==5) { //buy
        [buyWhat setStringValue:[appunt what]];
        [buyWhere setStringValue:[appunt place]];
        [buyDate setObjectValue:[appunt date]];
        [buyTime setStringValue:[appunt sTime]];
        [buyNotes setString:[appunt note]];
        if ([appunt canSetAlarm]==YES) [buyAlarm setState:[appunt alarm]]; else [buyAlarm setState:NO];
        if ([appunt canSetAlarm]==YES) [buyAlarm setEnabled:YES]; else [buyAlarm setEnabled:NO];
    }

    if (currentTab==6) { //to do
        [todoTodo setStringValue:[appunt todo]];
        [todoDate setObjectValue:[appunt date]];
        [todoTime setStringValue:[appunt sTime]];
        [todoNotes setString:[appunt note]];
        if ([appunt canSetAlarm]==YES) [todoAlarm setState:[appunt alarm]]; else [todoAlarm setState:NO];
        if ([appunt canSetAlarm]==YES) [todoAlarm setEnabled:YES]; else [todoAlarm setEnabled:NO];
    }

    [self updateUpperButtons];
    [self modificaTitolo];
    [dataWindow enableFlushWindow];
    [dataWindow flushWindowIfNeeded];
    [self excludeModifiedDOWN];
    }
}

-(void)modificaTitolo {
    if (currentTab==0) [dataWindow setTitle:[[NSString alloc] initWithFormat:@"Phone: %@",[appunt descrizioneTitolo]]];
    if (currentTab==1) [dataWindow setTitle:[[NSString alloc] initWithFormat:@"Meet: %@",[appunt descrizioneTitolo]]];
    if (currentTab==2) [dataWindow setTitle:[[NSString alloc] initWithFormat:@"Email: %@",[appunt descrizioneTitolo]]];
    if (currentTab==3) [dataWindow setTitle:[[NSString alloc] initWithFormat:@"Go To: %@",[appunt descrizioneTitolo]]];
    if (currentTab==4) [dataWindow setTitle:[[NSString alloc] initWithFormat:@"Website: %@",[appunt descrizioneTitolo]]];
    if (currentTab==5) [dataWindow setTitle:[[NSString alloc] initWithFormat:@"Buy: %@",[appunt descrizioneTitolo]]];
    if (currentTab==6) [dataWindow setTitle:[[NSString alloc] initWithFormat:@"To Do: %@",[appunt descrizioneTitolo]]];
}

-(void)updateUpperButtons {
    [dataWindow disableFlushWindow];
    if (currentTab==0) [phoneButton setState:NSOnState]; else [phoneButton setState:NSOffState];
    if (currentTab==1) [meetButton setState:NSOnState]; else [meetButton setState:NSOffState];
    if (currentTab==2) [emailButton setState:NSOnState]; else [emailButton setState:NSOffState];
    if (currentTab==3) [gotoButton setState:NSOnState]; else [gotoButton setState:NSOffState];
    if (currentTab==4) [websiteButton setState:NSOnState]; else [websiteButton setState:NSOffState];
    if (currentTab==5) [buyButton setState:NSOnState]; else [buyButton setState:NSOffState];
    if (currentTab==6) [todoButton setState:NSOnState]; else [todoButton setState:NSOffState];
    [dataWindow enableFlushWindow];
    [dataWindow flushWindowIfNeeded];
}

- (IBAction)buyModified:(id)sender
{
    if (excludeModified==0) {
        [self excludeModifiedUP];
        if (sender==buyTime) {
            [appunt setSTime:[buyTime stringValue]];
            [buyTime setStringValue:[appunt sTime]];
        }
        [self saveContext];
        [self modificaTitolo];
        if ([appunt canSetAlarm]==YES) [buyAlarm setEnabled:YES]; else [buyAlarm setEnabled:NO];
        [self excludeModifiedDOWN];
    }
}

- (IBAction)chooseBuy:(id)sender
{
    //[dataWindow disableFlushWindow];
    //excludeModified=YES;
    [self saveContext];
    [appunt setKind:5];
    [self updateUI];
    //excludeModified=NO;
    //    [dataWindow disableFlushWindow];
    //   [dataWindow flushWindowIfNeeded];
}

- (IBAction)chooseEmail:(id)sender
{
    //excludeModified=YES;
    [self saveContext];
    [appunt setKind:2];
    [self updateUI];
    //excludeModified=NO;
}

- (IBAction)chooseGoto:(id)sender
{
    //excludeModified=YES;
    [self saveContext];
    [appunt setKind:3];
    [self updateUI];
    //excludeModified=NO;
}

- (IBAction)chooseMeet:(id)sender
{
    //excludeModified=YES;
    [self saveContext];
    [appunt setKind:1];
    [self updateUI];
    //excludeModified=NO;
}

- (IBAction)choosePhone:(id)sender
{
    //excludeModified=YES;
    [self saveContext];
    [appunt setKind:0];
    [self updateUI];
    //excludeModified=NO;
}

- (IBAction)chooseTodo:(id)sender
{
    //excludeModified=YES;
    [self saveContext];
    [appunt setKind:6];
    [self updateUI];
    //excludeModified=NO;
}

- (IBAction)chooseWebsite:(id)sender
{
    //excludeModified=YES;
    [self saveContext];
    [appunt setKind:4];
    [self updateUI];
    //excludeModified=NO;
}

- (IBAction)emailModified:(id)sender {
    if (excludeModified==0) {
        [self excludeModifiedUP];
        if (sender==emailTime) {
            [appunt setSTime:[emailTime stringValue]];
            [emailTime setStringValue:[appunt sTime]];
        }
        [self saveContext];
        [self modificaTitolo];
        if ([appunt canSetAlarm]==YES) [emailAlarm setEnabled:YES]; else [emailAlarm setEnabled:NO];
        [self excludeModifiedDOWN];
    }
}

- (IBAction)gotoModified:(id)sender {
    if (excludeModified==0) {
        [self excludeModifiedUP];
        if (sender==gotoTime) {
            [appunt setSTime:[gotoTime stringValue]];
            [gotoTime setStringValue:[appunt sTime]];
        }
        [self saveContext];
        [self modificaTitolo];

        if ([appunt canSetAlarm]==YES) [gotoAlarm setEnabled:YES]; else [gotoAlarm setEnabled:NO];
        [self excludeModifiedDOWN];
    }
}

- (IBAction)meetModified:(id)sender {
    if (excludeModified==0) {
        [self excludeModifiedUP];
        if (sender==meetTime) {
            [appunt setSTime:[meetTime stringValue]];
            [meetTime setStringValue:[appunt sTime]];
        }
        [self saveContext];
        [self modificaTitolo];
        if ([appunt canSetAlarm]==YES) [meetAlarm setEnabled:YES]; else [meetAlarm setEnabled:NO];
        [self excludeModifiedDOWN];

    }
}

- (IBAction)phoneModified:(id)sender {
    if (excludeModified==0) {
        [self excludeModifiedUP];
        if (sender==phoneTime) {
            [appunt setSTime:[phoneTime stringValue]];
            [phoneTime setStringValue:[appunt sTime]];
        }
        [self saveContext];
        [self modificaTitolo];
        if ([appunt canSetAlarm]==YES) [phoneAlarm setEnabled:YES]; else [phoneAlarm setEnabled:NO];
        [self excludeModifiedDOWN];
    }
}

- (IBAction)setAlarm:(id)sender {
    //mort
}

- (IBAction)todoModified:(id)sender {
    if (excludeModified==0) {
        [self excludeModifiedUP];
        if (sender==todoTime) {
            [appunt setSTime:[todoTime stringValue]];
            [todoTime setStringValue:[appunt sTime]];
        }
        //   [dataWindow selectNextKeyView:self];
        [self saveContext];
        [self modificaTitolo];
        
        if ([appunt canSetAlarm]==YES) [todoAlarm setEnabled:YES]; else [todoAlarm setEnabled:NO];
        [self excludeModifiedDOWN];
    }
}

- (IBAction)websiteModified:(id)sender
{
    if (excludeModified==0) {
        [self excludeModifiedUP];
        if (sender==websiteTime) {
            [appunt setSTime:[websiteTime stringValue]];
            [websiteTime setStringValue:[appunt sTime]];
        }
        //   [dataWindow selectNextKeyView:self];
        [self saveContext];
        [self modificaTitolo];
        if ([appunt canSetAlarm]==YES) [websiteAlarm setEnabled:YES]; else [websiteAlarm setEnabled:NO];
        [self excludeModifiedDOWN];
    }
}

-(void)excludeModifiedUP {
    excludeModified++;
}

-(void)excludeModifiedDOWN {
    if (excludeModified>0) excludeModified--;
}

//finestra di ricerca
- (void) loadChildWindow {
    if (!findWindow) {
        findWindow=[[findWindowController alloc] init];
        windowVisible=NO;
    }
}

-(void) showChildWindow
{
    NSPoint	dove;


    if (windowVisible==NO) {

        NSRect winFra=[dataWindow frame];
        dove.x=156;
        dove.y=20;
        //  if (currentTab==0) dove=[phonePerson convertPoint:dove toView:[ARRdataWindow contentView]];
        //  if (currentTab==1) dove=[emailPerson convertPoint:dove toView:[ARRdataWindow contentView]];
        //  if (currentTab==2) dove=[meetPerson convertPoint:dove toView:[ARRdataWindow contentView]];

        if (currentTab==0) dove.y=winFra.size.height-160;
        if (currentTab==1) dove.y=winFra.size.height-160;
        if (currentTab==2) dove.y=winFra.size.height-160;

       // NSLog(@"%X",dove.y);

        dove=[dataWindow convertBaseToScreen:dove];
        [[findWindow window] setFrameTopLeftPoint:dove];
        [dataWindow addChildWindow: [findWindow window] ordered:NSWindowAbove];
        [[findWindow window] orderFront:self];
        windowVisible=YES;
        [findWindow sveglia];
        [dataWindow setVisibleChild:YES];
        [[findWindow tabella] selectRow:0 byExtendingSelection:NO];
        [[findWindow tabella] scrollRowToVisible:0];
    }
}

-(void) hideChildWindow
{
    if (windowVisible==YES) {
        [dataWindow removeChildWindow:[findWindow window]];
        [[findWindow window] orderOut:self];
        windowVisible=NO;
        [dataWindow setVisibleChild:NO];
    }
    //devo fare un remove window!!!
}

- (void)controlTextDidChange:(NSNotification *)aNotification
{

    NSText *fieldEditor = [[aNotification userInfo] objectForKey:@"NSFieldEditor"];/* the field editor */
        // NSControl *postingObject = [aNotification object];/* The object that posted the notification. */

        searchResults=[self performSearch:[fieldEditor string]];
      //  NSLog(@"%x",[searchResults count]);
        if ([searchResults count]>0) {
            [findWindow setTableData:searchResults];
            [self showChildWindow];
            //update risultati
        } else {
            [self hideChildWindow];
        }
}

- (void)textDidEndEditing:(NSNotification *)notification;
{
    [self hideChildWindow];
}

- (BOOL)control:(NSControl *)control textShouldEndEditing:(NSText *)fieldEditor
{
   // NSLog(@"textShouldEndEditing");
    [self hideChildWindow];

    return YES;
}

-(NSArray*)performSearch:(NSString*)who
{
    int	i=0,j=0;
    ABSearchElement *nome,*cognome,*organizzazione,*totale,*totalone;
    ABRecord *currentPerson;
    ABMutableMultiValue *multiValue;
    displayPersona *chi;
    NSMutableArray *risultato=nil;

    NSArray *allTheWords=[who componentsSeparatedByString:@" "];
    NSMutableArray *itemSearch;
    NSArray *peopleFound;
    NSString *loc;

    itemSearch=[[NSMutableArray alloc] init];
    [itemSearch autorelease];

    for(i=0;i<[allTheWords count];i++) {
        //  NSLog(@"-%@",[allTheWords objectAtIndex:i]);
        //loc=[[allTheWords objectAtIndex:i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        loc=[allTheWords objectAtIndex:i];
        if ([loc length]!=0) {
            // NSLog(@"%@",loc);
            nome =[ABPerson searchElementForProperty:kABFirstNameProperty label:nil key:nil value:loc comparison:kABContainsSubStringCaseInsensitive];
            cognome =[ABPerson searchElementForProperty:kABLastNameProperty label:nil key:nil value:loc comparison:kABContainsSubStringCaseInsensitive];
            organizzazione =[ABPerson searchElementForProperty:kABOrganizationProperty label:nil key:loc value:who comparison:kABContainsSubStringCaseInsensitive];
            totale =[ABSearchElement searchElementForConjunction:kABSearchOr children:[NSArray arrayWithObjects:nome, cognome, organizzazione, nil]];
            [itemSearch addObject:totale];
        }
    }
    i=[itemSearch count];

    if (i>0){
        if (i==1) {
            totalone=totale;
        } else {
            totalone =[ABSearchElement searchElementForConjunction:kABSearchAnd children:[NSArray arrayWithArray:itemSearch]];
            //  NSLog(@"--%x",i);
        }
        ABAddressBook *AB = [ABAddressBook sharedAddressBook];
        peopleFound = [AB recordsMatchingSearchElement:totalone];
    } else { peopleFound=nil;}

    //ok, adesso parserizziamo i risultati.

    if (peopleFound!=nil) {
        risultato=[[NSMutableArray alloc] init];
        [risultato autorelease];
        for (i=0;i<[peopleFound count];i++) {
            currentPerson=[peopleFound objectAtIndex:i];
            if (currentTab==2) {//email
                multiValue = [currentPerson valueForProperty: kABEmailProperty];
            } else {//altri casi
                multiValue = [currentPerson valueForProperty: kABPhoneProperty];
            }
            //nota: trova se la label e' di casa, lavoro ecc... e lo stampa
            for (j=0;j<[multiValue count];j++) {
                chi=[[displayPersona alloc]init];
                [chi autorelease];
                //estrae i dati
                NSString *ilNome=[currentPerson valueForProperty:kABFirstNameProperty];
                NSString *ilCognome=[currentPerson valueForProperty:kABLastNameProperty];
                NSString *lAzienda=[currentPerson valueForProperty:kABOrganizationProperty];

                [chi setNome:ilNome];
                [chi setCognome:ilCognome];
                [chi setAzienda:lAzienda];

                // if (ilNome==nil) ilNome=@"";
                // if (ilCognome==nil) ilCognome=@"";

                // [chi setName:[[NSString alloc] initWithFormat:@"%@ %@",ilNome,ilCognome ]];
                //[chi setKind:[multiValue labelAtIndex:j]];

                CFStringRef striRe=ABMultiValueCopyLabelAtIndex((ABMultiValueRef)multiValue, j);
                striRe=ABCopyLocalizedPropertyOrLabel(striRe);
                [chi setKind:(NSString*)striRe];

                //Aggiungere ABCopyLocalizedPropertyOrLabel!!!!!
                if (currentTab==2) {
                    [chi setEmail:[multiValue valueAtIndex:j]];
                } else {
                    [chi setPhone:[multiValue valueAtIndex:j]];
                }
                [risultato addObject:chi];
            }
            //nota2:potrebbe essere richiesto l'ordinamento?
        }
    } else {
        risultato=nil;
    }
    return risultato;
}

-(void)selectPrevious {
    int num=[[findWindow tabella] selectedRow];
    if (num>0) [[findWindow tabella] selectRow:--num byExtendingSelection:NO];
    [[findWindow tabella] scrollRowToVisible:num];

}

-(void)selectNext {
    int num=[[findWindow tabella] selectedRow];
    int maxNum=[[findWindow tabella] numberOfRows]-1;
    if (num<maxNum) [[findWindow tabella] selectRow:++num byExtendingSelection:NO];
    [[findWindow tabella] scrollRowToVisible:num];
}

-(void)selectEnter {
    //prende quello selezionato e compila i campi con i suoi dati
    int num=[[findWindow tabella] selectedRow];
    displayPersona *chi=[searchResults objectAtIndex:num];
    if (currentTab==0) {
        [phonePerson setStringValue:[chi name]];
        [phoneNumber setStringValue:[chi phone]];
        [dataWindow makeFirstResponder:phoneDate];
    }
    if (currentTab==1) {
        [meetPerson setStringValue:[chi name]];
        [meetPhone setStringValue:[chi phone]];
        [dataWindow makeFirstResponder:meetWhere];
    }
    if (currentTab==2) {
        [emailPerson setStringValue:[chi name]];
        [emailEmail setStringValue:[chi email]];
        [dataWindow makeFirstResponder:emailDate];
    }
    // Fill-in information about the trip's destination.                        //leg20180309 - 1.2.0
    if (currentTab==3) {
        [gotoWhere setStringValue:[chi name]];
        [gotoNotes insertText:[chi descrizione]];
        [dataWindow makeFirstResponder:gotoDate];
    }

}
@end
