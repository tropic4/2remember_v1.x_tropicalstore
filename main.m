//
//  main.m
// 2Remember
//
//
//  Created by Andrea Rincon Ray on Sun Feb 02 2003.
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "globals.h"


NSString 	*ARRdocked=@"Window Docked";
NSString 	*ARRfrontmost=@"Frontmost";
NSString 	*ARRleft=@"Keep the left";
NSString 	*ARRbottom=@"DockBottom";
NSString 	*ARRwinX=@"WinX";
NSString 	*ARRwinY=@"WinY";
NSWindow 	*ARRmainWindow=NULL;
NSWindow 	*ARRdataWindow=nil;

DataWindowController	*ARRdataWindowController=nil;
SmartScrollView	 	*ARRsmartScrollView=nil;
ButtonField		*ARRbuttonField=nil;

NSString 	*ARRdateTimeOrdering;
NSString 	*ARRtimeFormat;
BOOL 		ARRaboutToTerminate=NO;

int 		ARRbrowseMode=1;
int 		ARRbackgroundMode=2;
int 		ARRnormalMode=0;
int 		ARRshow=1;
int 		ARRhide=0;
int 		ARRlock=2;
int 		ARRinitialize=4;
int 		ARRunlock=5;
BOOL 		ARRdockedRT;
BOOL 		ARRleftRT;
BOOL 		ARRbottomRT;
BOOL 		ARRfrontmostRT;

MainWindowController 	*ARRmainWindowController=nil;
InterfaceDriver		*ARRinterfaceDriver=nil;
agenda			*ARRagenda=nil;
AppController		*ARRappController=nil;
//tabView			*ARRtabView=nil;
// Other custom preferences ..
//Altre preferenze custom...
BOOL 		ARRdoubleClick=YES;
BOOL 		ARRalarmTab=YES;
BOOL 		ARRalarmSound=YES;
BOOL 		ARRalarmIcon=YES;
BOOL 		ARRalarmAlert=YES;

int 		ARRdataSorting=0;
BOOL 		ARRautoStartup=NO;
BOOL 		ARRanimation=YES;
BOOL		ARRautoOpen=YES;

DataWindowInterfaceDriver *ARRdataWindowInterfaceDriver=nil;

int main(int argc, const char *argv[])
{
    return NSApplicationMain(argc, argv);
}
