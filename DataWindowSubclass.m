#import "DataWindowSubclass.h"
#import "DataWindowController.h"
#import "DataWindowInterfaceDriver.h"

@implementation DataWindowSubclass
-(BOOL)windowShouldClose:(id)sender
{
    return [ARRdataWindowController windowShouldClose:sender];
    
}

- (void)sendEvent:(NSEvent *)theEvent
{
    NSEventType	cheTipo;
    unichar theChar;
    
    if (visibleChild==YES) {
        cheTipo=[theEvent type];
        if (cheTipo==NSLeftMouseDown) {
            [ARRdataWindowInterfaceDriver hideChildWindow];
            [super sendEvent:theEvent];
        }
        if (cheTipo==NSKeyDown) {
            theChar=[[theEvent characters] characterAtIndex:0];
            if (theChar==NSUpArrowFunctionKey) {[ARRdataWindowInterfaceDriver selectPrevious];}
            else if (theChar==NSDownArrowFunctionKey) {[ARRdataWindowInterfaceDriver selectNext];}
            else if (theChar==13||theChar==3) {
                [ARRdataWindowInterfaceDriver selectEnter];

            }
            else {[super sendEvent:theEvent];}
        }

    } else {
        [super sendEvent:theEvent];
    }
}

-(void)setVisibleChild:(BOOL)val
{
    visibleChild=val;
}
@end
