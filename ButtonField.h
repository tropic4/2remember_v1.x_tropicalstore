/* ButtonField */

#import <Cocoa/Cocoa.h>
#import "globals.h"
@class appuntamento;




@interface ButtonField : NSView
{
    unsigned int	progressiveNumber; //OK
    NSMutableArray	*trackingRects;
    appuntamento	*appuntamentoHilight;
    appuntamento	*appuntamentoMouseOver;
    BOOL		lockSelection;
    BOOL		requireAnimation;
    int			numeroPagina;
    NSImage		*liveResizeBuffer,*dynamicTop,*dynamicBottom;
    BOOL		bufferIsEmpty;
    BOOL		modificaTotaleInterfaccia;
    BOOL		shouldSlide;
    BOOL		shouldSlideAdd;
    BOOL		lockRects;
    NSMutableArray	*safeguard; //serve per il retain degli eventi fino a qando non sono distrutti
}
-(void)dataChanged;
-(void)dynamicAdd:(appuntamento*)appuntamento;
-(void)endDynamicAdd;
-(void)onlyRecalculateButtons;
-(void)modificaTotaleInterfaccia;
-(void)redrawOffline;
-(void)selectedRecordChanged;
-(void)recalculateButtons;
-(void)setRequireAnimation:(BOOL)yesorno;
-(void)displayAll;

-(int)numeroPagina;
-(void)setNumeroPagina:(int)pag;
-(void)setScrolling;
-(void)numberOfRecordsChanged;
-(int)maxPages;
-(NSImage*)offlineImage;
-(void)setDynamicBuffer:(NSPoint)dove;
-(void)resetDynamicBuffer;
-(appuntamento*)matchButton:(appuntamento*)appunt withPoint:(NSPoint)punto;

-(void)setLockRects:(BOOL)val;
-(void)trackingAggiunto:(appuntamento*)appunt withPoint:(NSPoint)punto;
-(void)resetStatus;
-(void)doubleClickOnSelected;
@end
