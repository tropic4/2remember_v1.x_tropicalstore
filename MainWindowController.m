//
//  MainWindowController.m
// 2Remember  (was originally called iRemember)
//
//  Created by Andrea Rincon Ray on Sun Feb 09 2003.
//  Version 1.0-1.0.5 by Andrea Rincon Ray on or beforer Sun Feb 09 2003.
//  Versions after 1.0.6 by James Lee and Lewis Garrett, Tropical Software, Inc. 

//  2Remember
//  Copyright (c) 2011 Tropical Software, Inc. All rights reserved.
//  Version 1.0-1.0.5 by Andrea Rincon Ray on or beforer Sun Feb 09 2003.
//  Versions after 1.0.6 by James Lee and Lewis Garrett, Tropical Software, Inc. 

//  2Remember
//  Copyright (c) 2011 Tropical Software, Inc. All rights reserved.
//  April 20, 2011-- iGoogle translation added by James Lee
//

#import "MainWindowController.h"
#import "globals.h"
#import "InterfaceDriver.h"
#import "AppController.h"

@implementation MainWindowController

//...
//...............................................NOTICE.................................
//...All Italian translated to English by iGoogle- might not be perfectly translated ....
//...

// Initialize (the usual) ...
//Inizializza (� il solito)...
-(id)init{
    if(self=[super initWithWindowNibName:@"MainWindow"]) {
        setNeedUpdate=NO;
    }
    return self;
}

// Retrieving the window was ...
//la finestra � stata disarchiviata...
-(void)awakeFromNib
{
    //NSNotificationCenter *nc;

    //nc=[NSNotificationCenter defaultCenter];
    //[nc addObserver:self selector:@selector(gestoreMouseInside:) name:@"ARRmouseInside" object:nil];
    //[nc addObserver:self selector:@selector(gestoreMouseOutside:) name:@"ARRmouseOutside" object:nil];
    //[nc addObserver:self selector:@selector(gestoreMouseLock:) name:@"ARRmouseLock" object:nil];
}

// The window has loaded ...
//La finestra � stata caricata...
-(void)windowDidLoad {

// For now, nothing special.
//Per ora niente di particolare.
}


// calculation of the initial position of the window (collassata!)
//calcolo della posizione iniziale della finestra (collassata!)
-(void)setWindowInit //OK
{
    NSRect 	posizioneFinestra;
    float	winX;
    float	winY;
    NSRect	visibleScreen;

    dockedStat=ARRlock;
    visibleScreen=[[[self window] screen] visibleFrame];
    winX=[[NSUserDefaults standardUserDefaults] floatForKey:ARRwinX];
    winY=[[NSUserDefaults standardUserDefaults] floatForKey:ARRwinY];

    posizioneFinestra.size.width=260;
    posizioneFinestra.size.height=70;
    winY=winY-posizioneFinestra.size.height;
    
    if (winX+30<visibleScreen.origin.x) {
        winX=visibleScreen.origin.x-30;
    } else if (winX+posizioneFinestra.size.width-30>visibleScreen.origin.x+visibleScreen.size.width) {
        winX=visibleScreen.origin.x+visibleScreen.size.width-posizioneFinestra.size.width+30;
    }

    if (winY<visibleScreen.origin.y) {
        winY=visibleScreen.origin.y;
    } else if (winY+posizioneFinestra.size.height>visibleScreen.origin.y+visibleScreen.size.height) {
        winY=visibleScreen.origin.y+visibleScreen.size.height-posizioneFinestra.size.height;
    }

    posizioneFinestra.origin.x=winX;
    posizioneFinestra.origin.y=winY;

    if ([self outsideOfScreen:posizioneFinestra.origin]==YES) {
        [[self window] setFrame:posizioneFinestra display:YES animate:NO];
        [[self window] center];
    }else{
		// Now we set the initial state of the window:
        //adesso settiamo lo stato iniziale della finestra:
        [[self window] setFrame:posizioneFinestra display:YES animate:NO];
    }
	// and stores it as the default
    //e lo memorizziamo come quello default
    openRect=posizioneFinestra;
    [[self window] makeKeyAndOrderFront:self];//Adesso ci mostriamo!
}

// We will now save the window position for the next startup ..
//Vogliamo adesso salvare la posizione della finestra per il prossimo avvio...
-(void)saveWindow //OK
{
    NSRect posizioneFinestra;

    posizioneFinestra=[[self window] frame];
    [[NSUserDefaults standardUserDefaults] setFloat:posizioneFinestra.origin.x forKey:ARRwinX];
    [[NSUserDefaults standardUserDefaults] setFloat:posizioneFinestra.origin.y+posizioneFinestra.size.height forKey:ARRwinY];
}

//MouseInside....
-(void)gestoreMouseInside:(NSNotification *)note //OK
{
    if ([ARRinterfaceDriver isMouseOutside]==NO) {
       // 
		// NSLog(@"mainWindow:gestoreMouseInside");
        [self showFromDock:ARRshow];
    }
}

//Mouse Outside...
-(void)gestoreMouseOutside:(NSNotification *)note //OK
{
  //  NSLog(@"MainWindowController: gestoreMouseOutside");
    // This has to calculate whether the mouse is inside or outside!!!
	// Questo deve calcolare se il mouse � dentro o fuori!!!
    if([ARRinterfaceDriver isMouseOutside]==YES){
        [self showFromDock:ARRhide];
    }
}
 
//MouseLock!
-(void)gestoreMouseLock:(NSNotification *)note //OK
{
	// ...: Manage Mouse Lock ?
	//  NSLog(@"MainWindowController: Gestore mouse lock");
    [self showFromDock:ARRlock];
}

// This fans demand for opening / closing and then decide if you should make them.
// This will disappear from here and go to app controller!
//Questo si smazza le richieste di apertura/chiusura e poi decide se � il caso di farle.
//Questo sparisce da qua e va in app controller!
-(void)showFromDock:(int)selection
{
	//NSLog(@"Show From Dock... Evaluating");
    switch (selection)
    {
        case 4://initialize
            dockedStat=ARRshow;
            if (ARRdockedRT==YES)
            {
             //   NSLog(@"showFromDock initialize");
                dockedStat=ARRshow;
                // 
				//[self showFromDock:ARRhide]; //non serve che si nasconda?... inizia visibile.
            }
                // Now the state has been initialized and if necessary, was put in the dock.
			//Adesso � stato inizializzato lo stato e se necessario � stato messo in dock.
                break;
        case 0://hide
            if ((ARRdockedRT==YES)&&(dockedStat==ARRshow||dockedStat==ARRunlock))
            {
               // NSLog(@"showfromdock hide");
                dockedStat=ARRhide;
                [self dock];
            }
            break;
        case 1://show
            if ((ARRdockedRT==YES)&&dockedStat==ARRhide)
            {
               // NSLog(@"ShowFromDock show");
                dockedStat=ARRshow;
                [self undock];
            }
            break;
        case 2://lock
            if (ARRdockedRT==YES/*&&dockedStat!=ARRlock*/)
            {
				// NSLog(@"ShowFromDock lock");
                dockedStat=ARRlock;
                [self undock];
            }
            break;
        case 5://unlock
            if (ARRdockedRT==YES)
            {
               // NSLog(@"ShowFromDock unlock");
                dockedStat=ARRunlock;
                [self showFromDock:ARRhide];
            }
            break;
        default:
            NSLog(@"mainWindow:showFromDock:unrecognized option");
    }
}


-(void)mouseEntered:(NSEvent *)e {
    [ARRappController mainInterfaceDispatch:0];
}

-(void)mouseExited:(NSEvent *)e {
    [ARRappController mainInterfaceDispatch:1];
}


-(void)dock
{
    NSRect posizioneInizialeFinestra;
    NSRect coordinateSchermo;
   // NSPoint mouseLoc;

    posizioneInizialeFinestra=[[self window] frame];
    coordinateSchermo=[[[self window] screen] visibleFrame];
	// NSLog(@"%x",posizioneInizialeFinestra.size.height);
	// determines the operation
    //determina l'operazione
    if (ARRdockedRT==1){
        if (ARRleftRT==YES && ARRbottomRT==NO) {
            posizioneInizialeFinestra.origin.x=coordinateSchermo.origin.x+30-posizioneInizialeFinestra.size.width;
        } else if (ARRleftRT==NO && ARRbottomRT==NO){
            posizioneInizialeFinestra.origin.x=coordinateSchermo.origin.x+coordinateSchermo.size.width-30;
        } else if (ARRleftRT==NO && ARRbottomRT==YES){
            posizioneInizialeFinestra.origin.y=coordinateSchermo.origin.y-posizioneInizialeFinestra.size.height+22;
        } //Whoooah, Calcolate le nuove coordinate!
          //computa le nuove coordinate in base allo spostamento del mouse
          //setta le nuove coordinate nella finestra
    //NSLog(@"d:%x",posizioneInizialeFinestra.size.height);
        //da il comando di spostamento
        [[self window] setFrame:posizioneInizialeFinestra display:YES animate:ARRanimation];

		// initialWindowPosition=...
		//  posizioneInizialeFinestra=[[self window] frame];
  //  NSLog(@"d:%x",posizioneInizialeFinestra.size.height);
            
       // **** Check the mouse position to know if you have to close now ...
		//****controllare posizione del mouse per sapere se bisogna chiudere subito...
        //mouseLoc=[NSEvent mouseLocation];
      //  if (mouseLoc.x>=posizioneInizialeFinestra.origin.x+30 && mouseLoc.x<=posizioneInizialeFinestra.origin.x+posizioneInizialeFinestra.size.width-30 && mouseLoc.y>=posizioneInizialeFinestra.origin.y && mouseLoc.y<=posizioneInizialeFinestra.origin.y+posizioneInizialeFinestra.size.height) {
      //      NSLog(@"MouseInside2");
        //    [self showFromDock:ARRshow];
        //}

    }
}

-(void)undock
{
   // First check if it is hidden or not!
	//Prima controllare se � nascosta o no!!!

    NSRect posizioneInizialeFinestra;
    NSRect coordinateSchermo;
  // NSPoint mouseLoc;

    posizioneInizialeFinestra=[[self window] frame];
    coordinateSchermo=[[[self window] screen] visibleFrame];
   // determines the operation
	//determina l'operazione
   // NSLog(@"%x",posizioneInizialeFinestra.size.height);
    if (ARRdockedRT==1){
        if (ARRleftRT==YES && ARRbottomRT==NO) {
            posizioneInizialeFinestra.origin.x=coordinateSchermo.origin.x-30;
        } else if (ARRleftRT==NO && ARRbottomRT==NO){
            posizioneInizialeFinestra.origin.x=coordinateSchermo.origin.x+coordinateSchermo.size.width-posizioneInizialeFinestra.size.width+30;
        } else if (ARRleftRT==NO && ARRbottomRT==YES){
            posizioneInizialeFinestra.origin.y=coordinateSchermo.origin.y;
        } // Whoooah, calculate the new coordinates!
		//Whoooah, Calcolate le nuove coordinate!

        // by the motion command
		//da il comando di spostamento
        [[self window] setFrame:posizioneInizialeFinestra display:YES animate:ARRanimation];

		// initialWindowPosition
		//  posizioneInizialeFinestra=[[self window] frame];
      //  NSLog(@"d:%x",posizioneInizialeFinestra.size.height);
		
		// check the mouse position to know if you have to open soon!
        //controllare posizione del mouse per sapere se bisogna aprire subito!!!
       // mouseLoc=[NSEvent mouseLocation];
       // if (!(mouseLoc.x>=posizioneInizialeFinestra.origin.x+30 && mouseLoc.x<=posizioneInizialeFinestra.origin.x+posizioneInizialeFinestra.size.width-30 && mouseLoc.y>=posizioneInizialeFinestra.origin.y && mouseLoc.y<=posizioneInizialeFinestra.origin.y+posizioneInizialeFinestra.size.height)) {
      //      NSLog(@"MouseOutside2");
        //    [self showFromDock:ARRhide];
       // }
    }
}

// Require Update update the window size if it is visible!
// If it is not visible does nothing!
//Require Update aggiorna le dimensioni della finestra se � visibile!
//Se non � visibile non fa niente!
-(void)requireUpdate
{
    NSRect	oldFrame;
  //  NSLog(@"MainWindowController:requireUpdate");
    if (dockedStat!=ARRhide)
    {
        //realtimeUpdate
        [[self window] setFrame:openRect display:YES animate:ARRanimation];
       // [[self window] display];
        //[[self window] setNeedsDisplay:YES];
    } else {
        // immediately sets the height.
		//setta immediatamente l'altezza.
        oldFrame=[[self window] frame];
        oldFrame.origin.y=oldFrame.origin.y+oldFrame.size.height;
        oldFrame.size.height=openRect.size.height;
        oldFrame.origin.y=oldFrame.origin.y-oldFrame.size.height;
        [[self window] setFrame:oldFrame display:YES animate:NO];
    }
}

// But the same is not animated
//Stesso ma non � animato
-(void)requireUpdateRealTime
{
    NSRect	oldFrame;

  //  NSLog(@"MainWindowController:requireUpdate");
    if (dockedStat!=ARRhide)
    {
        //realtimeUpdate
        [[self window] setFrame:openRect display:YES animate:NO];
    } else {
		// immediately sets the height.
        //setta immediatamente l'altezza.
        oldFrame=[[self window] frame];
        oldFrame.origin.y=oldFrame.origin.y+oldFrame.size.height;
        oldFrame.size.height=openRect.size.height;
        oldFrame.origin.y=oldFrame.origin.y-oldFrame.size.height;
        [[self window] setFrame:oldFrame display:YES animate:NO];
    }
        
}

-(BOOL)outsideOfScreen:(NSPoint)position //Ok
{
//    int    screenNumber;
    NSInteger    screenNumber;                                                  //leg20180405 - 1.2.0
    BOOL	result=YES;
    int		i;
    NSArray	*configurazioneSchermi;
    NSRect	frameSchermo;
    
    configurazioneSchermi=[NSScreen screens];
    screenNumber=[configurazioneSchermi count];


    for (i=0; i<screenNumber; i++)
    {
        // is inside the screen.
		//� dentro allo schermo.
        frameSchermo=[[configurazioneSchermi objectAtIndex:i] visibleFrame];
        if (position.x>=(frameSchermo.origin.x-30)&&position.x<=frameSchermo.origin.x+frameSchermo.size.width&&position.y>=frameSchermo.origin.y&&position.y<=frameSchermo.origin.y+frameSchermo.size.height)
        {
            result=NO;
        }
    }
    return result;
}

-(void)dealloc
{
    NSNotificationCenter *nc;
    //NSLog(@"MainWindowController:dealloc");
    [self saveWindow];
    nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:self];
    [super dealloc];
}

// Accessories methods:
//Metodi Accessori:

// sets the size to open!
//setta le dimensioni da aperto!
-(void)setOpenRect:(NSRect)e {
    openRect=e;
}

// states of open size!
//dichiara de dimensioni da aperto!
-(NSRect)getOpenRect
{
    return openRect;
}
@end
