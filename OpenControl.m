#import "OpenControl.h"
#import "globals.h"

//Gestisce esclusivamente l'apertura della finestra e il redraw delle maniglie.
//It only manages the opening of the window and the redrawing of the handles.   //leg20230914 - Translation

@implementation OpenControl

-(void)awakeFromNib {
    oldRect.size.width=0;
    oldRect.size.height=0;
    oldRect.origin.x=0;
    oldRect.origin.y=0;
}

//Questa Funzione Gestisce il redraw della NSView (quindi tutta l'interfaccia).
// English Translation:                                                         //leg20180326 - 1.2.0
// This function manages the redraw the NSView (so any interface).
-(void)drawRect:(NSRect)rect {
    [self updateTrackingRect];
}

////Questa funzione gestisce la tracking rect della NSView.
////Distrugge e ricrea la trackingrect quando necessario.
//// English Translation:                                                         //leg20180326 - 1.2.0
//// This function manages the tracking rect of NSView.  It destroys and
////  recreates the trackingrect when needed.
//-(void)updateTrackingRect {
//    NSRect	rect;
//
//    rect=[self frame];
//
//    if (oldRect.size.height!=rect.size.height||oldRect.size.width!=rect.size.width) {
//        oldRect.size.height=rect.size.height;
//        oldRect.size.width=rect.size.width;
//
//        rect.origin.x=30;
//        rect.size.width=rect.size.width-60;
//
////        if (!trackingRectTag==NULL) {
//        if (!trackingRectTag) {                                                 //leg20180319 - 1.2.0
//            [self removeTrackingRect:trackingRectTag];
//        }
//        trackingRectTag=[self addTrackingRect:rect owner:self userData:NULL assumeInside:NO];
//    }
//}

//
// Merge in TopXNotes/QuickNotes "…Invalid NSTrackingRect" crash fix by         //leg20240415 - 1.4.0
//  replacing function.
//
//Questa funzione gestisce la tracking rect della NSView.
//Distrugge e ricrea la trackingrect quando necessario.
// English Translation:                                                         //leg20180326 - 1.2.0
// This function manages the tracking rect of NSView.  It destroys and
//  recreates the trackingrect when needed.
-(void)updateTrackingRect {
    NSRect    rect;

    rect=[self frame];

    if (oldRect.size.height!=rect.size.height||oldRect.size.width!=rect.size.width) {
        oldRect.size.height=rect.size.height;
        oldRect.size.width=rect.size.width;

        rect.origin.x=30;
        rect.size.width=rect.size.width-60;

//        if (!trackingRectTag==NULL) {
//        if (!trackingRectTag) {                                                 //leg20180319 - 1.2.0
        // Fix crash on Sonoma: An uncaught exception was raised - 0x0 is an    //leg20231004 - QuickNotes_Feature
        //  invalid NSTrackingRectTag. Common possible reasons for
        //  this are: 1. already removed this trackingRectTag, 2. Truncated the
        //  NSTrackingRectTag to 32bit at some point.
        if (trackingRectTag!=0) {
            [self removeTrackingRect:trackingRectTag];
            
            // Another fix for: An uncaught exception was raised - 0x0 is       //leg20240118 - TopXNotes_Catalyst_v3.0.0
            //  an invalid NSTrackingRectTag. Common possible reasons for
            //  this are: 1. already removed this trackingRectTag,
            //  2. Truncated the NSTrackingRectTag to 32bit at some point.
            // Insure removed NSTrackingRectTag is not removable again.
            trackingRectTag = 0;
        }
        trackingRectTag=[self addTrackingRect:rect owner:self userData:NULL assumeInside:NO];
    }
}

-(BOOL)resignFirstResponder { //Non serve, ma per sicurezza
    [self setNeedsDisplay:YES];
   // NSLog(@"OpenCOntrol resignFirstResponder");
    return YES;
}

-(BOOL)canBecomeFirstResponder{
    return NO;
}

- (BOOL)mouseDownCanMoveWindow {
    return YES; //interfaccia
}

-(BOOL)acceptsFirstMouse:(NSEvent *)e {
    return YES;
}



-(void)dealloc {
    [super dealloc];
}
@end
