/* clickImageView */

#import <Cocoa/Cocoa.h>

@interface clickImageView : NSView
{
    NSImage *myImage;
}
-(void)setImage:(NSImage*)image;
-(void)fastDraw;
@end
