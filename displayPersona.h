//
//  displayPersona.h
// 2Remember
//
//
//  Created by Andrea Rincon Ray on Tue Mar 25 2003.
//  Version 1.0-1.0.5 by Andrea Rincon Ray on or beforer Sun Feb 09 2003.
//  Versions after 1.0.6 by James Lee and Lewis Garrett, Tropical Software, Inc. 

//  2Remember
//  Copyright (c) 2011 Tropical Software, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface displayPersona : NSObject {
    NSString *name;
    NSString *phone;
    NSString *email;
    NSString *kind;
    NSString *cognome;
    NSString *azienda;
}
-(void)setNome:(NSString*)stri;
-(void)setCognome:(NSString*)stri;
-(void)setAzienda:(NSString*)stri;

-(void)setEmail:(NSString*)stri;

-(void)setPhone:(NSString*)stri;
-(void)setKind:(NSString*)stri;
-(NSString*)name;

-(NSString*)phone;

-(NSString*)email;
-(NSString*)descrizione;

@end
