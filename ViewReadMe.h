//
//  ViewReadMe.h
//  iAddressX
//
//  Created by Lewis Garrett on 2/10/15.
//
//

#import <Cocoa/Cocoa.h>

@interface ViewReadMe : NSWindowController
{
    @private
    NSTextView * _readMeView;
}
@property (retain) IBOutlet NSTextView* readMeView;

@end
