#import "tabView.h"
//TabView gestisce il comportamento e il display dei due pulsanti tab a alato dello schermo. inizalmente serve solo per mostrarli, potrebbe poi gestire interfacce piu' complesse di tipo tabbed (ad es diviso per argomenti).
//TabView manages the behavior and display of the two tab buttons on the side of the screen. initially it is only used to show them, it could then manage more complex tabbed interfaces (e.g. divided by topics).  //leg20230914 - Translation

//Nota: puo' essere doppia, visto che ce ne sono due!!!
//Note: it can be double, since there are two!!!                                //leg20230914 - Translation
@implementation tabView
-(id)initWithFrame:(NSRect)rect {
    if (self=[super initWithFrame:rect]) {
        visibile=NO;
        hilight=NO;
        lockRects=NO;
        NSNotificationCenter *nc;
        nc=[NSNotificationCenter defaultCenter];
        [nc addObserver:self selector:@selector(blinkON) name:@"ARRblinkON" object:nil];
        [nc addObserver:self selector:@selector(blinkOFF) name:@"ARRblinkOFF" object:nil];
    }
    return self;
}

-(void)blinkON {
    [self setHilight:YES];
}

-(void)blinkOFF {
    [self setHilight:NO];
}

- (BOOL)mouseDownCanMoveWindow {
    return NO; //interfaccia
}

- (void)mouseDown:(NSEvent *)theEvent {

}

- (void)mouseDragged:(NSEvent *)theEvent {

}



-(void)setImage:(NSImage*)image also:(NSImage*)image2 {
    //la prima immagine esiste?
    //does the first image exist?                                               //leg20230914 - Translation
    if (myImage) {
        //allora la rilascio
        //then I release it                                                     //leg20230914 - Translation
        [myImage release];
    }
    //mi tengo la nuova immagine
    //I'm keeping the new image                                                 //leg20230914 - Translation
    [image retain];
    //mi ricordo il suo puntatore.
    //I remember his pointer.                                                   //leg20230914 - Translation
    myImage=image;
    //lo stesso...
    //the same...                                                               //leg20230914 - Translation
    if (hiImage) {
        [hiImage release];
    }
    [image2 retain];
    hiImage=image2;
    //le immagini sono cambiate... ho bisogno di un nuovo display...
    //the images have changed... I need a new display...                        //leg20230914 - Translation
    [self setNeedsDisplay:YES];
}

-(void)setHilight:(BOOL)risultato {
    hilight=risultato;
    //valutare se mettere un set need display...
    //consider whether to install a need display set...                         //leg20230914 - Translation
    lockRects=YES;
    [self display];
    lockRects=NO;
}

-(void)setVisible:(BOOL)risultato {
    visibile=risultato;
}

////draw rect fa due cose: primo disegna la tab giusta e secondo registra una tracking rect...
////draw rect does two things: first it draws the right tab and second it records a tracking rect...  //leg20230914 - Translation
//-(void)drawRect:(NSRect)rect {
//    NSPoint 	p;
//    NSRect	trackRect;
//
//    trackRect=[self bounds];
//    [[NSColor clearColor] set];
//    NSRectFill([self frame]);
//
//    if (lockRects==NO) {
////        if (!trackingRectTag==NULL) {
//        if (!trackingRectTag) {                                                 //leg20180319 - 1.2.0
//            [self removeTrackingRect:trackingRectTag];
//        }
//    }
//    if (visibile==YES){
//        if (lockRects==NO) trackingRectTag=[self addTrackingRect:trackRect owner:self userData:NULL assumeInside:NO];
//
//        p.x=0;
//        p.y=0;
//
//        if ((myImage)&&hilight==NO) {
//            [myImage compositeToPoint:p operation:NSCompositeCopy];
//        }
//
//        if ((hiImage)&&hilight==YES) {
//            [hiImage compositeToPoint:p operation:NSCompositeCopy];
//        }
//    }
//
//}

//
// Merge in TopXNotes/QuickNotes "…Invalid NSTrackingRect" crash fix by         //leg20240415 - 1.4.0
//  replacing function.
//
//draw rect fa due cose: primo disegna la tab giusta e secondo registra una tracking rect...
//draw rect does two things: first it draws the right tab and second it records a tracking rect...  //leg20230914 - Translation

-(void)drawRect:(NSRect)rect {
    NSPoint     p;
    NSRect    trackRect;

    trackRect=[self bounds];
    [[NSColor clearColor] set];
    NSRectFill([self frame]);

    if (lockRects==NO) {
//        if (!trackingRectTag==NULL) {
//        if (!trackingRectTag) {                                                 //leg20180319 - 1.2.0
        // Fix crash on Sonoma: An uncaught exception was raised - 0x0 is an    //leg20231004 - QuickNotes_Feature
        //  invalid NSTrackingRectTag. Common possible reasons for
        //  this are: 1. already removed this trackingRectTag, 2. Truncated the
        //  NSTrackingRectTag to 32bit at some point.
        if (trackingRectTag!=0) {
            [self removeTrackingRect:trackingRectTag];
            
            // Another fix for: An uncaught exception was raised - 0x0 is       //leg20240118 - TopXNotes_Catalyst_v3.0.0
            //  an invalid NSTrackingRectTag. Common possible reasons for
            //  this are: 1. already removed this trackingRectTag,
            //  2. Truncated the NSTrackingRectTag to 32bit at some point.
            //
            // Insure removed NSTrackingRectTag is not removable again.
            trackingRectTag = 0;
        }
    }
    if (visibile==YES){
        if (lockRects==NO) trackingRectTag=[self addTrackingRect:trackRect owner:self userData:NULL assumeInside:NO];

        p.x=0;
        p.y=0;

        if ((myImage)&&hilight==NO) {
            [myImage compositeToPoint:p operation:NSCompositeCopy];
        }

        if ((hiImage)&&hilight==YES) {
            [hiImage compositeToPoint:p operation:NSCompositeCopy];
        }
    }

}

-(BOOL)acceptsFirstResponder { //NON accetta di diventare First Responder.
                               //DO NOT agree to become a First Responder.      //leg20230914 - Translation
                               //should be: "Agree to become a First Responder!" //leg20230914 - Translation
    return YES;
}

-(BOOL)resignFirstResponder { //Non serve, ma per sicurezza
                              //Not necessary, but just to be safe              //leg20230914 - Translation
    [self setNeedsDisplay:YES];
    return YES;
}

-(BOOL)acceptsFirstMouse:(NSEvent *)e {
    return YES;
}

-(void)dealloc {
    NSNotificationCenter *nc;
    nc=[NSNotificationCenter defaultCenter];
    [nc removeObserver:self];
    [super dealloc];                                                            //leg20180319 - 1.2.0
}
@end
