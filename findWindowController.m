#import "findWindowController.h"
#import "displayPersona.h"
#import "DataWindowController.h"
#import "DataWindowInterfaceDriver.h"

@implementation findWindowController
-(id)init{
    if(self=[super initWithWindowNibName:@"findWind"]) {
    }
    return self;
}

-(void)awakeFromNib {
    NSArray* allThat=[tabella tableColumns];
    NSTableColumn *colonna=[allThat objectAtIndex:0];
    [[colonna dataCell] setFont:[NSFont controlContentFontOfSize:12]];
}

-(void)setTableData:(NSArray*)newData {
    [newData retain];
    [tableData release];
    tableData=newData;
    [tabella reloadData];
}

- (int)numberOfRowsInTableView:(NSTableView*)aTableView {
    return [tableData count];
}

- (id)tableView:(NSTableView*)aTableView objectValueForTableColumn:(NSTableColumn*)aTableColumn row:(int)row {
    return [[tableData objectAtIndex:row] descrizione];
}

-(void)sveglia {
    [[self window] makeFirstResponder:tabella];
}

-(NSTableView*)tabella {
    return tabella;
}

- (IBAction)selected:(id)sender {
    [ARRdataWindowInterfaceDriver selectEnter];
}
@end
