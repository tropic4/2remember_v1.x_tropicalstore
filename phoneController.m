//
//  phoneController.m
//
//          2Remember- A simple reminder program

//  2Remember
//
//  Created by Andrea Rincon Ray on Tue Apr 08 2003.
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//

#import "phoneController.h"
#import "phoneSubclass.h"


@implementation phoneController

static phoneController *sharedInstance=nil;

+ (phoneController *)sharedInstance {
    return sharedInstance ? sharedInstance : [[self alloc] init];
}

-(id)init {
    if (sharedInstance) {
        [self dealloc];
    } else {
        sharedInstance =[super init];
    }
    return sharedInstance;
}

-(void)showWithString:(NSString*)stri {
    stringa=[[NSString alloc] initWithString:stri];
    [self showPanel:self];
}

-(IBAction)showPanel:(id)sender {
    if (!testo) {
        NSWindow *theWindow;

        if (![NSBundle loadNibNamed:@"phoneDisplay" owner:self]) {
            NSLog(@"Failed to load phoneDisplay.nib");
            return;
        }

        theWindow= [testo window];

        [theWindow setExcludedFromWindowsMenu:YES];
        [theWindow setMenu:nil];
        [theWindow setLevel:NSStatusWindowLevel];
        [theWindow center];
    }
    [testo setStringValue:stringa];
    [[self window] makeKeyAndOrderFront:nil];
}

- (void)windowDidResignKey:(NSNotification *)aNotification {
    [[self window] close];
}

-(void) dealloc {
    [stringa autorelease];
    [[self window] release];
    [super dealloc];
}
@end
