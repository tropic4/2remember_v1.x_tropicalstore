/* DataWindowInterfaceDriver */

#import <Cocoa/Cocoa.h>
@class appuntamento;
@class findWindowController;
@class DataWindowSubclass;


@interface DataWindowInterfaceDriver : NSObject
{
    IBOutlet NSTabView *tabView;
    
    IBOutlet NSButton *buyAlarm;
    IBOutlet NSButton *buyButton;
    IBOutlet NSTextField *buyDate;
    IBOutlet NSTextView *buyNotes;
    IBOutlet NSTextField *buyTime;
    IBOutlet NSTextField *buyWhat;
    IBOutlet NSTextField *buyWhere;
    IBOutlet DataWindowSubclass *dataWindow;
    IBOutlet NSButton *emailAlarm;
    IBOutlet NSButton *emailButton;
    IBOutlet NSTextField *emailDate;
    IBOutlet NSTextField *emailEmail;
    IBOutlet NSTextView *emailNotes;
    IBOutlet NSTextField *emailPerson;
    IBOutlet NSTextField *emailTime;
    IBOutlet NSButton *gotoAlarm;
    IBOutlet NSButton *gotoButton;
    IBOutlet NSTextField *gotoDate;
    IBOutlet NSTextView *gotoNotes;
    IBOutlet NSTextField *gotoTime;
    IBOutlet NSTextField *gotoWhere;
    IBOutlet NSButton *meetAlarm;
    IBOutlet NSButton *meetButton;
    IBOutlet NSTextField *meetDate;
    IBOutlet NSTextView *meetNotes;
    IBOutlet NSTextField *meetPerson;
    IBOutlet NSTextField *meetPhone;
    IBOutlet NSTextField *meetTime;
    IBOutlet NSTextField *meetWhere;
    IBOutlet NSButton *phoneAlarm;
    IBOutlet NSButton *phoneButton;
    IBOutlet NSTextField *phoneDate;
    IBOutlet NSTextView *phoneNotes;
    IBOutlet NSTextField *phoneNumber;
    IBOutlet NSTextField *phonePerson;
    IBOutlet NSTextField *phoneTime;
    IBOutlet NSButton *todoAlarm;
    IBOutlet NSButton *todoButton;
    IBOutlet NSTextField *todoDate;
    IBOutlet NSTextView *todoNotes;
    IBOutlet NSTextField *todoTime;
    IBOutlet NSTextField *todoTodo;
    IBOutlet NSButton *websiteAlarm;
    IBOutlet NSButton *websiteButton;
    IBOutlet NSTextField *websiteDate;
    IBOutlet NSTextView *websiteNotes;
    IBOutlet NSTextField *websiteTime;
    IBOutlet NSTextField *websiteUrl;
    
    int			currentTab;

    appuntamento	*appunt;

    findWindowController	*findWindow;

    BOOL		windowVisible;

    NSMutableArray 	*people;
    BOOL 		pendingUpdate;
    NSArray		*searchResults;
    BOOL visibileAdesso;
    int		excludeModified;
}
- (IBAction)buyModified:(id)sender;
- (IBAction)chooseBuy:(id)sender;
- (IBAction)chooseEmail:(id)sender;
- (IBAction)chooseGoto:(id)sender;
- (IBAction)chooseMeet:(id)sender;
- (IBAction)choosePhone:(id)sender;
- (IBAction)chooseTodo:(id)sender;
- (IBAction)chooseWebsite:(id)sender;
- (IBAction)emailModified:(id)sender;
- (IBAction)gotoModified:(id)sender;
- (IBAction)meetModified:(id)sender;
- (IBAction)phoneModified:(id)sender;
- (IBAction)setAlarm:(id)sender;
- (IBAction)todoModified:(id)sender;
- (IBAction)websiteModified:(id)sender;

- (void)updateUpperButtons;

- (void) saveIfNeeded;
- (void) updateUI;
- (void) saveContext;

- (void)setupFormatters;

- (void) loadChildWindow;
- (void) showChildWindow;
- (void) hideChildWindow;

-(NSArray*)performSearch:(NSString*)who;

-(void)selectPrevious;
-(void)selectNext;
-(void)selectEnter;

-(void)modificaTitolo;
-(void)lockActions:(BOOL)val;

-(void)excludeModifiedUP;
-(void)excludeModifiedDOWN;

-(void)isVisible:(BOOL)val;
@end
