//
//  appuntamento.m
// 2Remember
//
//
//  Created by Andrea Rincon Ray on Wed Feb 26 2003.
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//


#import "appuntamento.h"
#import "MainWindowController.h"
#import "globals.h"
#import "agenda.h"
#import "AppController.h"
#import "phoneController.h"

@implementation appuntamento
+(void)initialize {
    if (self==[appuntamento class]) {
        [self setVersion:2];
    }
}

-(id)init {// Here we create a new //Qua creiamo uno nuovo
    if (self=[super init]) {
        [self initDizionari];
        [self setupAlarms];

        [self setPerson:@""];
        [self setNote:@""];
        [self setKind:6]; // but must be used last for hours To Do //ma deve essere ultimo usato Per ora To Do
        [self setSyncId:@""];
        [self setPhone:@""];
        [self setPlace:@""];
        alarm=NO;
        [self setEmail:@""];
        [self setWhat:@""];
        [self setTodo:@""];
        hour=-1;
        minute=-1;
        markDelete=NO;
        markNew=YES;
        displayed=NO;
        [self setDataCreazione:[NSDate date]];
        categoria=0;

        hilight=NO;
        selected=NO;
    }
    return self;
}

-(id)initWithCoder:(NSCoder*)coder //qua leggiamo i salvato
{
    if (self=[super init]) {
        [self initDizionari];
        [self setupAlarms];

        if ([coder versionForClassName:@"appuntamento"]==1) {
            [self setPerson:[coder decodeObject]];
            [self setNote:[coder decodeObject]];
            [coder decodeValueOfObjCType:@encode(int) at:&kind];
            [self setSyncId:[coder decodeObject]];
            [self setDate:[coder decodeObject]];
            [self setPhone:[coder decodeObject]];
            [self setPlace:[coder decodeObject]];
            [coder decodeValueOfObjCType:@encode(BOOL) at:&alarm];
            [self setEmail:[coder decodeObject]];
            [self setWhat:[coder decodeObject]];
            [self setTodo:[coder decodeObject]];
            [coder decodeValueOfObjCType:@encode(int) at:&hour];
            [coder decodeValueOfObjCType:@encode(int) at:&minute];
            [self setDataCreazione:[coder decodeObject]];

            [self setKind:kind]; // the call to initialize the pictures!! //la chiamo per inizializzare le immagini!!!
            [self evaluateStartUp]; // the call to initialize the timer if necessary ... //la chiamo per inizializzare il timer se necessario...
            
        } else if ([coder versionForClassName:@"appuntamento"]==2) {
            [self setPerson:[coder decodeObject]];
            [self setNote:[coder decodeObject]];
            [coder decodeValueOfObjCType:@encode(int) at:&kind];
            [self setSyncId:[coder decodeObject]];
            [self setDate:[coder decodeObject]];
            [self setPhone:[coder decodeObject]];
            [self setPlace:[coder decodeObject]];
            [coder decodeValueOfObjCType:@encode(BOOL) at:&alarm];
            [self setEmail:[coder decodeObject]];
            [self setWhat:[coder decodeObject]];
            [self setTodo:[coder decodeObject]];
            [coder decodeValueOfObjCType:@encode(int) at:&hour];
            [coder decodeValueOfObjCType:@encode(int) at:&minute];
            [self setDataCreazione:[coder decodeObject]];
            [coder decodeValueOfObjCType:@encode(int) at:&categoria];

            [self setKind:kind];
            [self evaluateStartUp];
            
        } else if ([coder versionForClassName:@"appuntamento"]>2) {
            [ARRagenda inconsistentArchiveVersion];
        }
        hilight=NO;
        selected=NO;
        markDelete=NO;
        markNew=YES;
        displayed=NO;
    }
    return self;
}

-(void)setDataCreazione:(NSDate*)dataNuova{
    [dataNuova retain];
    [dataCreazione release];
    dataCreazione=dataNuova;
}

-(NSTimeInterval)absoluteCreation {
    return [dataCreazione timeIntervalSinceReferenceDate];
}

-(void)initDizionari {
    attributiDescrizione=[[NSMutableDictionary alloc] init];
    [attributiDescrizione setObject:[NSFont systemFontOfSize:12] forKey: NSFontAttributeName];
    
    attributiData=[[NSMutableDictionary alloc] init];
    [attributiData setObject:[NSFont fontWithName:@"Monaco" size:9] forKey: NSFontAttributeName];
    [attributiData setObject:[NSColor darkGrayColor] forKey: NSForegroundColorAttributeName];

    attributiDati=[[NSMutableDictionary alloc] init];
    [attributiDati setObject:[NSFont systemFontOfSize:12] forKey: NSFontAttributeName];
    //[attributiData setObject:[NSColor darkGrayColor] forKey: NSForegroundColorAttributeName];

    attributiTag=[[NSMutableDictionary alloc] init];
    [attributiTag setObject:[NSFont boldSystemFontOfSize:12] forKey: NSFontAttributeName];
    //[attributiData setObject:[NSColor darkGrayColor] forKey: NSForegroundColorAttributeName];
}

-(void)setupAlarms {
    blink=NO;
    suona=NO;
    snoozeDelta=0;
    NSNotificationCenter *nc;
    nc=[NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(blinkON) name:@"ARRblinkON" object:nil];
    [nc addObserver:self selector:@selector(blinkOFF) name:@"ARRblinkOFF" object:nil];
}

-(void)blinkON {
    if (suona==YES) { // we blink? //dobbiamo lampeggiare? 
        blink=YES;
        // redesigns
		//ridisegna
        [self redrawImage];
    }
}

-(void)blinkOFF {
    if (blink==YES) { // we are enlightened and we have to turn off? //siamo illuminati e dobbiamo spegnere?
        blink=NO;
        [self redrawImage];
    }
}

-(void)fireAlarm {
    suona=YES;
    ilTimer=nil; // is gone ... So we delete the pointer 'avoid trouble // andato... Cancelliamo il puntatore cosi' evitiamo casini
    //NSLog(@"FireAlarm");
	 // I feel that I need AppController notifications!
    //devo avvertire AppController che ho bisogno delle notifiche!!!
    [ARRappController notifyOfAlarm:self];
}

-(void)mightStopAlarm {
    if (suona==YES) {
        suona=NO;
        snoozeDelta=0;
        [self setAlarm:NO];
		// I feel that I no longer AppController 'need notifications
        //devo avvertire AppController che non ho piu' bisogno delle notifiche
        [ARRappController endNotificationOfAlarm:self];
    }
}

-(void)snooze:(int)howMuch {
    if (suona==YES) {
		// This pushes back by x minutes the alarm
		// change ... calculates seconds from now, turn the sign and add delta
		// int = backUpDelta snoozeDelta // We store the existing delta
//questo posticipa di x minuti l'allarme
//modifica... calcola seconds from now, gira il segno ed aggiunge delta
     //   int backUpDelta=snoozeDelta;//Memorizziamo il delta gi esistente
        [self mightStopAlarm]; //fermiamo l'attuale (resetta il delta)
        snoozeDelta=howMuch*60-[self secondsFromNow]; // set the delta again  //impostiamo il delta di nuovo
		// sommo seconds from now, cosi' compenso per il tempo gi trascorso da quando l'allarme  suonato.
		//sommo seconds from now, cosi' compenso per il tempo gi trascorso da quando l'allarme  suonato.
        [self setAlarm:YES];
    }
}

-(void)setTimer {
    // we remove the timer last active ...
	//togliamo il timer precedentemente attivo...
    if (ilTimer!=nil) { [ilTimer invalidate]; ilTimer=nil;}
	// if you stop playing
    //se sta suonando smette
	
    [self mightStopAlarm];
    // if we play we make a new
    //se dobbiamo suonare ne facciamo uno nuovo
    if (alarm==YES) {
// SE secondsFromNow we can set a positive ...
//possiamo settare SE secondsFromNow  positivo...
	
        NSTimeInterval secs=[self secondsFromNow]+snoozeDelta;
        if (secs>5) {
            ilTimer= [NSTimer scheduledTimerWithTimeInterval:secs
                                                      target:self
                                                    selector:@selector(fireAlarm)
                                                    userInfo:nil
                                                     repeats:NO];
        } else {
            // the dates do not coincide ... take off alarm
			//le date non coincidono... togliamo alarm	
            alarm=NO;
        }
    }
}

-(NSTimeInterval)secondsFromNow {
    NSCalendarDate	*localDate;
    
    localDate=[NSCalendarDate dateWithYear:[date yearOfCommonEra]
                                     month:[date monthOfYear]
                                       day:[date dayOfMonth]
                                      hour:hour
                                    minute:minute
                                    second:0
                                  timeZone:[NSTimeZone systemTimeZone]];

    return ([localDate timeIntervalSinceNow]);
}

-(BOOL)canSetAlarm {
    if ([self secondsFromNow]+snoozeDelta>5) return YES; else return NO;
}

// The following should be used only when you start to see if there have been awake when he was off ...
//il seguente deve essere utilizzato solo all'avvio per sapere se ci sono state sveglie quando era spento...

-(void)evaluateStartUp {
	// if the latter is negative and the alarm is ... part.
	//se i secondi sono negativi e l'allarme  si... parte.
    if (date!=nil&&[self secondsFromNow]<0&&alarm==YES&&suona==NO) [self fireAlarm];
}

-(void)matchAction {
    if (kind==0) {//phone
        [[phoneController sharedInstance] showWithString:phone];
    }
    if (kind==2) {//mail
        NSString *tempo=[NSString stringWithFormat:@"mailto:%@",email];
        [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:tempo]];
    }
    if (kind==4) {//web
                  // and there is no need to add http://
				  //se non c' http:// deve aggiungerlo
        if ([place hasPrefix:@"http://"]==YES) {
            [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:place]];
        } else {
            NSString *tempo=[NSString stringWithFormat:@"http://%@",place];
            [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:tempo]];
        }
    }
}

-(void)matchAlarm {
    [self mightStopAlarm];
}

-(void)redrawImage
{
    NSRect aPoint,descrPoint,dataPoint,ringPoint,activePoint;
    NSString *tempo,*dateS;

    dateS=[date descriptionWithCalendarFormat:ARRdateTimeOrdering timeZone:nil locale:nil];
    if (hour!=-1) tempo=[[NSString alloc] initWithFormat:@"%@ - %@", dateS, [self sTime]];
    else tempo=[[NSString alloc] initWithFormat:@"%@", dateS];

    // This is for the designs ...
	//Questo  per i disegni...
    aPoint.origin.x=0;
    aPoint.origin.y=0;
    aPoint.size.height=40;
    aPoint.size.width=200;

	// This is for the description
    //Questo  per la descrizione
    descrPoint.origin.x=50;
    descrPoint.origin.y=18;
    descrPoint.size.height=16;
    descrPoint.size.width=130;

	// This is for the date ..
	//Questo  per la data...
    dataPoint.origin.x=50;
    dataPoint.origin.y=8;
    dataPoint.size.height=10;
    dataPoint.size.width=135;

    // This is for the alarm clock 
	//Questo  per la sveglietta
    ringPoint.origin.x=180;
    ringPoint.origin.y=6;
    ringPoint.size.height=0;
    ringPoint.size.width=0;

    // and this small icons for active ...
	// e questo per le iconcine attive...
    activePoint.origin.x=180;
    activePoint.origin.y=20;
    activePoint.size.height=0;
    activePoint.size.width=0;
    

    [selfImage release];
    selfImage=[[NSImage alloc] initWithSize:aPoint.size];
    [selfImage lockFocus];

    [[NSImage imageNamed:@"button"] compositeToPoint:aPoint.origin operation:NSCompositeCopy];
    if (blink==NO) [myIcon compositeToPoint:aPoint.origin operation:NSCompositeSourceOver];
    if (blink==YES) [myIconH compositeToPoint:aPoint.origin operation:NSCompositeSourceOver];

    [[self descrizioneBottone] drawInRect:descrPoint withAttributes:attributiDescrizione];
    if (date!=nil) [tempo drawInRect:dataPoint withAttributes:attributiData];
    if (alarm!=nil) [[NSImage imageNamed:@"sveglia"] compositeToPoint:ringPoint.origin operation:NSCompositeSourceOver];
    if (kind==0) [[NSImage imageNamed:@"lente"] compositeToPoint:activePoint.origin operation:NSCompositeSourceOver];
    if (kind==2||kind==4) [[NSImage imageNamed:@"freccia"] compositeToPoint:activePoint.origin operation:NSCompositeSourceOver];
    [selfImage unlockFocus];

    
    //selectedImage=[NSImage imageNamed:@"buttonsel"];
    [selectedImage release];
    selectedImage=[[NSImage alloc] initWithSize:aPoint.size];
    [selectedImage lockFocus];
    [[NSImage imageNamed:@"buttonsel"] compositeToPoint:aPoint.origin operation:NSCompositeCopy];
    if (blink==NO) [myIcon compositeToPoint:aPoint.origin operation:NSCompositeSourceOver];
    if (blink==YES) [myIconH compositeToPoint:aPoint.origin operation:NSCompositeSourceOver];

    [[self descrizioneBottone] drawInRect:descrPoint withAttributes:attributiDescrizione];
    if (date!=nil) [tempo drawInRect:dataPoint withAttributes:attributiData];
    if (alarm!=nil) [[NSImage imageNamed:@"sveglia"] compositeToPoint:ringPoint.origin operation:NSCompositeSourceOver];
    if (kind==0) [[NSImage imageNamed:@"lente"] compositeToPoint:activePoint.origin operation:NSCompositeSourceOver];
    if (kind==2||kind==4) [[NSImage imageNamed:@"freccia"] compositeToPoint:activePoint.origin operation:NSCompositeSourceOver];
    [selectedImage unlockFocus];

    
    //hilightImage=[NSImage imageNamed:@"buttonhilight"];
    [hilightImage release];
    hilightImage=[[NSImage alloc] initWithSize:aPoint.size];
    [hilightImage lockFocus];
    [[NSImage imageNamed:@"buttonhilight"] compositeToPoint:aPoint.origin operation:NSCompositeCopy];
    if (blink==NO) [myIcon compositeToPoint:aPoint.origin operation:NSCompositeSourceOver];
    if (blink==YES) [myIconH compositeToPoint:aPoint.origin operation:NSCompositeSourceOver];

    [[self descrizioneBottone] drawInRect:descrPoint withAttributes:attributiDescrizione];
    if (date!=nil) [tempo drawInRect:dataPoint withAttributes:attributiData];
    if (alarm!=nil) [[NSImage imageNamed:@"sveglia"] compositeToPoint:ringPoint.origin operation:NSCompositeSourceOver];
    if (kind==0) [[NSImage imageNamed:@"lente"] compositeToPoint:activePoint.origin operation:NSCompositeSourceOver];
    if (kind==2||kind==4) [[NSImage imageNamed:@"freccia"] compositeToPoint:activePoint.origin operation:NSCompositeSourceOver];
    [hilightImage unlockFocus];
    
    // Prevent tempo from leaking.                                              //leg20180326 - 1.2.0
    [tempo release];
}

// Accessor methods:
//Metodi accessori:
-(void)setDeleted:(BOOL)val {
    markDelete=val;
}

-(BOOL)isDeleted {
    return markDelete;
}

-(void)setNew:(BOOL)val {
    markNew=val;
}

-(BOOL)isNew {
    return markNew;
}

-(void)setPosizione:(NSPoint)val {
    posizione=val;
}

-(NSPoint)posizione {
    return posizione;
}

-(void)setDisplayed:(BOOL)val {
    displayed=val;
}

-(BOOL)displayed {
    return displayed;
}

-(NSImage*)selfImage {
    if (selected==YES) {
        return selectedImage;
    } else if (hilight==YES) {
        return hilightImage;
    } else {
        return selfImage;
    }
}

-(void)selected:(BOOL)val {
    selected=val;
}

-(void)hilight:(BOOL)val {
    hilight=val;
}

-(void)setPerson:(NSString*)val {
    //NSLog(@"SetPerson:%@",val);
    [val retain];
    [person release];
    person=val;
    //person=[[NSString alloc] initWithFormat:@"%@",val];
    [self redrawImage];
    [ARRagenda mightNeedResorting];
    [ARRagenda shouldSaveAsap];

}

-(NSString*)person {
    //NSLog(@"ReturnPerson:%@",person);
    return person;
}

-(void)setNote:(NSString*)val {
    //[val retain];
    [note release];
    note=[[NSString alloc] initWithString:val];
    //note=val;
    [ARRagenda shouldSaveAsap];

}

-(NSString*)note {
    return note;
}

-(void)setKind:(int)val {
    kind=val;
    if (val==0) {myIcon=[NSImage imageNamed:@"phoneSN"]; myIconH=[NSImage imageNamed:@"phoneSH"];}
    if (val==1) {myIcon=[NSImage imageNamed:@"meetSN"]; myIconH=[NSImage imageNamed:@"meetSH"];}
    if (val==2) {myIcon=[NSImage imageNamed:@"mailSN"]; myIconH=[NSImage imageNamed:@"mailSH"];}
    if (val==3) {myIcon=[NSImage imageNamed:@"gotoSN"]; myIconH=[NSImage imageNamed:@"gotoSH"];}
    if (val==4) {myIcon=[NSImage imageNamed:@"webSN"]; myIconH=[NSImage imageNamed:@"webSH"];}
    if (val==5) {myIcon=[NSImage imageNamed:@"buySN"]; myIconH=[NSImage imageNamed:@"buySH"];}
    if (val==6) {myIcon=[NSImage imageNamed:@"todoSN"]; myIconH=[NSImage imageNamed:@"todoSH"];}
    [self redrawImage];
    [ARRagenda mightNeedResorting];
    [ARRagenda shouldSaveAsap];

}

-(int)kind {
    return kind;
}

-(void)setSyncId:(NSString*)val {
    [val retain];
    [syncId release];
    syncId=val;
}

-(NSString*)syncId {
    return syncId;
}

-(void)setDate:(NSCalendarDate*)val {
    [val retain];
    [date release];
    date=val;
    [self redrawImage];
    [ARRagenda mightNeedResorting];
    [ARRagenda shouldSaveAsap];

}

-(NSCalendarDate*)date {
    return date;
}

-(void)setPhone:(NSString*)val {
    [val retain];
    [phone release];
    phone=val;
    [self redrawImage];
    [ARRagenda mightNeedResorting];
    [ARRagenda shouldSaveAsap];

}

-(NSString*)phone {
    return phone;
}

-(void)setPlace:(NSString*)val {
    [val retain];
    [place release];
    place=val;
    [self redrawImage];
    [ARRagenda mightNeedResorting];
    [ARRagenda shouldSaveAsap];

}

-(NSString*)place {
    return place;
}

/*-(void)setSDate:(NSString*)val {


[data release];
data=[NSCalendarDate dateWithNaturalLanguageString:val];
[data retain];//Check
NSLog(@"%@",data);
}*/

/*-(NSString*)sDate {
return [data description];
}*/

-(void)setSTime:(NSString*)val {
    const char *stringa;
    char	digit,zero;
    int		where=0,stato;
    int		lora=0,iminuti=0;


    stringa=[val lossyCString];
    zero=48;
    stato=0;
    while (stringa[where]!=nil) {
        digit=stringa[where]-zero;
        if (digit>=0&&digit<=9) {
            if (stato==0||stato==1) {
                if (stato==0&&digit>2) stato++;
                stato++;
                lora=lora*10+digit;
                if (lora>=10) stato=2;
            } else if (stato==2||stato==3) {
                stato++;
                iminuti=iminuti*10+digit;
            }
        } else {
            if (stato==3) stato=4;
            if (stato==1) stato=2;
        }
        where++;
    }

    // OK now needs to be validated.
	//OK adesso deve essere validata.
    if (lora>=0&&lora<24&&iminuti>=0&&iminuti<60&&stato>0) {
        hour=lora;
        minute=iminuti;
        if (date==nil) {date=[NSCalendarDate date]; [date retain];}


    } else {
        hour=-1;
        minute=-1;
    }
    [self setAlarm:alarm];
    [self redrawImage];
	//[ARRagenda mightNeedResorting];
    [self setAlarm:alarm];
	//[ARRagenda shouldSaveAsap];

}

-(NSString*)sTime{
    NSString	*returnTime;
    if (hour>=0&&hour<24&&minute>=0&&minute<60){
        if (minute>=0&&minute<10)
        {
            returnTime=[[NSString alloc] initWithFormat:@"%d:0%d",hour,minute];
        } else {
            returnTime=[[NSString alloc] initWithFormat:@"%d:%d",hour,minute];
        }
    } else {
        returnTime=[[NSString alloc] initWithString:@""];
    }
    [returnTime autorelease];
    return returnTime;
}

-(void)setAlarm:(BOOL)val {
    if ([self canSetAlarm]==YES) alarm=val; else { alarm=NO; snoozeDelta=0;}
    if (val==NO) snoozeDelta=0;
    [self setTimer]; // however, because it also clears //comunque perch cancella anche
    [self redrawImage];
    [ARRagenda mightNeedResorting];
    [ARRagenda shouldSaveAsap];

}

-(BOOL)alarm {
    return alarm;
}

-(void)setEmail:(NSString*)val {
    [val retain];
    [email release];
    email=val;
    [self redrawImage];
    [ARRagenda mightNeedResorting];
    [ARRagenda shouldSaveAsap];

}

-(NSString*)email {
    return email;
}

-(void)setWhat:(NSString*)val {
    [val retain];
    [what release];
    what=val;
    [self redrawImage];
    [ARRagenda mightNeedResorting];
    [ARRagenda shouldSaveAsap];

}

-(NSString*)what {
    return what;
}

-(void)setTodo:(NSString*)val {
    [val retain];
    [todo release];
    todo=val;
    [self redrawImage];
    [ARRagenda mightNeedResorting];
    [ARRagenda shouldSaveAsap];
}

-(NSString*)todo {
    return todo;
}

-(NSString*)dataRelativa {return nil;//**********
}

-(NSString*)descrizioneTitolo {
    return [self descrizioneBottone];
}

-(NSString*)descrizioneBottone {
    NSString *risultato=[[NSString alloc] init];
    [risultato autorelease];
    switch (kind)
    {
        case 0:
            if (person!=nil) if ([person length]>0) risultato=[risultato stringByAppendingFormat:@"%@ ",person];
            if (phone!=nil) if ([phone length]>0) risultato=[risultato stringByAppendingFormat:@"%@ ",phone];
            break;
        case 1:
            if (person!=nil) if ([person length]>0) risultato=[risultato stringByAppendingFormat:@"%@ ",person];
            if (place!=nil) if ([place length]>0) risultato=[risultato stringByAppendingFormat:@"%@ ",place];
            if (phone!=nil) if ([phone length]>0) risultato=[risultato stringByAppendingFormat:@"%@ ",phone];
            break;
        case 2:
            if (person!=nil) if ([person length]>0) risultato=[risultato stringByAppendingFormat:@"%@ ",person];
            if (email!=nil) if ([email length]>0) risultato=[risultato stringByAppendingFormat:@"%@ ",email];
            break;
        case 3:
            if (place!=nil) if ([place length]>0) risultato=[risultato stringByAppendingFormat:@"%@ ",place];
            break;
        case 4:
            if (place!=nil) if ([place length]>0) risultato=[risultato stringByAppendingFormat:@"%@ ",place];
            break;
        case 5:
            if (what!=nil) if ([what length]>0) risultato=[risultato stringByAppendingFormat:@"%@ ",what];
            if (place!=nil) if ([place length]>0) risultato=[risultato stringByAppendingFormat:@"%@ ",place];
            break;
        case 6:
            if (todo!=nil) if ([todo length]>0) risultato=[risultato stringByAppendingFormat:@"%@ ",todo];
            break;
        default :
            risultato=@"";
    }
    return risultato;
}

-(void)dealloc {
    [self mightStopAlarm];
    NSNotificationCenter *nc;
    nc=[NSNotificationCenter defaultCenter];
    [nc removeObserver:self];
    [super dealloc];
}

-(void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:person];
    [coder encodeObject:note];
    [coder encodeValueOfObjCType:@encode(int) at:&kind];
    [coder encodeObject:syncId];
    [coder encodeObject:date];
    [coder encodeObject:phone];
    [coder encodeObject:place];
    [coder encodeValueOfObjCType:@encode(BOOL) at:&alarm];
    [coder encodeObject:email];
    [coder encodeObject:what];
    [coder encodeObject:todo];
    [coder encodeValueOfObjCType:@encode(int) at:&hour];
    [coder encodeValueOfObjCType:@encode(int) at:&minute];
    [coder encodeObject:dataCreazione];
    [coder encodeValueOfObjCType:@encode(int) at:&categoria];
}
@end
