/* MainWindowSubclass */

#import <Cocoa/Cocoa.h>

@interface MainWindowSubclass : NSWindow
{
    NSPoint			initialLocation;
    NSArray			*configurazioneSchermi;
    NSRect			frameSchermo;
    NSNotificationCenter	*nc;
}
-(BOOL)outsideOfScreen:(NSPoint)position;
-(void)dockingRealTimeProcessor:(int)selection;
@end
