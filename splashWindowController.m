//
//  splashWindowController.m
//
//          2Remember- A simple reminder program

//  2Remember
//
//
//  Copyright (c) 2003 Andrea Rincon Ray. All rights reserved.
//

#import "splashWindowController.h"


@implementation splashWindowController

static splashWindowController *sharedInstance=nil;

+ (splashWindowController *)sharedInstance {
    return sharedInstance ? sharedInstance : [[self alloc] init];
}

-(id)init {
    if (sharedInstance) {
        [self dealloc];
    } else {
        sharedInstance =[super init];
    }
    return sharedInstance;
}

-(IBAction)showPanel:(id)sender {
    if (!welcomeField) {
        NSWindow *theWindow;
        NSString *creditsPath;
        NSAttributedString *creditsString;

//        if (![NSBundle loadNibNamed:@"splashWindow" owner:self]) {
//            NSLog(@"Failed to load splashWindow.nib");
//            return;
//        }
        // Replace deprecated loadNibNamed:owner member.                        //leg20201202 - 1.3.0
        if (![[NSBundle mainBundle] loadNibNamed:@"splashWindow" owner:self topLevelObjects:nil]) {
             NSLog(@"Failed to load splashWindow.nib");
            return;
        }

        theWindow=[welcomeField window];
        creditsPath=[[NSBundle mainBundle] pathForResource:@"Welcome" ofType:@"rtf"];
        creditsString = [[NSAttributedString alloc] initWithPath:creditsPath documentAttributes:nil];

        [welcomeField replaceCharactersInRange:NSMakeRange(0,0)
                                       withRTF:[creditsString RTFFromRange:
                                           NSMakeRange(0, [creditsString length] )
                                                        documentAttributes:nil]];

        // Fix MAS Issue #016 - "Welcome texts were not visible in Dark mode."  //leg20201218 - 1.3.0
        //  Insure "SplashWindow" Appearance is Aqua regardless of Dark Mode.
        if (@available(macOS 10.9, *)) {
//            theWindow.appearance = [NSAppearance appearanceNamed: NSAppearanceNameAqua];
            // Set appearance on just rolling credits instead of the window.    //Leg20201222 - 1.3.0
            NSTextView * _welcomeField = welcomeField;
            _welcomeField.appearance = [NSAppearance appearanceNamed: NSAppearanceNameAqua];
        } else {
            // Not an Issue prior to Dark Mode option in MacOS.
        }
        
        [theWindow setExcludedFromWindowsMenu:YES];
        [theWindow setMenu:nil];
        [theWindow center];
    }
    [[welcomeField window] makeKeyAndOrderFront:nil];
}

@end
