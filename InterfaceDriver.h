/* InterfaceDriver */

#import <Cocoa/Cocoa.h>
@class clickImageView;
@class OpenControl;
@class tabView;


@interface InterfaceDriver : NSObject
{
    IBOutlet NSView *buttonField;
    IBOutlet NSButton *downButton;
    IBOutlet NSButton *minusButton;
@public
    IBOutlet NSButton *plusButton;
    IBOutlet NSScrollView *scrollView;
    IBOutlet NSButton *upButton;
    IBOutlet NSWindow *mainWindow;
    IBOutlet OpenControl *backView;
    IBOutlet clickImageView *topLeft;
    IBOutlet clickImageView *topRight;
    IBOutlet clickImageView *bottomLeft;
    IBOutlet clickImageView *bottomRight;
    IBOutlet clickImageView *titleBar;
    IBOutlet clickImageView *bottomBackground;
    IBOutlet tabView *leftTabView;
    IBOutlet tabView *rightTabView;

    int	displayStatus;
    
    NSImage *topLeftStraight;
    NSImage *topLeftRound;
    NSImage *topRightStraight;
    NSImage *topRightRound;
    NSImage *bottomLeftStraight;
    NSImage *bottomLeftRound;
    NSImage *bottomRightStraight;
    NSImage *bottomRightRound;
    BOOL primoAvvio;
}

- (void)mainInterfaceUpdate;
- (IBAction)downButton:(id)sender;
- (IBAction)minusButton:(id)sender;
- (IBAction)plusButton:(id)sender;
- (IBAction)upButton:(id)sender;
- (void)updateSize;
- (void)showDockImage;
- (void)showDockImage;
- (void)redisplay;
- (BOOL)isMouseOutside;
- (void)updateButtons;
-(void)deleteKey;
-(void)enterKey;
@end
